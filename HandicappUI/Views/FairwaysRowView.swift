//
//  FairwaysRowView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-23.
//

import SwiftUI

struct FairwaysRowView: View {
    @EnvironmentObject var scoreclass: ScoreClass
    let enteringRow: Bool
    var body: some View {
        if scoreclass.scores.filter({ $0.Fairway != "" }).map({ $0.Score }).reduce(0, +) != 0  || MyDefaults().enteringData {
            HStack(spacing: 0) {
                myLog("FairwaysRowView Game=\(scoreclass.scores[0].GameID)")
                Text("Fair").frame(width: holeDimensions().titleColumnWidth)
                    .foregroundColor(MyDefaults().printingScoreCard ? .black : .white)
                ForEach(scoreclass.scores.indices) { scorerecIndex in
                    holeValue(value: String(scoreclass.scores[scorerecIndex].Fairway), enteringRow: enteringRow, index: scorerecIndex, viewNames: ViewNames.init(currentName: String(describing: Self.self)))
                    if scoreclass.scores[scorerecIndex].Hole == 9 {
                        Text( MyDefaults().printingScoreCard ? "Fair" : "\(scoreclass.fairwayFront9)")
                            .holeX2Style()
                    }
                }.environmentObject(scoreclass)
                Text("\(scoreclass.fairwayBack9)")
                    .holeX2Style()
                Text("\(scoreclass.fairwayBack18)")
                    .holeX2Style()
            }
        }
    }
}

//struct FairwaysRowView_Previews: PreviewProvider {
//    static var previews: some View {
//        FairwaysRowView(enteringRow: true).environmentObject(ScoreClass())
//    }
//}
