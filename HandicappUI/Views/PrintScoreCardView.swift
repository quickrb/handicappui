//
//  PrintScoreCardView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-05-29.
//

import SwiftUI

struct PrintScoreCardView: View {
    @State  var  gameRecord: GameRec = GameRec()
    @State private var isShowingDelete = false
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var scoreclass: ScoreClass
    @EnvironmentObject var parsclass: ParsClass
    @State private var courseNames = [String]()
    @State private var selectedCourseName = ""
    @State private var selectedTeeName = ""
    @State private var isChangedCourseTee: String = ""
    @State private var x = "0"
    @State private var y = "0"
    @State private var width = "720"
    @State private var height = "1000"
    @State private var numberOfCards = "3"
    var body: some View {

            VStack {
                HStack {

                    myButton(action: {
                            MyDefaults().printingScoreCard = false
                            presentationMode.wrappedValue.dismiss()

                    }) {
                        Text("Done")
                    }

                    GolfCoursePicture(coursename: selectedCourseName)
                     //   .environmentObject(parsclass)
                     //   .environmentObject(scoreclass)

                    Text("\(MyDefaults().playerName)")
                    Spacer()
//                    TextField(x, text: $x)
//                    TextField(y, text: $y)
//                    TextField(width, text: $width)
//                    TextField(height, text: $height)
                }
                Spacer()
                VStack {

                    CourseTeeSelector(selectedCourseName: $selectedCourseName, selectedTeeName: $selectedTeeName, isChanged: $isChangedCourseTee)
                        .environmentObject(parsclass)
                        .onAppear(perform: {
                            gameRecord = sqlGames().selectPlayersLastGame(playerID: MyDefaults().playerID)
                            selectedCourseName = gameRecord.CourseName
                            selectedTeeName = gameRecord.TeeName
                            _ = myLog("GameEntryView.CourseTeeSelectore.onAppear \(selectedCourseName) - \(selectedTeeName)")
                        })
                        .onChange(of: isChangedCourseTee, perform: { value in
                            gameRecord =  GameRec()
                          //  setupDummyEnvironment()
                        })

                }
                HStack {
                ScoreCardView(gamerec: $gameRecord)
                  //  .environmentObject(parsclass)
                  //  .environmentObject(scoreclass)
                }
                Spacer()
                HStack{
                Text("Number of cards, Max 3 ")
                    .foregroundColor(.white)
                    TextField("3", text: $numberOfCards)
                        .frame(width: 20)
                    myButton(action: { myPrint(numberOfCards: numberOfCards, x: x, y: y, width: width, height: height) }) {
                                            Text("Print")
                                        }
                }
            }
            .frame(width: MyDefaults().appDefaultWidth, height: MyDefaults().appDefaultHeight, alignment: .center)
            .myBackgroundStyle()

    }

    func myPrint(numberOfCards: String, x: String, y: String, width: String, height: String) {
        guard let x = Int(x) else { return }
        guard let y = Int(y) else { return }
        guard let width = Int(width) else { return }
        guard let height = Int(height) else { return }
        guard let numberOfCards = Int(numberOfCards) else { return }

        let printInfo = NSPrintInfo()
        printInfo.scalingFactor = 0.7
        printInfo.leftMargin = 40
        printInfo.rightMargin = 40
        printInfo.topMargin = 15
        printInfo.bottomMargin = 0
        printInfo.isVerticallyCentered = false

        let org = MyDefaults().enteringData
        MyDefaults().enteringData = true
//         let view = ScoreCardView(gamerec: $gameRecord)
//            .environmentObject(scoreclass)
//            .environmentObject(parsclass)

        let view = PrintingView(gameRecord: gameRecord, selectedCourseName: selectedCourseName, selectedTeeName: selectedTeeName, numberOfCards: numberOfCards)
            .environmentObject(scoreclass)
            .environmentObject(parsclass)
        MyDefaults().enteringData = org

        let contentRect = NSRect(x: x, y: y, width: width, height: height) // these values will vary

        let viewToPrint = NSHostingView(rootView: view)
        viewToPrint.frame = contentRect

        let bitMap = viewToPrint.bitmapImageRepForCachingDisplay(in: contentRect)!
        viewToPrint.cacheDisplay(in: contentRect, to: bitMap)

        let image = NSImage(size: bitMap.size)
        image.addRepresentation(bitMap)

        let imageView = NSImageView(frame: contentRect)
        imageView.image = image
        let printOperation = NSPrintOperation(view: imageView, printInfo: printInfo)
        printOperation.showsPrintPanel = true
        printOperation.showsProgressPanel = true
        printOperation.run()

// https://thomas.skowron.eu/blog/printing-with-swiftui/

//        let viewToPrint = NSHostingView(rootView: view)
//        viewToPrint.frame.size = CGSize(width: width, height: height)
//
//        let printOperation = NSPrintOperation(view: viewToPrint, printInfo: printInfo)
//        printOperation.showsPrintPanel = true
//        printOperation.showsProgressPanel = true
//        printOperation.run()
    }
}

struct PrintingView: View {
    @State  var  gameRecord: GameRec = GameRec()
    @State  var selectedCourseName: String
    @State  var selectedTeeName: String
    @State var numberOfCards: Int
    @EnvironmentObject var scoreclass: ScoreClass
    @EnvironmentObject var parsclass: ParsClass

    var body: some View {
        VStack {
            Group {
                ForEach(0..<numberOfCards) { i in
                    Text("----------------------------------- ")
                                    Text(" ")
                Text("\(selectedCourseName)      \(selectedTeeName)     Date _____________")
                Text(" ")
                ScoreCardView(gamerec: $gameRecord)
                    .environmentObject(scoreclass)
                    .environmentObject(parsclass)
                }
            }
//            Group {
//                Text(" ")
//                Text("----------------------------------- ")
//                Text(" ")
//                Text("\(selectedCourseName)      \(selectedTeeName)     Date _____________")
//                Text(" ")
//                ScoreCardView(gamerec: $gameRecord)
//                    .environmentObject(scoreclass)
//                    .environmentObject(parsclass)
//            }
//            Group {
//                Text(" ")
//                Text("----------------------------------- ")
//                Text(" ")
//                Text("\(selectedCourseName)      \(selectedTeeName)     Date _____________")
//                Text(" ")
//                ScoreCardView(gamerec: $gameRecord)
//                    .environmentObject(scoreclass)
//                    .environmentObject(parsclass)
//            }
        }
    }
}

//struct PrintScoreCardView_Previews: PreviewProvider {
//    static var previews: some View {
//        PrintScoreCardView()
//    }
//}

struct PrintScoreCardView_Previews: PreviewProvider {
struct BindingTestHolder: View {
@State var games = sqlGames().selectAllGames(playerID: MyDefaults().playerID)
    @StateObject var scoreclass = ScoreClass()
    @StateObject var parsclass = ParsClass()
    var body: some View {
        PrintScoreCardView()
            .environmentObject(scoreclass)
            .environmentObject(parsclass)
    }
}
static var previews: some View {
    BindingTestHolder()
}
}
