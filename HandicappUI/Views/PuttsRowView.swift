//
//  PuttsRowView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-23.
//

import SwiftUI

struct PuttsRowView: View {

    @EnvironmentObject var scoreclass: ScoreClass
    let enteringRow: Bool
    var body: some View {
        if scoreclass.scores.reduce(0, {$0 + $1.Putts}) != 0 || MyDefaults().enteringData {
            HStack(spacing: 0) {
            myLog("PuttsRowView Game=\(scoreclass.scores[0].GameID)")
                Text("Putts").frame(width: holeDimensions().titleColumnWidth)
                    .foregroundColor(MyDefaults().printingScoreCard ? .black : .white)
                ForEach(scoreclass.scores.indices) { scorerecIndex in
                    holeValue(value: String(scoreclass.scores[scorerecIndex].Putts), enteringRow: enteringRow, index: scorerecIndex, viewNames: ViewNames.init(currentName: String(describing: Self.self)))
                    if scoreclass.scores[scorerecIndex].Hole == 9 {
                        Text( MyDefaults().printingScoreCard ? "Putts" : "\(scoreclass.puttsFront9)")
                            .holeX2Style()
                    }
                }//.environmentObject(scoreclass)
                Text("\(scoreclass.puttsBack9)")
                    .holeX2Style()
                Text("\(scoreclass.puttsBack18)")
                    .holeX2Style()
            }
        }
    }
}

//struct PuttsRowView_Previews: PreviewProvider {
//    static var previews: some View {
//        PuttsRowView(enteringRow: true).environmentObject(ScoreClass())
//    }
//}
