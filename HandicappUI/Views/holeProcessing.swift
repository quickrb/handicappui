//
//  holeProcessing.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-23.
//

import SwiftUI

struct ViewNames {
    var currentName: String
    static let BunkersRowView = "BunkersRowView"
    static let DetailView = "DetailView"
    static let FairwaysRowView = "FairwaysRowView"
    static let GreensRowView = "GreensRowView"
    static let ParRowView = "ParRowView"
    static let PenaltyRowView = "PenaltyRowView"
    static let PuttsRowView = "PuttsRowView"
    static let ScoreRowView = "ScoreRowView"
    static let YardageRowView = "YardageRowView"
    static let HandicapRowView = "HandicapRowView"
    var showingColors: Bool {
        if currentName == ViewNames.ScoreRowView {
            return true
        }
        return false
    }
    var hidingZeros: Bool {
//        if currentName == ViewNames.PenaltyRowView {
//            return false
//        }
        return true
    }
    var scoreViews: Bool {
        if currentName == ViewNames.BunkersRowView
            || currentName == ViewNames.FairwaysRowView
            || currentName == ViewNames.GreensRowView
            || currentName == ViewNames.PuttsRowView
            || currentName == ViewNames.ScoreRowView
        {
            return true
        }
        return false
    }
    var courseViews: Bool {
        if currentName == ViewNames.ParRowView
            || currentName == ViewNames.YardageRowView
            || currentName == ViewNames.HandicapRowView
        {
            return true
        }
        return false
    }
}


//struct holeValueTestText: Text {
//    @State var value: String
//    Text(value)
//}

struct holeValue: View {
    var value: String
    var enteringRow: Bool
    @State private var enteredValue: String = ""
    @EnvironmentObject var scoreclass: ScoreClass
    @EnvironmentObject var parsclass: ParsClass
    @State var index: Int
    @State  var viewNames = ViewNames(currentName: "")
    @State private var isErrorOn: Bool = false
    @State private var isFairwayError: Bool = false
    @State private var isGreenError: Bool = false
//    init(value: String, index: Int, containingView: String) {
//        _value = State(wrappedValue: value)
//        _index = State(wrappedValue: index)
//        _containingView = State(wrappedValue: containingView)
//    }


    var valueFilter: String {
        switch viewNames.currentName {
        case ViewNames.FairwaysRowView:
            return HoleValueFilters().fairwayValues
        case ViewNames.GreensRowView:
            return HoleValueFilters().greenValues
        default:
            return HoleValueFilters().numberValues
        }
    }
    var backgroundColor: Color {
        switch viewNames.currentName {
        case ViewNames.ScoreRowView:
            return viewNames.showingColors ? myParColorFinder(score: scoreclass.scores[index].Score, par: parsclass.pars[index].Par) : .clear
        case ViewNames.GreensRowView:
            return greenInRegulation(score: scoreclass.scores[index].Score, putts: scoreclass.scores[index].Putts, par: parsclass.pars[index].Par) == 0 ? .clear : .green
        default:
            return .clear
        }
    }

    var body: some View {
        
        // myLog("holeValue from \(containingView) index \(index) value \(value) -- $value \($enteredValue)")
        if !enteringRow {
            if value == "0" && viewNames.hidingZeros {
                Text("")
                    .holeStyle()
            } else {
                Text("\(value)")
                    .holeStyle()
//                    .background(viewNames.showingColors ? myParColorFinder(score: scoreclass.scores[index].Score, par: parsclass.pars[index].Par) : .clear)
                    .background(backgroundColor)
            }
        } else {
            TextField(String(value), text: self.$enteredValue)
            .holeStyle()
//            .background(viewNames.showingColors ? myParColorFinder(score: scoreclass.scores[index].Score, par: parsclass.pars[index].Par) : .clear )
                .background(backgroundColor)
            .onChange(of: enteredValue) {
                let txt = $0.filter(valueFilter.contains)
                if enteredValue != "" && enteredValue != txt {
                    isErrorOn.toggle()
                    switch viewNames.currentName {
                    case ViewNames.FairwaysRowView:
                        isFairwayError.toggle()
                    case ViewNames.GreensRowView:
                        isGreenError.toggle()
                    default:
                        isFairwayError = false
                    }
                }
                var maxCount = 0
                switch viewNames.currentName {
                case ViewNames.YardageRowView:
                    maxCount = 3
                case ViewNames.HandicapRowView:
                    maxCount = 2
                default:
                    maxCount = 1
                }
                if txt.count <= maxCount {
                    enteredValue = txt
                } else {
                    enteredValue = String(txt.dropLast())
                }

                switch viewNames.currentName {
                case ViewNames.ScoreRowView:
                    scoreclass.scores[index].Score = Int(enteredValue) ?? 0
                case ViewNames.PuttsRowView:
                    scoreclass.scores[index].Putts = Int(enteredValue) ?? 0
                case ViewNames.FairwaysRowView:
                    scoreclass.scores[index].Fairway = enteredValue.uppercased()
                case ViewNames.GreensRowView:
                    scoreclass.scores[index].Green = enteredValue.uppercased()
                case ViewNames.BunkersRowView:
                    scoreclass.scores[index].Bunker = Int(enteredValue) ?? 0
                case ViewNames.PenaltyRowView:
                    scoreclass.scores[index].Penalty = Int(enteredValue) ?? 0
                case ViewNames.HandicapRowView:
                    parsclass.pars[index].Handicap = Int(enteredValue) ?? 0
                case ViewNames.YardageRowView:
                    parsclass.pars[index].Yardage = Int(enteredValue) ?? 0
                case ViewNames.ParRowView:
                    parsclass.pars[index].Par = Int(enteredValue) ?? 0
                default:
                       enteredValue = "99"
                }
                if viewNames.courseViews {
                    parsclass.isChanged.toggle()
                } else {
                    if viewNames.scoreViews {
                        scoreclass.isChanged.toggle()
                    }
                }
            }
                .popover(isPresented: self.$isErrorOn) {
                    VStack{
                        if self.isFairwayError {
                            VStack {
                                Text("Fairway Entries Allowed")
                                Divider()
                                Text("  H - Hit\n  R - Missed Right\n  L - Missed Left\n  M - Missed everything\n  B - Bunker")
                            }.foregroundColor(.black)
                        } else if self.isGreenError {
                            VStack {
                                Text("Greens Entries Allowed")
                                Divider()
                                Text("  H - Hit\n  R - Missed Right\n  L - Missed Left\n  S - Short\n  O - Missed Long")
                            }.foregroundColor(.black)
                        }
                    }
                }


        }
    }
}
func myParColorFinder(score: Int, par: Int) -> Color {

    switch score - par {
    case -2:
        return .orange
    case -1:
        return .yellow
    case 0:
        return .clear
    case 1:
        return .green
    case 2:
        return .red
    case 3:
        return .purple
    default:
        return .secondary

    }
}
func greenInRegulation(score: Int, putts: Int, par: Int) -> Int {
    var gir = 0
    if (putts == 0 && score == par - 2)
    ||
        (putts == 1 && score == par - 1)
    ||
        (putts == 2 && score == par)
    ||
        (putts == 3 && score == par + 1)
    ||
        (putts == 4 && score == par + 2) {
        gir = 1
    }
    return gir
}
struct holeDimensions {
    let titleColumnWidth:CGFloat = 45
//       let holeWidth:CGFloat = 17         // rbq 2021-04-01
    let holeWidth:CGFloat = 23
       let holePadding:CGFloat = 3
}
struct holeModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(width: holeDimensions().holeWidth)
            .border(Color.black)
            .padding(holeDimensions().holePadding)
            .foregroundColor(MyDefaults().enteringData ? .black : .white)
    }
}
extension Text {
    func holeStyle() -> some View {
        self.modifier(holeModifier())
    }
}
extension TextField {
    func holeStyle() -> some View {
        self.modifier(holeModifier())
    }
}
struct holeX2Modifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(width: holeDimensions().holeWidth * 1.75)
            .padding(holeDimensions().holePadding)
            .border(Color.black)
            .foregroundColor(MyDefaults().printingScoreCard ? .black : .white)
    }
}
extension Text {
    func holeX2Style() -> some View {
        self.modifier(holeX2Modifier())
    }
}
//struct holeValue_Previews: PreviewProvider {
//    static var previews: some View {
//        holeValue(value: "2", enteringRow: true, index: 0, viewNames: ViewNames.init(currentName: ViewNames.ScoreRowView))
//            .environmentObject(ParsClass())
//            .environmentObject(ScoreClass())
//    }
//}
