//
//  ScoreRowAverageView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-05-01.
//

import SwiftUI

struct OneStatisticView: View {
    @ObservedObject  var statistic: Statistic
    var body: some View {
        myLog("ScoreRowAverageView \(statistic.averageOfType)")
        Text(statistic.averageOfType)
            .foregroundColor(.black)
            .holeStyle()
    }
}
struct  ScoreRowAverageView: View {

    @Binding var scoreAccums: StatsAccum
    @State private var isChanged: Bool = true
    var body: some View {
        HStack(spacing: 0) {
           // myLog("ScoreRowAverageView \(scoreAccums.statistics[0].averageOfType)")
            Text("Score")
                .frame(width: holeDimensions().titleColumnWidth)
            ForEach(scoreAccums.statistics.indices, id: \.self) { ind in
                OneStatisticView(statistic: scoreAccums.statistics[ind])
                if scoreAccums.statistics[ind].typeCode == "9" {
                    Text(" ")
                        .holeX2Style()
                }
            }


            Text(" ")
                .holeX2Style()

            Text(" ")
                .holeX2Style()

        }
        .onAppear(perform: { isChanged.toggle() })
    }
}

//struct ScoreRowAverageView_Previews: PreviewProvider {
//    @State var scoreAccums: StatsAccum"
//    static var previews: some View {
//        ScoreRowAverageView(scoreAccums = StatsAccum(accumChars: "Scores"))
//
//    }
//}
