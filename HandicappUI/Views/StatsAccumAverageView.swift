//
//  StatsAccumAverageView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-05-05.
//

import SwiftUI

struct StatisticsCountView: View {
    @ObservedObject var stat: Statistic
    var body: some View {
            HStack {
                Text("\(stat.typeCode)")
                Text("\(stat.numberOfType)")
                Text("\(stat.percentOfType)")
            }

    }
}

struct StatsAccumCountView: View {
    var title: String
    @Binding var stats: StatsAccum
    var body: some View {
        VStack {
        if stats.statistics.reduce(0, {$0 + $1.numberOfType}) == 0 {
            EmptyView()
        } else {
            Text(title)
            ForEach(stats.statistics, id: \.typeCode) { stat in

                StatisticsCountView(stat: stat)

            }
        }
        }
        .font(.system(size: 14, weight: .regular, design: .monospaced))
        .myBackgroundStyle()
    }
}
struct StatisticsAverageView: View {
    @ObservedObject var stat: Statistic
    var body: some View {
        HStack {
            Text("\(stat.typeCode)")
            Text("\(stat.averageOfType)")
        }
    }
}

struct StatsAccumAverageView: View {
    var title: String
    @Binding var stats: StatsAccum
    var body: some View {
        VStack {
            if stats.statistics.reduce(0, {$0 + $1.numberOfType}) == 0 {
                EmptyView()
            } else {
            Text(title)
        ForEach(stats.statistics, id: \.typeCode) { stat in

                StatisticsAverageView(stat: stat)

        }
        }
        }
        .font(.system(size: 14, weight: .regular, design: .monospaced))
        .myBackgroundStyle()
    }
}

struct StatisticsPercentageView: View {
    @ObservedObject var stat: Statistic
    var body: some View {
            HStack {
                Text("\(stat.typeCode)")
                Text("\(stat.percentOfType)")
            }
    }
}

struct StatsAccumPercentageView: View {
    var title: String
    @Binding var stats: StatsAccum
    var body: some View {
        VStack {
            if stats.statistics.reduce(0, {$0 + $1.numberOfType}) == 0 {
                EmptyView()
            } else {
            Text(title)
        ForEach(stats.statistics, id: \.typeCode) { stat in

                StatisticsPercentageView(stat: stat)

        }
        }
        }
        .font(.system(size: 14, weight: .regular, design: .monospaced))
        .myBackgroundStyle()
    }
}
//struct StatsAccumCountView_Previews: PreviewProvider {
//    struct BindingTestHolder: View {
//        var stats = StatsAccum(accumChars: "ABCD")
//        var body: some View {
//            ZStack {
//                StatsAccumCountView(title: "testing", stats: stats)
//            }
//            .onAppear(perform: {
//                stats.statistics[2].typeCode = "Par"
//                stats.statistics[0].numberOfType = 10
//                stats.statistics[1].numberOfType = 10
//                stats.statistics[2].numberOfType = 10
//            })
//        }
//    }
//    static var previews: some View {
//        BindingTestHolder()
//    }
//}

