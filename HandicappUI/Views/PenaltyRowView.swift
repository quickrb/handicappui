//
//  PenaltyRowView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-23.
//

import SwiftUI

struct PenaltyRowView: View {
    @EnvironmentObject var scoreclass: ScoreClass
    let enteringRow: Bool
    var body: some View {
        if scoreclass.scores.filter({ $0.Penalty != 0 }).map({ $0.Score }).reduce(0, +) != 0  || MyDefaults().enteringData {
            HStack(spacing: 0) {
                myLog("PenaltyRowView Game=\(scoreclass.scores[0].GameID)")
                Text("Penalty").frame(width: holeDimensions().titleColumnWidth)
                    .foregroundColor(MyDefaults().printingScoreCard ? .black : .white)
                ForEach(scoreclass.scores.indices) { scorerecIndex in
                    holeValue(value: String(scoreclass.scores[scorerecIndex].Penalty), enteringRow: enteringRow, index: scorerecIndex, viewNames: ViewNames.init(currentName: String(describing: Self.self)))
                    if scoreclass.scores[scorerecIndex].Hole == 9 {
                        Text( MyDefaults().printingScoreCard ? "Penal" : "\(scoreclass.penaltyFront9)")
                            .holeX2Style()
                    }
                }.environmentObject(scoreclass)

                Text("\(scoreclass.penaltyBack9)")
                    .holeX2Style()
                Text("\(scoreclass.penaltyBack18)")
                    .holeX2Style()

            }
        }
    }
}

//struct PenaltyRowView_Previews: PreviewProvider {
//    static var previews: some View {
//        PenaltyRowView(enteringRow: true).environmentObject(ScoreClass())
//    }
//}
