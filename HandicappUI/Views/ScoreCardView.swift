//
//  ScoreCardView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-28.
//

import SwiftUI

struct ScoreCardView: View {
   @Binding var gamerec: GameRec
   @EnvironmentObject var parsclass: ParsClass
   @EnvironmentObject var scoreclass: ScoreClass

    @State private var isScoreRowView = MyDefaults().enteringData
    @State private var isPuttsRowView = MyDefaults().printingScoreCard
    @State private var isFairwaysRowView = MyDefaults().printingScoreCard
    @State private var isGreensRowView = MyDefaults().printingScoreCard
    @State private var isBunkersRowView = MyDefaults().printingScoreCard
    @State private var isPenaltyRowView = MyDefaults().printingScoreCard
    var body: some View {

        if scoreCardCheckScoreEntry() {
            Text("Score Card information has not been entered")
                .font(.largeTitle)
        } else {
            VStack {
                Group {
                    
                    if MyDefaults().enteringData && !MyDefaults().printingScoreCard {
                        HStack {
                            myButton(action: {
                                isScoreRowView.toggle()
                            }, content: {
                                Text("\(isScoreRowView ? Image(systemName: "pencil") : Image(systemName: "pencil.slash"))Score")
                            }, width: 80, height: 20)
                            myButton(action: {
                                isPuttsRowView.toggle()
                            }, content: {
                                Text("\(isPuttsRowView ? Image(systemName: "pencil") : Image(systemName: "pencil.slash"))Putts")
                            }, width: 80, height: 20)
                            myButton(action: {
                                isFairwaysRowView.toggle()
                            }, content: {
                                Text("\(isFairwaysRowView ? Image(systemName: "pencil") : Image(systemName: "pencil.slash"))Fair")
                            }, width: 80, height: 20)
                            myButton(action: {
                                isGreensRowView.toggle()
                            }, content: {
                                Text("\(isGreensRowView ? Image(systemName: "pencil") : Image(systemName: "pencil.slash"))Greens")
                            }, width: 80, height: 20)
                            myButton(action: {
                                isBunkersRowView.toggle()
                                isPenaltyRowView.toggle()
                            }, content: {
                                Text("\(isBunkersRowView ? Image(systemName: "pencil") : Image(systemName: "pencil.slash"))Bunker/Penalty")
                            }, width: 100, height: 20)

                            
                        }
                    }
                }
                Group {
                    myLog("ScoreCardView \(gamerec.GameID)")
                    HeaderRowView(foregroundColor: MyDefaults().printingScoreCard ? .black : .white)
                    YardageRowView(enteringRow: false)
                    HandicapRowView(enteringRow: false)
                    ParRowView(enteringRow: false)
                }.environmentObject(parsclass)
                Group {
                    ScoreRowView(enteringRow: isScoreRowView)
                    PuttsRowView(enteringRow: isPuttsRowView)
                    FairwaysRowView(enteringRow: isFairwaysRowView)
                    GreensRowView(enteringRow: isGreensRowView)
                    BunkersRowView(enteringRow: isBunkersRowView)
                    PenaltyRowView(enteringRow: isPenaltyRowView)
                }
                //.environmentObject(scoreclass)
                //.environmentObject(parsclass)
            }
            .foregroundColor(.white)
            //        .onAppear(perform: {
            //            scoreclass.scores = sqlScores().selectAGame(playerID: gameRecord.PlayerID, gameID: gameRecord.GameID)
            //        })
        }
    }
    func setEntering(to: Bool) -> EmptyView {
        //MyDefaults().enteringData = to
        return EmptyView()
    }
   func scoreCardCheckScoreEntry() -> Bool {

       if MyDefaults().enteringData {
           return false
       }
       return  scoreclass.scores.reduce(0, {$0 + $1.Score}) == 0
   }
}


struct ScoreCardView_Previews: PreviewProvider {
    struct BindingTestHolder: View {
    @State var gamerec: GameRec = sqlGames().selectGameRec(gameID: 214)
        @StateObject var scoreclass = ScoreClass()
        @StateObject var parsclass = ParsClass()
        var body: some View {
            ScoreCardView(gamerec: $gamerec)
                .environmentObject(scoreclass)
                .environmentObject(parsclass)
        }
    }
    static var previews: some View {
        BindingTestHolder()
    }
}
