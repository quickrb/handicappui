//
//  ParRowView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-18.
//

import SwiftUI

struct ParRowView: View {

    @EnvironmentObject var parsclass: ParsClass
    let enteringRow: Bool
    var body: some View {
        HStack(spacing: 0) {
            myLog("ParRowView")
            Text("Par").frame(width: holeDimensions().titleColumnWidth)
                .foregroundColor(MyDefaults().printingScoreCard ? .black : .white)
            ForEach(parsclass.pars.indices, id:\.self) { (hole) in
                if hole == 9 {
                    Text("\(parsclass.parFront9)")
                        .holeX2Style()
                }
                holeValue(value: String(parsclass.pars[hole].Par), enteringRow: enteringRow, index: hole, viewNames: ViewNames.init(currentName: String(describing: Self.self)))
                    .environmentObject(parsclass)
            }.environmentObject(parsclass)

            Text("\(parsclass.parBack9)")
                .holeX2Style()
            Text("\(parsclass.parBack18)")
                .holeX2Style()
        }
    }
}

//struct ParRowView_Previews: PreviewProvider {
//    static var previews: some View {
//        ParRowView(enteringRow: true).environmentObject(ParsClass())
//    }
//}
