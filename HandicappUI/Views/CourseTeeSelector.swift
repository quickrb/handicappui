//
//  CourseTeeSelector.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-30.
//

import SwiftUI

struct CourseTeeSelector: View {
    @Binding  var selectedCourseName: String
    @State private var courseRecs = [CourseRec]()
    @Binding  var selectedTeeName: String
    @State private var teeRecs = [TeeRec]()
    @Binding var isChanged: String
    @EnvironmentObject var parsclass: ParsClass
    var body: some View {
        HStack {
            VStack {
                Text("Course")

                Picker("Course", selection: $selectedCourseName) {
                    ForEach(courseRecs, id:\.CourseName) { rec in
                        Text(rec.CourseName)                        }
                }
                .frame(width: 160, height: 25, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            }
            .labelsHidden()
            .frame(width: 160)
            .clipped()
            .onChange(of: selectedCourseName) { _ in
                _ = myLog("CourseTeeSelector Course Picker .onChange - \(selectedCourseName)")
                getTeeRecs()
                getTeeSelection()
                getParsClass(selectedCourseName: selectedCourseName, selectedTeeName: selectedTeeName, parsclass: parsclass)
                isChanged = selectedCourseName + selectedTeeName
            }
            .onAppear {
                _ = myLog("CourseTeeSelector Course Picker .onAppear - \(selectedCourseName)")
                courseRecs = sqlCourses().selectAll()
             //   selectedCourseName = courseRecs[courseRecs.count - 1].CourseName
            }


            VStack {
                Text("Tee")
                Picker("Tee", selection: $selectedTeeName) {
                    ForEach(teeRecs, id:\.Tee) { rec in
                        Text(rec.Tee)
                    }
                    .onChange(of: selectedTeeName) { _ in
                        _ = myLog("CourseTeeSelector Tee Picker .onChange - \(selectedTeeName)")
                        getTeeRecs()
                        getTeeSelection()
                        getParsClass(selectedCourseName: selectedCourseName, selectedTeeName: selectedTeeName, parsclass: parsclass)
                        isChanged = selectedCourseName + selectedTeeName
                    }
                    .onAppear {
                        _ = myLog("CourseTeeSelector Tee Picker .onAppear - \(selectedTeeName)")
                       // getTeeSelection()
                    }
                }
                .labelsHidden()
                .frame(width: 100, height: 25)
                .clipped()
            }
        }.frame(width:260)
    }
    func getTeeRecs() {
        teeRecs = sqlTees().SelectAllForCourseName(CourseName: selectedCourseName)
    }
    func getTeeSelection() {
        if teeRecs.count > 0 {
            // FIXME: this will give the last tee.  should be able to find the tee the game was played on
           // selectedTeeName = teeRecs[teeRecs.count - 1].Tee
        } else {
            selectedTeeName = ""
        }
    }
    func getParsClass(selectedCourseName: String, selectedTeeName: String, parsclass: ParsClass) {
        let courseID = sqlCourses().courseID(CourseName: selectedCourseName)
        let teeID = sqlTees().TeeID(teeName: selectedTeeName, courseID: courseID)
        if parsclass.pars[0].CourseID == courseID && parsclass.pars[0].TeeID == teeID {
            return
        }
        _ = myLog("getParsClass \(courseID)\(teeID)")
        let pars = sqlPars().selectCourseTee(courseID: courseID, teeID: teeID)
        if pars.count > 1 {
    // doing this to keep apps the same
            parsclass.create(newpars: sqlPars().selectCourseTee(courseID: courseID, teeID: teeID))
        }
    }}

struct CourseTeeSelector_Previews: PreviewProvider {
    struct BindingTestHolder: View {
    @State var selectedCourseName = "Tarandowah"
    @State var selectedTeeName = "White"
    @State private var isChanged = ""
    var body: some View {
        CourseTeeSelector(selectedCourseName: $selectedCourseName, selectedTeeName: $selectedTeeName, isChanged: $isChanged)
    }
    }

    static var previews: some View {
        BindingTestHolder()
    }
}
