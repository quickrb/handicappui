//
//  GreensRowView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-23.
//

import SwiftUI

struct GreensRowView: View {
    @EnvironmentObject var scoreclass: ScoreClass
    let enteringRow: Bool
    var body: some View {
        if scoreclass.scores.filter({ $0.Green != "" }).map({ $0.Score }).reduce(0, +) != 0  || MyDefaults().enteringData {
            HStack(spacing: 0) {
                myLog("GreensRowView Game=\(scoreclass.scores[0].GameID)")
                Text("Green").frame(width: holeDimensions().titleColumnWidth)
                    .foregroundColor(MyDefaults().printingScoreCard ? .black : .white)
                ForEach(scoreclass.scores.indices) { scorerecIndex in
                    holeValue(value: String(scoreclass.scores[scorerecIndex].Green), enteringRow: enteringRow, index: scorerecIndex, viewNames: ViewNames.init(currentName: String(describing: Self.self)))
                    if scoreclass.scores[scorerecIndex].Hole == 9 {
                        Text( MyDefaults().printingScoreCard ? "Green" : "\(scoreclass.greensFront9)")
                            .holeX2Style()
                    }
                }.environmentObject(scoreclass)

                Text("\(scoreclass.greensBack9)")
                    .holeX2Style()
                Text("\(scoreclass.greensBack18)")
                    .holeX2Style()
            }
        }
    }
}

//struct GreensRowView_Previews: PreviewProvider {
//    static var previews: some View {
//        GreensRowView(enteringRow: true).environmentObject(ScoreClass())
//    }
//}
