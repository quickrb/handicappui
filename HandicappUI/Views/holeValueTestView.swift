//
//  holeValueTestView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-04-12.
//

import SwiftUI


struct holeValueTestView: View {
      var value: String
    @State private var entered: String = ""
    var body: some View {
    //return TextField(value, text: $value)
        myLog("holeValueTestView \(value)")
        TextField(String(value), text: $entered)
    }
}

//
//struct holeValueTestView_Previews: PreviewProvider {
//    static var previews: some View {
//        holeValueTestView()
//    }
//}
