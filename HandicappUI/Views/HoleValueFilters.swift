//
//  HoleValueFilters.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-04-30.
//

import SwiftUI


struct HoleValueFilters {
    var fairwayDictionary = [
        "H": "Hit  ",
        "R": "Right",
        "L": "Left ",
        "M": "Miss ",
        "B": "Bunker"
    ]
    var fairwayValues = "HhRrLlMmBb"
    // this gets a string with only the uppercase charactores from the Values
    var fairwayValuesStored: String {
        let upperCharacters = fairwayValues.unicodeScalars.compactMap({ char in
            CharacterSet.uppercaseLetters.contains(char) ? char : nil
        })
        return String(upperCharacters.map{Character($0)})
    }
    var greenDictionary = [
        "H": "Hit  ",
        "R": "Right",
        "L": "Left ",
        "O": "Over ",
        "S": "Short"
    ]
    var greenValues = "HhRrLlOoSs"
    var greenValuesStored: String {
        let upperCharacters = greenValues.unicodeScalars.compactMap({ char in
            CharacterSet.uppercaseLetters.contains(char) ? char : nil
        })
        return String(upperCharacters.map{Character($0)})
    }
    let scoringDictionary = [
        "A": "Albatros",
        "E": "Eagle   ",
        "I": "Birdie  ",
        "P": "Par     ",
        "B": "Bogey   ",
        "D": "Double  ",
        "O": "Other   "
    ]
    var scoringValues = "AlbatrosEaglebIrdieParBoggyDoubleOther"
    var scoringValuesStored: String {
        let upperCharacters = scoringValues.unicodeScalars.compactMap({ char in
            CharacterSet.uppercaseLetters.contains(char) ? char : nil
        })
        return String(upperCharacters.map{Character($0)})
    }
    var numberValues = "0123456789"
}
