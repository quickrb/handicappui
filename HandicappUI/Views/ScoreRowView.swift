//
//  ScoreRowView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-23.
//

import SwiftUI

struct  ScoreRowView: View {

    @EnvironmentObject var scoreclass: ScoreClass
    let enteringRow: Bool
    var body: some View {
        if scoreclass.scores.reduce(0, {$0 + $1.Score}) != 0 || MyDefaults().enteringData {
        HStack(spacing: 0) {
            myLog("ScoreRowView Game=\(scoreclass.scores[0].GameID)")
            Text("Score").frame(width: holeDimensions().titleColumnWidth)
                .foregroundColor(MyDefaults().printingScoreCard ? .black : .white)
            ForEach(scoreclass.scores.indices, id: \.self) { scorerecIndex in
                holeValue(value: String(scoreclass.scores[scorerecIndex].Score), enteringRow: enteringRow, index: scorerecIndex, viewNames: ViewNames.init(currentName: String(describing: Self.self)))
                if scoreclass.scores[scorerecIndex].Hole == 9 {
                    Text( MyDefaults().printingScoreCard ? "Score" : "\(scoreclass.scoreFront9)")
                        .holeX2Style()
                }
            }

            Text("\(scoreclass.scoreBack9)")
                .holeX2Style()

            Text("\(scoreclass.scoreBack18)")
                .holeX2Style()
        }
        }
    }
}

//struct ScoreRowView_Previews: PreviewProvider {
//    static var previews: some View {
//        ScoreRowView(enteringRow: true).environmentObject(ScoreClass())    }
//}

