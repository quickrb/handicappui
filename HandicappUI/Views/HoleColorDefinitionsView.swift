//
//  HoleColorDefinitionsView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-04-20.
//

import SwiftUI

struct HoleColorDefinitionsView: View {
    var body: some View {
        HStack {
            Group {
                Spacer()
                Text("Alba")
                Text("")
                    .holeStyle()
                    .background( myParColorFinder(score: 2, par: 5))
                Text("Eagle")
                Text("")
                    .holeStyle()
                    .background( myParColorFinder(score: 3, par: 5))
                Group {
                    Text("Bird")
                    Text("")
                        .holeStyle()
                        .background( myParColorFinder(score: 4, par: 5))
                    Text("Par")
                    Text("")
                        .holeStyle()
                        .background( myParColorFinder(score: 5, par: 5))
                }
                Group {
                    Text("Bogey")
                    Text("")
                        .holeStyle()
                        .background( myParColorFinder(score: 6, par: 5))
                    Text("Double")
                    Text("")
                        .holeStyle()
                        .background( myParColorFinder(score: 7, par: 5))
                    Text("Other")
                    Text("")
                        .holeStyle()
                        .background( myParColorFinder(score: 8, par: 5))
                }
            }
        }
    }
}

//struct HoleColorDefinitionsView_Previews: PreviewProvider {
//    static var previews: some View {
//        HoleColorDefinitionsView()
//    }
//}
