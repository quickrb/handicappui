//
//  GameEntryView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-28.
//

import SwiftUI

struct GameEntryView: View {
    @State var GDate: String = "2021-01-01"
    @State var entryDate: Date = Date()
    @State var CourseID: String = "1"
    @State var TeeID: String = "1"
    @State var Score: String = "0"
    @State var ScoreAdj: String = "0"
    @Binding var gameRecord: GameRec
    @State var enteringData: Bool
    @State  var selectedCourseName = ""
    @State  var selectedTeeName: String = ""
    @State private var isChangedCourseTee: String = ""
    @EnvironmentObject var parsclass: ParsClass

    var body: some View {
        HStack {
            if enteringData {
                myLog("GameEntryView Game=\(gameRecord.GameID)")

                CourseTeeSelector(selectedCourseName: $selectedCourseName, selectedTeeName: $selectedTeeName, isChanged: $isChangedCourseTee)
                    .environmentObject(parsclass)
                    .onAppear(perform: {
                        selectedCourseName = gameRecord.CourseName
                        selectedTeeName = gameRecord.TeeName
                        _ = myLog("GameEntryView.CourseTeeSelectore.onAppear \(selectedCourseName) - \(selectedTeeName)")
                    })
                    .onChange(of: isChangedCourseTee, perform: { value in
                        gameRecord.CourseID = sqlCourses().courseID(CourseName: selectedCourseName)
                        gameRecord.TeeID = sqlTees().TeeID(teeName: selectedTeeName, courseID: gameRecord.CourseID)
                    })
                HStack {
                VStack {
                    Text("Date")
                    DatePicker("Date", selection: $entryDate, displayedComponents: .date)
                        .labelsHidden()
                        .datePickerStyle(CompactDatePickerStyle())
                        .frame(width: 30)
                        .padding(8)
                        .onChange(of: entryDate, perform: { value in
                            let formatter = DateFormatter()
                            formatter.dateFormat = "yyyy-MM-dd"
                            gameRecord.GDate =  formatter.string(from: entryDate)
                        })
                        .frame(width: 100, height: 25, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                        .clipped()
                }.frame(width:110)
                VStack {
                    Text("Score")
                    TextField(String(gameRecord.Score), text: $Score, onEditingChanged: { (isBegin) in
                        if isBegin {
                            _ = myLog("Begins editing \(Score)")
                        } else {
                            _ = myLog("Finishes editing \(Score)")
                        }
                    },
                    onCommit: {
                        _ = myLog("Commit \(Score)")
                        gameRecord.Score = Int(Score) ?? 0
                    })
                        .frame(width: 30, height: 25)
                       //.padding(8)
                        .foregroundColor(MyDefaults().enteringData ? .black : .white)
                        .onChange(of: Score, perform: { value in
                            _ = myLog("onChange start")
                            let txt = value.filter { $0.isNumber }
                            if value != txt {
                                Score = txt
                            }
                            gameRecord.Score = Int(Score) ?? 0
                            _ = myLog("onChange end")
                        })
                }
                VStack {
                    Text("AdjScore")
                    TextField(String(gameRecord.ScoreAdj), text: $ScoreAdj, onEditingChanged: { (isBegin) in
                        if isBegin {
                            _ = myLog("Begins editing \(ScoreAdj)")
                        } else {
                            _ = myLog("Finishes editing \(ScoreAdj)")
                        }
                    },
                    onCommit: {
                        _ = myLog("Commit \(ScoreAdj)")
                        gameRecord.ScoreAdj = Int(ScoreAdj) ?? 0
                    })
                        .frame(width: 30, height: 25)
                        //.padding(8)
                        .foregroundColor(MyDefaults().enteringData ? .black : .white)
                        .onChange(of: ScoreAdj, perform: { value in
                            _ = myLog("onChange start")
                            let txt = value.filter { $0.isNumber }
                            if value != txt {
                                ScoreAdj = txt
                            }
                            gameRecord.ScoreAdj = Int(ScoreAdj) ?? 0
                            _ = myLog("onChange end")
                        })
                }
            }
            } else {
                Text("Course: \(self.gameRecord.CourseName)")
                Text("Tee: \(self.gameRecord.TeeName)")
                Text("Date: \(self.gameRecord.GDate)")
                Text("Score: \(self.gameRecord.Score)")
                Text("AdjScore: \(self.gameRecord.ScoreAdj)")

            }

        }.onAppear(perform: {
            _ = myLog("GameEntryView.onAppear")
            CourseID = String(gameRecord.CourseID)
            TeeID = String(gameRecord.TeeID)
            GDate = gameRecord.GDate
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/dd"
            entryDate = dateFormatter.date(from: gameRecord.GDate) ?? Date()
            Score = String(gameRecord.Score)
            ScoreAdj = String(gameRecord.ScoreAdj)
        })
        HStack {

            Text("Differenctial \(String(format: "%.1f", self.gameRecord.Differential))")
            Text("Handicap \(self.gameRecord.Handicapp)")
            Text("Handicap Index \(String(format: "%.1f", self.gameRecord.HandiIndex))")
        }
    }
    func getGdate(myDate: String) -> Date {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter.date(from: myDate) ?? Date()
    }
}


struct GameEntryView_Previews: PreviewProvider {

    struct BindingTestHolder: View {
        @State var testrec: GameRec = sqlGames().selectGameRec(gameID: 118)
        var body: some View {
            GameEntryView(gameRecord: $testrec, enteringData: true).environmentObject(ParsClass())

        }
    }
    static var previews: some View {
        BindingTestHolder()
    }
}
