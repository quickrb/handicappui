//
//  BunkersRowView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-23.
//

import SwiftUI

struct BunkersRowView: View {
    @EnvironmentObject var scoreclass: ScoreClass
    var enteringRow: Bool
    var body: some View {
        if scoreclass.scores.filter({ $0.Bunker != 0 }).map({ $0.Score }).reduce(0, +) != 0  || MyDefaults().enteringData {
            HStack(spacing: 0) {
                myLog("BunkersRowView Game=\(scoreclass.scores[0].GameID)")
                Text("Bunker").frame(width: holeDimensions().titleColumnWidth)
                    .foregroundColor(MyDefaults().printingScoreCard ? .black : .white)
                ForEach(scoreclass.scores.indices) { scorerecIndex in
                    holeValue(value: String(scoreclass.scores[scorerecIndex].Bunker), enteringRow: enteringRow, index: scorerecIndex, viewNames: ViewNames.init(currentName: String(describing: Self.self)))
//                    holeValue(value: String(scoreclass.scores[scorerecIndex].Bunker), enteringRow: false, index: scorerecIndex, containingView: String(describing: Self.self))

                    if scoreclass.scores[scorerecIndex].Hole == 9 {
                        Text( MyDefaults().printingScoreCard ? "Bunk" : "\(scoreclass.bunkerFront9)")
                            .holeX2Style()
                    }
                }.environmentObject(scoreclass)
                    Text("\(scoreclass.bunkerBack9)")
                        .holeX2Style()
                    Text("\(scoreclass.bunkerBack18)")
                        .holeX2Style()

            }
        }
    }
}

//struct BunkersRowView_Previews: PreviewProvider {
//    static var previews: some View {
//        BunkersRowView(enteringRow: true).environmentObject(ScoreClass())
//    }
//}
