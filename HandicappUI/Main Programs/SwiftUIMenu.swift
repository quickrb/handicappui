//
//  SwiftUIMenu.swift
//  macstoryswiftUI
//
//  Created by Brian Quick on 2021-02-21.
//

import SwiftUI



struct SwiftUIMenu: View {
    @State private var gameRecord = sqlGames().selectPlayersLastGame(playerID: MyDefaults().playerID)
   // @State private var  playerName =  sqlPlayer().name(PlayerID: MyDefaults().playerID)
    @State var games = sqlGames().selectAllGames(playerID: MyDefaults().playerID)
    @State private var todaysTrivia = "No Trivia Today"
    @EnvironmentObject var parsclass: ParsClass
    @EnvironmentObject var currentPlayer: CurrentPlayer

    init() {
        _ = myLog("init SwiftUIMenu")
        MyDefaults().sqlLogging = true
        MyDefaults().appLogging = true
        MyDefaults().courseID = 1
    }
    var body: some View {
        
        ZStack(alignment: .top) {
            VStack {
                HStack {
                    Text("Welcome \(currentPlayer.playerName)")
                    Spacer()
                    Text(myDateTimeToString(from: Date()))
                }
                HStack {
                    VStack{
                        GameEntryView(gameRecord: $gameRecord, enteringData: false)
                            //.disabled(false)
                            //.environmentObject(parsclass)
                    }


                }

                Spacer()
                HStack {
                    VStack{
                        PopoverLink(destination: DashScoreCard(games: $games), action: {
                            MyDefaults().enteringData = true
                            addAGame()
                        }, label: {
                            Text("Add a New Game")
                        })
                        PopoverLink(destination: PrintScoreCardView(), action: {
                            MyDefaults().enteringData = true
                            MyDefaults().printingScoreCard = true
                        }, label: {
                            Text("Print a Score Card")
                        })
                    }
                    VStack {
                        PopoverLink(destination: DashScoreCard(games: $games), action: {
                            games = sqlGames().selectAllGames(playerID: MyDefaults().playerID)
                            MyDefaults().enteringData = false
                        }, label: {
                            Text("Display Score Card")
                        })
                        PopoverLink(destination: BestGamePossible(), action: {
                            MyDefaults().enteringData = false
                        }, label: {
                            Text("Best Score Card")
                        })

                        PopoverLink(destination: ScoringGrid(), action: {
                            MyDefaults().enteringData = false
                        }, label: {
                            Text("Scoring Grid")
                        })
                        PopoverLink(destination: ScoringGridStatistics(), action: {
                            MyDefaults().enteringData = false
                        }, label: {
                            Text("Statistics Grid")
                        })
                        PopoverLink(destination: ScoringGraphView(), action: {
                            MyDefaults().enteringData = false
                        }, label: {
                            Text("Bar Graph")
                        })

                    }
                    VStack {
                        PopoverLink(destination: CourseMaintenance().environmentObject(parsclass), action: {
                            MyDefaults().enteringData = true
                        }, label: {
                            Text("Course Maintenance")
                        })
                        PopoverLink(destination: ImportCourses().environmentObject(parsclass),label: {
                            Text("Course importing")
                        })
                        PopoverLink(destination: PlayerMaintenance(), action: {
                            MyDefaults().enteringData = true
                        }, label: {
                            Text("Player Maintenance")
                        })

                        PopoverLink(destination: DashScoreCard(games: $games), action: {
                            games = sqlGames().selectAllGames(playerID: MyDefaults().playerID)
                            MyDefaults().enteringData = true
                        }, label: {
                            Text("Score Card Maintenance")
                        })
                    }
                }
                Spacer()
                HStack {
                    Text("\(MyDefaults().sqliteLocation)")
                    Spacer()
                Text(todaysTrivia)
                    .frame(width: 450)
                    .font(.title)
                }
            }
            //.font(.headline)
            .onAppear(perform: {
                setupDummyEnvironment()
                startGame()
            })
        }

        .frame(width: MyDefaults().appDefaultWidth, height: MyDefaults().appDefaultHeight, alignment: .center)
        .myBackgroundStyle()
    }

    func setupDummyEnvironment() {
//        playerName = sqlPlayer().name(PlayerID: MyDefaults().playerID)
//        gameRecord = sqlGames().selectPlayersLastGame(playerID: MyDefaults().playerID)

        parsclass.create(newpars: sqlPars().selectCourseTee(courseID: 1, teeID: 1)) // 2021-04-12 09:50
    }

    func addAGame() {
        var gamerec = sqlGames().selectPlayersLastGame(playerID: MyDefaults().playerID)
        if gamerec.PlayerID == 0 {
            gamerec = sqlGames().selectPlayersLastGame(playerID: 1)
            gamerec.Handicapp = 0
            gamerec.HandiIndex = 0
        }
        gamerec.PlayerID = MyDefaults().playerID
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        gamerec.GDate =  formatter.string(from: Date())
        gamerec.Score = 0
        gamerec.ScoreAdj = 0


        _ = sqlGames().insert(game: gamerec)
        // FIXME: should be able to get the GameID for the last game entered
        gamerec = sqlGames().selectPlayersLastGame(playerID: MyDefaults().playerID)
        for i in 1...18 {
            let score = ScoreRec(PlayerID: gamerec.PlayerID, GameID: gamerec.GameID, Hole: i, Score: 0, Fairway: "", Green: "", Putts: 0, Bunker: 0, Penalty: 0 )
            _ = sqlScores().insertOneScore(score: score)
        }
        games = sqlGames().selectAllGames(playerID: MyDefaults().playerID)
    }

func startGame() {
    if let TriviaForTodayURL = Bundle.main.url(forResource: "TriviaForToday", withExtension: "txt") {
        if let allTrivia = try? String(contentsOf: TriviaForTodayURL) {
            let allLines = allTrivia.components(separatedBy: "\n")
            let filteredLines = allLines.filter { word in
                return word.count > 5
            }
            todaysTrivia = filteredLines.randomElement() ?? "No Trivia Today"
            return
        }
    }
    fatalError("Could not load TriviaForToday.txt from bundle.")
}
}
struct SwiftUIMenu_Previews: PreviewProvider {
   // @State var scoreclass: ScoreClass
    static var previews: some View {
        SwiftUIMenu()
            .environmentObject(ParsClass())
            .environmentObject(CurrentPlayer(playerID: 1, playerName: "Brian"))
    }
}
