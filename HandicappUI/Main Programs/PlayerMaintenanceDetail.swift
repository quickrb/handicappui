//
//  PlayerMaintenanceDetail.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-20.
//

import SwiftUI

struct PlayerMaintenanceDetail: View {
    @Binding var player: PlayerRec
    @State private var enteringData = MyDefaults().enteringData
    @State private var Name: String = ""
    @State private var PlayerID: String = ""
    @State private var Level: String = ""
    @State private var Hdate: String = ""
    @State private var Handicap: String = ""
    @State private var Hindex: String = ""
    @State private var showingCannotDeleteAlert = false
    let textwidth:CGFloat = 80
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var currentPlayer: CurrentPlayer
    var body: some View {
        ZStack(alignment: .top) {
            VStack {
                VStack {
                    HStack {
                        myButton(action: { presentationMode.wrappedValue.dismiss() }) {
                            Text("Done")
                        }

                        Text("ID")
                        Text(String(player.PlayerID))
                        Spacer()
                    }
                }
                Spacer()
                VStack {
                    HStack(spacing:20) {
                        VStack(alignment: .leading, spacing: 24) {
                            Text("Name")
                            Text("Level")
                            Text("Date")
                            Text("Handicapp")
                            Text("Index")
                        }
                        VStack(alignment: .leading, spacing: 20) {
                            TextField(player.Name, text: self.$Name)
                            TextField(String(player.Level), text: self.$Level)
                            TextField(player.Hdate, text: self.$Hdate)
                            TextField(String(player.Handicap), text: self.$Handicap)
                            TextField(String(player.Hindex), text: self.$Hindex)
                        }
                        .frame(width: 80)
                    }
                }
                .frame(width: 300, height: 300, alignment: .center)
                Spacer()
                VStack {
                    if MyDefaults().enteringData {
                        HStack {
                            myButton(action: { PlayerMaintenanceAdd() }) {
                                Text("Add")
                            }
                            myButton(action: { PlayerMaintenanceChange() }) {
                                Text("Change")
                            }
                            myButton(action: {
                                        PlayerMaintenanceDelete(PlayerID: player.PlayerID) }) {
                                Text("Delete")
                            }
                            .alert(isPresented: $showingCannotDeleteAlert){
                                Alert(title: Text("Cannot Delete"), message: Text("There are games recorded for this player"), dismissButton: .default(Text("OK")))
                            }
                            Spacer()
                            ToggleSidebar()
                            .myButtonStyle()
                        }
                    }
                }
            }
            .onAppear(perform: {
                Name =    player.Name
                Level = String(player.Level)
                Hdate = player.Hdate
                Handicap = String(player.Handicap)
                Hindex =  String(player.Hindex)
            })
        }
    }

    func PlayerMaintenanceAdd() {
        let  player =  PlayerRec(Name: Name, PlayerID: Int(PlayerID) ?? 0, Level: Int(Level) ?? 0, Hdate: Hdate, Handicap: Int(Handicap) ?? 0, Hindex: Double(Hindex) ?? 0)
        _ = sqlPlayer().insert(player: player)
    }
    func PlayerMaintenanceChange() {
        player.Name = Name
        player.Level = Int(Level) ?? 0
        player.Hdate = Hdate
        player.Handicap = Int(Handicap) ?? 0
        player.Hindex = Double(Hindex) ?? 0
        _ = sqlPlayer().update(player: player)
        currentPlayer.playerID = player.PlayerID
        currentPlayer.playerName = player.Name
    }

    func PlayerMaintenanceDelete(PlayerID: Int) {

        do {
            let gameCount = try sqlGames().CountPlayerGames(PlayerID: PlayerID)
            if gameCount == 0 {
                _ = sqlPlayer().delete(PlayerID: PlayerID)
            } else {
                showingCannotDeleteAlert.toggle()
            }
        } catch {

            return
        }

    }

}
//struct PlayerMaintenanceDetail_Previews: PreviewProvider {
//
//    struct BindingTestHolder: View {
//        @State var player: PlayerRec = sqlPlayer().selectAPlayer(playerID: 1)
//        @ObservedObject var currentPlayer: CurrentPlayer
//        var body: some View {
//            PlayerMaintenanceDetail(player: $player)
//        }
//    }
//    static var previews: some View {
//        BindingTestHolder().environmentObject(CurrentPlayer)
//    }
//}
