//
//  ScoringGridStatistics.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-04-21.
//

import SwiftUI

struct ScoringGridStatistics: View {

    @State var handicapp: Int = 0
    @State var handiIndex: Double = 0.0
    @State var differentialRecs = [DifferentialRec]()
    @State private var oldestGameID: Int = 0
    @Environment(\.presentationMode) var presentationMode

    @State var stats = StatisticsForGames()

    @State private var scoreParClass = ScoreParClass()
    @State private var courseNames = [String]()
    @State private var selectedCourseName = ""
    var body: some View {
        VStack {
            HStack {
                myButton(action: { presentationMode.wrappedValue.dismiss() }) {
                    Text("Done")
                }
                GolfCoursePicture(coursename: selectedCourseName)
                Text("\(MyDefaults().playerName)")
                Spacer()
            }
            VStack {
                Text("\tThis is the scoring average per hole along with the percentages of scoring, fairways and greens from the course selected.\n\tThe differentials are for the games used to calculate your handicapp with the Highlighted line being the next score to be deleted.")
                    .frame(width: MyDefaults().appDefaultWidth / 1.75)
                    .fixedSize(horizontal: false, vertical: true)
                    .font(.headline)
                Spacer(minLength: 20)
                Picker("Course", selection: $selectedCourseName) {
                    ForEach(courseNames, id:\.self) { rec in
                        Text(rec)
                    }
                }
                .onChange(of: selectedCourseName, perform: { value in
                    stats = StatisticsForGames()
                    fillGameStats()
                })
                .frame(width: 160, height: 25, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                .labelsHidden()
                .clipped()
            }

            Spacer(minLength: 40)
            VStack {

                HeaderRowView(foregroundColor: .black)
                ScoreRowAverageView(scoreAccums: $stats.allAccums[stats.scoreIdx])
                ScoreRowAverageView(scoreAccums: $stats.allAccums[stats.puttsIdx])

            }
            Spacer(minLength: 30)
            HStack(alignment: .top) {
                Group {
                    Spacer()
                VStack {
                    Text("Differentials")
                    ForEach(differentialRecs.indices, id: \.self) { ind in
                        HStack {
                        Text("\(differentialRecs[ind].GDate) ")
                        Text("\(String(format: "%.1f", differentialRecs[ind].Differential))")
                            .underline(ind == differentialRecs.count - 1, color: Color.black)

                        }
                        .foregroundColor(differentialRecs[ind].GameID == oldestGameID ?  Color.red : Color.black)
                    }
                    HStack {
                    Text("\(String(format: "%.1f",differentialRecs.map({ $0.Differential }).reduce(0, +)))")
                    }
                }
                .myBackgroundStyle()

                Spacer()
                }
                Group {
                VStack {
                    StatsAccumPercentageView(title: "Scores %", stats: $stats.allAccums[stats.scoringIdx])
                }
                Spacer()
                VStack {
                    StatsAccumPercentageView(title: "Fairways %", stats: $stats.allAccums[stats.fairwayIdx])
                }
                Spacer()
                VStack {
                    StatsAccumPercentageView(title: "Greens %", stats: $stats.allAccums[stats.greenIdx])
                }
                Spacer()

                VStack {
                    StatsAccumPercentageView(title: "GIR %", stats: $stats.allAccums[stats.girIdx])
                }
                Spacer()
                }
            }
            Spacer(minLength: 40)
        }
        .onAppear(perform: {
                courseNames = sqlGames().selectAllCourseNameWithScores(playerID: MyDefaults().playerID)
            if courseNames.count > 0 {
                selectedCourseName = courseNames[0]
            }
            initOnAppear()
        })
        .frame(width: MyDefaults().appDefaultWidth, height: MyDefaults().appDefaultHeight, alignment: .center)
        .myBackgroundStyle()
    }
    func initOnAppear() {
        _ = myLog("ScoringGridStatistics.initOnAppear")
        differentialRecs = sqlGames().differential20Scores(GameRec: sqlGames().selectPlayersLastGame(playerID: MyDefaults().playerID))
        if differentialRecs.count == 0 {
            return
        }
        differentialRecs.sort {
            $1.Differential > $0.Differential
        }
        if differentialRecs.count > 8 {
        differentialRecs.removeSubrange(8..<differentialRecs.count)
        }
        differentialRecs.sort {
            $0.GDate > $1.GDate
        }

        oldestGameID = differentialRecs[0].GameID



    }
    func fillGameStats() {
        let gamerecs = sqlGames().selectAllGamesWithScores(playerID: MyDefaults().playerID, courseID: sqlCourses().courseID(CourseName: selectedCourseName))
        for i in 0..<gamerecs.count {
            stats.accumAGame(gameRec: gamerecs[i])
        }
        stats.removeZeroes()
    }
}

struct ScoringGridStatistics_Previews: PreviewProvider {
    static var previews: some View {
        ScoringGridStatistics()
    }
}
