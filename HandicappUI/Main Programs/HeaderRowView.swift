//
//  HeaderRowView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-18.
//

import SwiftUI

struct HeaderRowView: View {
    var foregroundColor: Color
    var body: some View {
        HStack(spacing: 0) {
            myLog("HeaderRowView")
            Text("Hole").frame(width: holeDimensions().titleColumnWidth)
                .foregroundColor(foregroundColor)
            ForEach(0..<18) { (hole) in
                if hole == 9 {
                    Text("Out").holeX2Style()
                        .foregroundColor(foregroundColor)
                }
                Text("\(hole + 1)")
                    .foregroundColor(foregroundColor)
                    .holeStyle()
                if hole == 17 {
                    Text("In").holeX2Style()
                        .foregroundColor(foregroundColor)
                    Text("Tot").holeX2Style()
                        .foregroundColor(foregroundColor)
                }
            }
        }
    }
}

//struct HeaderRowView_Previews: PreviewProvider {
//    static var previews: some View {
//        HeaderRowView(foregroundColor: .black)
//    }
//}
