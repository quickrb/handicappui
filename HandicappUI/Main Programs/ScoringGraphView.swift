//
//  ScoringGraphView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-05-14.
//

import SwiftUI

struct ScoringGraphView: View {
    @State var games = sqlGames().selectAllGames(playerID: MyDefaults().playerID)
    @State private var differentials = [DifferentialRec]()
    @State private var  showCourseName: Bool = false
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        VStack {
            HStack {
                myButton(action: { presentationMode.wrappedValue.dismiss() }) {
                    Text("Done")
                }
                Text("\(MyDefaults().playerName)")
                Spacer()
            }
            VStack {
Text("Just because you can doesn't mean you should, so here is a graph showing the last 20 games played, where it was played, the score, and the number of putts.\nPress the bottom button to see the course names.\nThe highlighted bars are games that go towards your handicapp calculation. ")
    .frame(width: MyDefaults().appDefaultWidth / 2)
    .fixedSize(horizontal: false, vertical: true)
    .font(.headline)

                HStack(alignment: .lastTextBaseline) {
                    ForEach(games.indices, id: \.self) { ind in
                        ScoringGraphBarView(game: games[ind], differentials:  differentials, showCourseName: showCourseName)

                    }
                }
            }
            .frame(width: MyDefaults().appDefaultWidth - 100, height: MyDefaults().appDefaultHeight - 200, alignment: .center)
            Spacer()
            myButton(action: { showCourseName.toggle()}, content: {
                Text(showCourseName ? "Show Date Played" : "Show Course Name")
            }, width: 140, height: 20)
        }
        .onAppear(perform: {
            if 20 < games.count {
                games.removeSubrange(20..<games.count)
            }
            (_, _, differentials) = calculateHandicap(GameRec: games[0])

        })
        .frame(width: MyDefaults().appDefaultWidth, height: MyDefaults().appDefaultHeight, alignment: .center)
        .myBackgroundStyle()
    }
}

struct ScoringGraphBarView: View {
    let game: GameRec
    let differentials: [DifferentialRec]
    let showCourseName: Bool
    @State private var putts = 0
    var body: some View {
        //        let value = game.Score
        //        let yValue = Swift.min(value * 2, Int(MyDefaults().appDefaultHeight) - 50)
        VStack {
            HStack(alignment: .lastTextBaseline, spacing: 0) {

                myRectangle(highLight: differentials.contains { $0.GameID == game.GameID } )
                    .frame(width: 10, height: CGFloat(game.Score * 2))
                    .overlay(showCourseName ?
                             Text("\(game.CourseName)")
                        : Text("\(game.GDate)")

                    )



                myRectangle()
                    .frame(width: 15, height: CGFloat(putts * 4))
                    .overlay(Text("..\(putts).."))

            }
            .frame(width: 25, height:250, alignment: .bottom)
            Text("\(game.Score)")
        }
        .onAppear(perform: {
            putts = sqlScores().sumGamePuts(gameID: game.GameID)
        })
    }
}
struct ScoringGraphView_Previews: PreviewProvider {
    static var previews: some View {
        ScoringGraphView()
    }
}
