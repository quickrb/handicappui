//
//  HandicapRowView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-18.
//

import SwiftUI

struct HandicapRowView: View {

    @EnvironmentObject var parsclass: ParsClass
    let enteringRow: Bool
    var body: some View {
        HStack(spacing: 0) {
            myLog("HandicapRowView")
            Text("Handi").frame(width: holeDimensions().titleColumnWidth)
                .foregroundColor(MyDefaults().printingScoreCard ? .black : .white)
            ForEach(parsclass.pars.indices, id: \.self) { (hole) in
                if hole == 9 {
                    Text("")
                        .holeX2Style()
                }
                holeValue(value: String(parsclass.pars[hole].Handicap), enteringRow: enteringRow, index: hole, viewNames: ViewNames.init(currentName: String(describing: Self.self)))
                    .environmentObject(parsclass)
//                Text(verbatim: String(parsclass.pars[hole].Handicap))
//                    .foregroundColor(.white)
//                    .holeStyle()
            }.environmentObject(parsclass)

            Text("")
                .holeX2Style()
            Text("")
                .holeX2Style()
        }
    }
}

//struct HandicapRowView_Previews: PreviewProvider {
//
//   static var previews: some View {
//
//    HandicapRowView(enteringRow: true).environmentObject(ParsClass())
//    }
//}
