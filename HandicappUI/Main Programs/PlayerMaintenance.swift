//
//  PlayerMaintenance.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-11.
//

import SwiftUI


struct PlayerMaintenance: View {

    @State var isEntering: Bool = true
    @State var selection: Int?

    @State var playersclass = PlayersClass()
    //@State private var players = sqlPlayer().selectPlayers()
    @Environment(\.presentationMode) var presentationMode

    var body: some View {
        HStack(alignment: .top) {
//            myButton(action: { presentationMode.wrappedValue.dismiss() }) {
//                Text("Done")
//            }
            NavigationView {
                ZStack {
                    List {
                        ForEach(playersclass.players.indices, id: \.self) { (i) in
                            VStack {
                                NavigationLink(destination: PlayerMaintenanceDetail(player: $playersclass.players[i]), tag: i, selection: self.$selection) {

                                    ( Text("\(playersclass.players[i].Name)")
                                        + Text("\(playersclass.players[i].Hdate)")
                                    )
                                    .lineLimit(/*@START_MENU_TOKEN@*/2/*@END_MENU_TOKEN@*/)

                                }
                            }
                        }.onAppear {
                            self.selection = 0
                        }
                    }
                    .frame(width: 190)
                }
                .myBackgroundStyle()
            }
        }
        .frame(width: 800, height: 381, alignment: .center)
        .myBackgroundStyle()
        }
}


struct PlayerMaintenance_Previews: PreviewProvider {
    static var previews: some View {
        PlayerMaintenance()
    }
}
