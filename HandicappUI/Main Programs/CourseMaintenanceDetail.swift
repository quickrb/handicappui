//
//  CourseMaintenanceDetail.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-05-18.
//

import SwiftUI

struct CourseMaintenanceDetail: View {
    @State  var selectedCourseName: String = ""
    @State  var selectedTeeName: String = ""
    @State var par: String = ""
    @State var rating: String = ""
    @State var slope: String = ""
    @EnvironmentObject var parsclass: ParsClass
    @State private var isAddCourseHidden = true
    @State private var isAddTeeHidden = true
    @State private var isYardageRowView = false
    @State private var isHandicapRowView = false
    @State private var isParRowView = false
    @State private var showingCannotAddCourse = false
    @State private var showingCannotAddTee = false
    @State private var showingCannotDeleteCourse: Bool = false
    @State private var alertMessage: String = ""

    @State  var courserec: CourseRec
    @State var teerec: TeeRec
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        ZStack() {
            VStack {
                Group {

                        HStack {
                            myButton(action: {
                                MyDefaults().enteringData = false
                                presentationMode.wrappedValue.dismiss()

                            }) {
                                Text("Done")
                            }
                            GolfCoursePicture(coursename: courserec.CourseName)
                            Spacer()
                        }

                        HStack {
                            //                    fillEnvironment()

                            Text("Course Name")
                                .frame(width: 100)

                            TextField("Course Name", text: $selectedCourseName)
                                .frame(width: 130, height: 50, alignment: .center)
                                .onChange(of: selectedCourseName, perform: { value in
                                    isAddCourseHidden = sqlCourses().exists(CourseName: selectedCourseName)
                                })
                            myButton(action: { addACourse()
                                isAddCourseHidden = true

                            }) {
                                Text("Add")
                            }.hiddenConditionally(isHidden: isAddCourseHidden)
                            .alert(isPresented: $showingCannotAddCourse) {
                                Alert(title: Text("Course cannot be added"), message: Text(alertMessage), dismissButton: .default(Text("OK")))
                            }
                        }.frame(height: 30)
                        HStack {
                            Text("Tee Name")
                                .frame(width: 100)
                            TextField("Tee Name", text: $selectedTeeName)
                                .frame(width: 90, height: 50, alignment: .center)
                                .onChange(of: selectedTeeName, perform: { value in
                                    isAddTeeHidden = sqlTees().exists(courseName: selectedCourseName, tee: selectedTeeName)
                                })
                            myButton(action: { addATee()
                                isAddTeeHidden = true
                            }) {
                                Text("Add")
                            }.hiddenConditionally(isHidden: isAddTeeHidden)
                            .alert(isPresented: $showingCannotAddTee) {
                                Alert(title: Text("Tee cannot be added"), message: Text(alertMessage), dismissButton: .default(Text("OK")))
                            }
                        }.frame(height: 40)

                        HStack {
                            Text("Par")
                            TextField(par, text: $par)
                                .frame(width: 30, height: 50, alignment: .center)
                            Text("Rating")
                            TextField(rating, text: $rating)
                                .frame(width: 40, height: 50, alignment: .center)
                            Text("Slope")
                            TextField(slope, text: $slope)
                                .frame(width: 45, height: 50, alignment: .center)
                        }.frame(height: 40)

                }
                Group {
                HStack {
                    myButton(action: {
                        isYardageRowView.toggle()
                    }, content: {
                        Text("\(isYardageRowView ? Image(systemName: "pencil") : Image(systemName: "pencil.slash"))Yardage")
                    }, width: 100, height: 20)
                    myButton(action: {
                        isHandicapRowView.toggle()
                    }, content: {
                        Text("\(isHandicapRowView ? Image(systemName: "pencil") : Image(systemName: "pencil.slash"))Handi")
                    }, width: 100, height: 20)
                    myButton(action: {
                        isParRowView.toggle()
                    }, content: {
                        Text("\(isParRowView ? Image(systemName: "pencil") : Image(systemName: "pencil.slash"))Par")
                    }, width: 100, height: 20)
                }
                }
                Group {
                    HeaderRowView(foregroundColor: .white)
                    YardageRowView(enteringRow: isYardageRowView).environmentObject(parsclass)
                    HandicapRowView(enteringRow: isHandicapRowView)
                    ParRowView(enteringRow: isParRowView)
                }
                Spacer()
                Group {
                HStack {
                    myButton(action: { updateACourse(courserec: courserec, teerec: teerec) }) {
                        Text("Update")
                    }
                    myButton(action: { deleteACourse(courserec: courserec, teerec: teerec) }) {
                        Text("Delete")
                    }
                    .alert(isPresented: $showingCannotDeleteCourse) {
                        Alert(title: Text("Course cannot be deleted"), message: Text(alertMessage), dismissButton: .default(Text("OK")))
                    }
                    Spacer()
                    ToggleSidebar()
                }
                }
            }
            .onAppear(perform: {
                parsclass.pars = sqlPars().selectCourseTee(courseID: teerec.CourseID, teeID: teerec.TeeID)
                selectedCourseName = courserec.CourseName
                selectedTeeName = teerec.Tee
                par = String(teerec.Par)
                rating = String(teerec.Rating)
                slope = String(teerec.Slope)
            })
        }
    }
    func addACourse() {
        if selectedCourseName.isEmpty {
            alertMessage = "Course must not be blank for add"
            showingCannotAddCourse.toggle()
            return
        }
        let courseID = sqlCourses().courseID(CourseName: selectedCourseName)
        if courseID != 99 {
            alertMessage = "Course cannot exist for add"
            showingCannotAddCourse.toggle()
        }
        var courserecs = [CourseRec]()
        courserecs = sqlCourses().selectAll()
        courserecs.sort {
            $0.CourseID > $1.CourseID
        }
        let newRec = CourseRec()
        newRec.CourseID = courserecs[0].CourseID + 1
        newRec.CourseName = selectedCourseName
        _ = sqlCourses().insert(courserec: newRec)
        selectedTeeName = ""
        return
    }
    func addATee() {
        if selectedTeeName.isEmpty {
            alertMessage = "Tee must not be blank for add"
            showingCannotAddTee.toggle()
            return
        }
        if par.isEmpty || Int(par) ?? 0 == 0
            || rating.isEmpty || Double(rating) ?? 0.0 == 0.0
            || slope.isEmpty || Double(slope) ?? 0.0 == 0.0 {
            alertMessage = "Par, Rating and Slope must be entered"
            showingCannotAddTee.toggle()
            return
        }
        let courseID = sqlCourses().courseID(CourseName: selectedCourseName)
        if courseID == 99 {
            alertMessage = "Course must be added before Tee can be added"
            showingCannotAddTee.toggle()
            return
        }

        let teerec = TeeRec()
        teerec.CourseID = courseID
        teerec.Tee =  selectedTeeName
        teerec.TeeID = 0
        teerec.Par =  Int(par) ?? 0
        teerec.Rating = Double(rating) ?? 0.0
        teerec.Slope = Double(slope) ?? 0.0
        var teerecs = sqlTees().SelectAllForCourse(courseID: courseID)
        if teerecs.count == 0 {
            teerec.TeeID = 1
        } else {
            teerecs.sort {
                $0.TeeID > $1.TeeID
            }
            teerec.TeeID = teerecs[0].TeeID + 1
        }
        _ = sqlTees().insert(record: teerec)
        for i in 0...17 {
            let parsrec = ParsRec()
            parsrec.CourseID = courseID
            parsrec.TeeID = teerec.TeeID
            parsrec.Hole = parsclass.pars[i].Hole
            parsrec.Par = parsclass.pars[i].Par
            parsrec.Handicap = parsclass.pars[i].Handicap
            parsrec.Yardage = parsclass.pars[i].Yardage
            _ = sqlPars().insertOnePar(par: parsrec)
        }

    }

    func deleteACourse(courserec: CourseRec, teerec: TeeRec) {
        if courserec.CourseID == teerec.CourseID && courserec.CourseID == parsclass.pars[0].CourseID && teerec.TeeID == parsclass.pars[0].TeeID {

        } else {
            alertMessage = "Course, Tee and Pars are not matched, Please select course and tee"
            showingCannotDeleteCourse.toggle()
            return
        }
        var numberOfGames: Int64 = 0
        do {
            try numberOfGames = sqlGames().CountACourse(CourseID: courserec.CourseID)
        } catch {
            numberOfGames = 99
        }
        if numberOfGames != 0 {
            alertMessage = "There are \(numberOfGames) games outstanding for this course"
            showingCannotDeleteCourse.toggle()
            return
        }
        for i in 0...17 {
            _ = sqlPars().delete(CourseID: parsclass.pars[i].CourseID, TeeID: parsclass.pars[i].TeeID, Hole: parsclass.pars[i].Hole)
        }
        _ = sqlTees().delete(CourseID: teerec.CourseID, TeeID: teerec.TeeID)
        var numberOnCourse: Int64 = 0
        do {
            try numberOnCourse = sqlTees().CountForCourse(courseID: teerec.CourseID)
        } catch {
            alertMessage = "Error checking CountForCourse on Tees"
            showingCannotDeleteCourse.toggle()
        }
        if numberOnCourse == 0 {
            _ = sqlCourses().delete(CourseID: courserec.CourseID)
            selectedCourseName = ""
            selectedTeeName = ""
            parsclass.deleteAll()
        }
    }
    func updateACourse(courserec: CourseRec, teerec: TeeRec) {
        let orgcourserec = sqlCourses().select(courseID: courserec.CourseID)
        if orgcourserec.CourseID == courserec.CourseID {
            courserec.CourseName = selectedCourseName
            _ = sqlCourses().update(record: courserec, orgCourseName: orgcourserec.CourseName)
        }
        let orgteerec = sqlTees().select(courseName: selectedCourseName, tee: teerec.Tee)
        if orgteerec.TeeID == teerec.TeeID {
            teerec.Tee = selectedTeeName
            teerec.Par = Int(par) ?? 0
            teerec.Slope = Double(slope) ?? 0.0
            teerec.Rating = Double(rating) ?? 0.0
            _ = sqlTees().update(record: teerec, orgTee: orgteerec.Tee)
        }
        for i in 0..<parsclass.pars.count {
            _ = sqlPars().updateOnePar(par: parsclass.pars[i])

        }
    }
    func getParsClass(selectedCourseName: String, selectedTeeName: String, parsclass: ParsClass) {
        let courseID = sqlCourses().courseID(CourseName: selectedCourseName)
        let teeID = sqlTees().TeeID(teeName: selectedTeeName, courseID: courseID)
        if parsclass.pars[0].CourseID == courseID && parsclass.pars[0].TeeID == teeID {
            return
        }
        _ = myLog("getParsClass \(courseID)\(teeID)")
        let pars = sqlPars().selectCourseTee(courseID: courseID, teeID: teeID)
        if pars.count > 1 {
    // doing this to keep apps the same
            parsclass.create(newpars: sqlPars().selectCourseTee(courseID: courseID, teeID: teeID))
        }
    }

//    func fillEnvironment()  -> EmptyView {
//        _ = myLog("CourseMaintenanceDetail.fillEnvironment")
//
//        return EmptyView()
//    }
}


struct CourseMaintenanceDetail_Previews: PreviewProvider {
struct BindingTestHolder: View {
    @StateObject var parsclass = ParsClass()
    var body: some View {
        CourseMaintenanceDetail(courserec: CourseRec(), teerec: TeeRec()).environmentObject(parsclass)

    }
}
static var previews: some View {
    BindingTestHolder()
}
}
