//
//  ScoringGrid.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-04-17.
//

import SwiftUI

struct ScoringGrid: View {
    @State var games = [GameRec]()

    @State var handicapp: Int = 0
    @State var handiIndex: Double = 0.0
    @State var differentialRecs = [DifferentialRec]()

    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        VStack {
            HStack {
                myButton(action: { presentationMode.wrappedValue.dismiss() }) {
                    Text("Done")
                }
                PopoverLink(destination: ScoringGridStatistics(), action: {
                    MyDefaults().enteringData = false
                }, label: {
                    Text("Statistics Grid")
                })
                HoleColorDefinitionsView()
                Spacer()
                    .frame(width: 110)

            }
            HStack(spacing: 0) {
                Spacer()
                    .frame(width: 90)
                HeaderRowView(foregroundColor: .black)

            }

            ScrollView(.vertical, showsIndicators: false) {
                ForEach(games.indices, id: \.self) { ind in
                    ScoringGridView(gameID: games[ind].GameID, playerID: games[ind].PlayerID, courseID: games[ind].CourseID, teeID: games[ind].TeeID, Gdate: games[ind].GDate, differentialRecs: $differentialRecs)
                }
            }
        }
        .onAppear(perform: {
            games = sqlGames().selectAllGamesWithScores(playerID: MyDefaults().playerID)
            if games.count > 0 {
            (handicapp, handiIndex, differentialRecs) = calculateHandicap(GameRec: games[0])
            }
        })
        .frame(width: MyDefaults().appDefaultWidth, height: MyDefaults().appDefaultHeight, alignment: .center)
        .myBackgroundStyle()
    }
}

struct ScoringGrid_Previews: PreviewProvider {

    static var previews: some View {
        ScoringGrid()
    }
}
