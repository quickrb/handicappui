//
//  ScoreCard.swift
//  macstoryswiftUI
//
//  Created by Brian Quick on 2021-02-07.
//

import SwiftUI

struct Detail: View {
    @State  var  gameRecord: GameRec
    @Binding var games: [GameRec]
    @State var index: Int
    @State private var isShowingDelete = false
    @State private var isShowingUpdate = false
    @Environment(\.presentationMode) var presentationMode
    @State var stats = StatisticsForGames()
    @EnvironmentObject var scoreclass: ScoreClass
    @EnvironmentObject var parsclass: ParsClass
    var body: some View {
        ZStack {
            VStack {
                HStack {
                    fillEnvironment()
                    myButton(action: {
                                MyDefaults().enteringData = false
                                presentationMode.wrappedValue.dismiss() }) {
                        Text("Done")
                    }

                    GolfCoursePicture(coursename: gameRecord.CourseName)

                    Text("\(MyDefaults().playerName)")
                    Spacer()
                }

                GameEntryView(gameRecord: $gameRecord, enteringData: MyDefaults().enteringData)
                if !MyDefaults().enteringData {
                    HStack {
                        VStack {
                            StatsAccumCountView(title: "Scores %", stats: $stats.allAccums[stats.scoringIdx])
                        }
                        VStack {
                            StatsAccumPercentageView(title: "Fairways %", stats: $stats.allAccums[stats.fairwayIdx])
                        }
                        VStack {
                            StatsAccumPercentageView(title: "Greens %", stats: $stats.allAccums[stats.greenIdx])
                        }
                        VStack {
                            StatsAccumCountView(title: "GIR %", stats: $stats.allAccums[stats.girIdx])
                        }
                    }

                    .onAppear(perform: {
                        stats.accumAGame(gameRec: gameRecord)
                        stats.removeZeroes()
                    })
                }

                Spacer()
                ScoreCardView(gamerec: $gameRecord)

                Spacer()
                if MyDefaults().enteringData {
                    HStack {
                        myButton(action: {
                            updateAGame(gamerec: gameRecord, scoreclass: scoreclass)
                            isShowingUpdate = true
                        }) {
                            Text("Update")
                        }
                        .alert(isPresented: $isShowingUpdate, content: {
                            Alert(title: Text("Game has been updated.\nYour handicap is now \(gameRecord.Handicapp) \n with an index of \(String(format: "%.1f", gameRecord.HandiIndex))"))
                        })

                        myButton(action: { isShowingDelete.toggle() }) {
                            Text("Delete")
                        }
                        .alert(isPresented: $isShowingDelete) {
                            Alert(title: Text("Are you sure you want to delete this Game"), message: Text("There is no recovery"), primaryButton: .destructive(Text("Delete")) {
                                ScoreCardDeleteAGame(gameid: gameRecord.GameID, games: &games, index: index)
                            }, secondaryButton: .cancel())
                        }

                        Spacer()


                        ToggleSidebar()
                    }
                } else {
                    HStack {
                        Spacer()

                        ToggleSidebar()
                    }
                }
            }
        }
    }
    func fillEnvironment()  -> EmptyView {
        _ = myLog("filling environment Game=\(gameRecord.GameID) Score=\(scoreclass.scores[0].GameID)")
        if scoreclass.scores[0].GameID != gameRecord.GameID {
            parsclass.create(newpars: sqlPars().selectCourseTee(courseID: gameRecord.CourseID, teeID: gameRecord.TeeID))

            scoreclass.create(newscore: sqlScores().selectAGame(playerID: gameRecord.PlayerID, gameID: gameRecord.GameID))

        }
        return EmptyView()
    }
    func ScoreCardDeleteAGame(gameid: Int, games: inout [GameRec], index: Int) {

        // FIXME: yea, like you are never going to have an error
        _ = sqlScores().deleteAGame(gameID: gameid)
        _ = sqlGames().deleteAGame(gameID: gameid)
        games.remove(at: index)
        if index == games.count {
            gameRecord = games[index - 1]
        } else {
            gameRecord = games[index]
        }
        scoreclass.reInit()
    }

func updateAGame(gamerec: GameRec, scoreclass: ScoreClass) {
    sqlScores().updateAGamesScores(scores: scoreclass.scores)
    var mygamerec = gamerec
    let playerRec = sqlPlayer().selectAPlayer(playerID: MyDefaults().playerID)
    let Score = scoreclass.scores.reduce(0, {$0 + $1.Score})
    if Score != 0 {
        mygamerec.Score = Score
    mygamerec.ScoreAdj = 0
    for i in 0..<scoreclass.scores.count {
        let maxScore = playerRec.Handicap <= parsclass.pars[i].Handicap ? parsclass.pars[i].Par + 2 : parsclass.pars[i].Par + 3
        print("MaxScore=\(maxScore) Score=\(scoreclass.scores[i].Score)")
        if scoreclass.scores[i].Score > maxScore {
            mygamerec.ScoreAdj += maxScore
        } else {
            mygamerec.ScoreAdj += scoreclass.scores[i].Score
        }
    }
    // let ScoreAdj = scoreclass.scores.reduce(0, {$0 + $1.Score})
    // FIXME: this should be a question of updating or not see the adjusted score

    }
    mygamerec.Differential = calculateDifferential(GameRec: mygamerec)
    _ = sqlGames().update(game: mygamerec)
    var differentialRecs = [DifferentialRec]()
    (mygamerec.Handicapp, mygamerec.HandiIndex, differentialRecs) = calculateHandicap(GameRec: gamerec)
    _ = sqlGames().update(game: mygamerec)
    var playerrec = sqlPlayer().selectAPlayer(playerID: MyDefaults().playerID)
    playerrec.Handicap = mygamerec.Handicapp
    playerrec.Hindex = mygamerec.HandiIndex
    playerrec.Hdate = mygamerec.GDate
    _ = sqlPlayer().update(player: playerrec)
}



}

struct DashScoreCard: View {
    @Binding var games: [GameRec]

    @State var selection: Int?
    @Environment(\.presentationMode) var presentationMode

    @State var name =  MyDefaults().playerName

    var body: some View {
        HStack(alignment: .top) {

            NavigationView {
                ZStack(alignment: .top) {
                    LinearGradient(gradient: Gradient(colors: [.white, .blue ]), startPoint: .top, endPoint: .bottom)
                        List {
                            ForEach(games.indices, id: \.self) { i in

                                NavigationLink(destination: Detail(gameRecord: games[i], games: $games, index: i), tag: i, selection: self.$selection) {
                                        ( Text("\(games[i].CourseName)...\(games[i].GameID) ")
                                        + Text("\n")
                                        + Text("\(games[i].Handicapp)|\(String(format: "%.1f", games[i].HandiIndex)) Score \(games[i].Score)")
                                        )
                                        .lineLimit(2)
                                        .myBackgroundStyle()
                                }

                            }

                            .onAppear {
                                self.selection = 0
                            }
                        } // List
                    // On macOS, customization of the accent color is only used if the “Multicolor” accent color is selected in System Preferences -> General -> Accent color:
                    //.accentColor(.blue)
                } // ZStack
            } // NavigationView
        }  // hstack
            .frame(width: MyDefaults().appDefaultWidth, height: MyDefaults().appDefaultHeight, alignment: .center)
            .myBackgroundStyle()


        if games.count == 0 {
            VStack(alignment: .center) {
                Text("No Games available for player \(name)")
                    .font(.headline)
                myButton(action: { presentationMode.wrappedValue.dismiss() }) {
                    Text("Done")
                }
            }

        }
    }
    func deleteGame(at offset: Int) {
        games.remove(at: offset)
    }
}
struct DashScoreCard_Previews: PreviewProvider {
struct BindingTestHolder: View {
@State var games = sqlGames().selectAllGames(playerID: MyDefaults().playerID)
    @StateObject var scoreclass = ScoreClass()
    @StateObject var parsclass = ParsClass()
    var body: some View {
        DashScoreCard(games: $games)
            .environmentObject(scoreclass)
            .environmentObject(parsclass)
    }
}
static var previews: some View {
    BindingTestHolder()
}
}
