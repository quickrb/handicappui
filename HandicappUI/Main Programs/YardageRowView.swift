//
//  YardageRowView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-18.
//

import SwiftUI

struct YardageRowView: View {

    @EnvironmentObject var parsclass: ParsClass
    @State  var myvalue: String = ""
    let enteringRow: Bool
    var body: some View {
        if parsclass.pars.reduce(0, {$0 + $1.Yardage}) != 0 || MyDefaults().enteringData {
            HStack(spacing: 0) {
                Text("Yards")
                    .frame(width: holeDimensions().titleColumnWidth)
                    .foregroundColor(MyDefaults().printingScoreCard ? .black : .white)
                ForEach(parsclass.pars.indices, id: \.self) { scorerecIndex in
                    holeValue(value: String(parsclass.pars[scorerecIndex].Yardage), enteringRow: enteringRow, index: scorerecIndex, viewNames: ViewNames.init(currentName: String(describing: Self.self))).font(.system(size: 8.0)).environmentObject(parsclass)
                    if parsclass.pars[scorerecIndex].Hole == 9 {
                        Text("\(parsclass.yardageFront9)")
                            .holeX2Style()
                            .font(.system(size: 11.0))
                    }

                }
                .environmentObject(parsclass) // 2021-04-12 08:51

                Text("\(parsclass.yardageBack9)")
                    .holeX2Style()
                    .font(.system(size: 11.0))
                Text("\(parsclass.yardageBack18)")
                    .holeX2Style()
                    .font(.system(size: 11.0))
            }
        }
    }
}





//struct YardageRowView_Previews: PreviewProvider {
//    static var previews: some View {
//        YardageRowView(enteringRow: true).environmentObject(ParsClass())
//    }
//}
