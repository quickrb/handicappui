//
//  ScoringGridView.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-04-21.
//

import SwiftUI

struct ScoringGridView: View {
    @State var gameID: Int
    @State var playerID: Int
    @State var courseID: Int
    @State var teeID: Int
    @State var Gdate: String
    @Binding var differentialRecs: [DifferentialRec]
    var formattedDate: String {
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        let mydate = inputFormatter.date(from: Gdate) ?? Date()
        let outputFormatter = DateFormatter()
        outputFormatter.dateFormat = "MM-dd"
        let outputDate =  outputFormatter.string(from: mydate)
        return outputDate
    }
    @State var scoreFront9:  Int = 0
    @State var scoreBack9:  Int = 0
    @State var scoreBack18:  Int = 0
//    @State var scores: [ScoreRec]  = sqlScores().selectAGame(playerID: 1, gameID: 1)
//    @State var pars: [ParsRec] = sqlPars().selectCourseTee(courseID: 1, teeID: 1)
    @State private var scoreParClass = ScoreParClass()
    var body: some View {
        HStack(spacing: 0) {
            Text("\(formattedDate + " " + sqlCourses().CourseName(courseID: courseID))")
                .frame(width: holeDimensions().titleColumnWidth * 3)
                .background(differentialRecs.contains { $0.GameID == gameID } ? Color.yellow : Color.clear)

            ForEach(scoreParClass.scores.indices, id: \.self) { ind in
                Text("\(scoreParClass.scores[ind].Score)")
                    .holeStyle()
                    .background(myParColorFinder(score: scoreParClass.scores[ind].Score, par: scoreParClass.pars[ind].Par))
                if scoreParClass.scores[ind].Hole == 9 {
                    Text("\(scoreFront9)")
                        .holeX2Style()
                }
            }
            Text("\(scoreBack9)")
                .holeX2Style()

            Text("\(scoreBack18)")
                .holeX2Style()
                .background(differentialRecs.contains { $0.GameID == gameID } ? Color.yellow : Color.clear)
        }
        .onAppear(perform: {
            scoreParClass.scores  = sqlScores().selectAGame(playerID: playerID, gameID: gameID)
            scoreParClass.pars = sqlPars().selectCourseTee(courseID: courseID, teeID: teeID)
            scoreFront9 = scoreParClass.scores.filter({ $0.Hole < 10 }).map({ $0.Score }).reduce(0, +)
            scoreBack9 = scoreParClass.scores.filter({ $0.Hole > 9 }).map({ $0.Score }).reduce(0, +)
            scoreBack18 = scoreParClass.scores.reduce(0, {$0 + $1.Score})
        })
    }
}

struct ScoringGridView_Previews: PreviewProvider {
    struct BindingTestHolder: View {
    @State private var differentialRecs = sqlGames().differential20Scores(GameRec: sqlGames().selectPlayersLastGame(playerID: 32))
    var body: some View {
        ScoringGridView(gameID: 108, playerID: 1, courseID: 1, teeID: 1, Gdate: "2021-04-04", differentialRecs: $differentialRecs)
    }
    }
    static var previews: some View {
        BindingTestHolder()
    }
}
