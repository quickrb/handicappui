//
//  BestGamePossible.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-28.
//

import SwiftUI

struct BestGamePossible: View {
    @State  var  gameRecord: GameRec = GameRec()
    @State private var isShowingDelete = false
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var scoreclass: ScoreClass
    @EnvironmentObject var parsclass: ParsClass
    @State private var courseNames = [String]()
    @State private var selectedCourseName = ""
    @State private var selectedTeeName = ""
    @State private var isChangedCourseTee: String = ""
    var body: some View {

            VStack {
                HStack {

                    myButton(action: { presentationMode.wrappedValue.dismiss() }) {
                        Text("Done")
                    }

                    GolfCoursePicture(coursename: selectedCourseName)
                     //   .environmentObject(parsclass)
                     //   .environmentObject(scoreclass)

                    Text("\(MyDefaults().playerName)")
                    Spacer()
                }
                VStack {
                    Text("\tThe best game you can play for the course selected, based on all the games for the course.")
                        .frame(width: MyDefaults().appDefaultWidth / 1.75)
                        .fixedSize(horizontal: false, vertical: true)
                        .font(.headline)
                    Spacer(minLength: 20)
                    CourseTeeSelector(selectedCourseName: $selectedCourseName, selectedTeeName: $selectedTeeName, isChanged: $isChangedCourseTee)
                        .environmentObject(parsclass)
                        .onAppear(perform: {
                            gameRecord = sqlGames().selectPlayersLastGame(playerID: MyDefaults().playerID)
                            selectedCourseName = gameRecord.CourseName
                            selectedTeeName = gameRecord.TeeName
                            _ = myLog("GameEntryView.CourseTeeSelectore.onAppear \(selectedCourseName) - \(selectedTeeName)")
                        })
                        .onChange(of: isChangedCourseTee, perform: { value in
                            gameRecord =  sqlGames().selectPlayersLastGame(playerID: MyDefaults().playerID, courseID: sqlCourses().courseID(CourseName: selectedCourseName), teeID: sqlTees().TeeID(teeName: selectedTeeName, courseID: sqlCourses().courseID(CourseName: selectedCourseName)))
                            setupDummyEnvironment()
                        })
//                    Picker("Course", selection: $selectedCourseName) {
//                        ForEach(courseNames, id:\.self) { rec in
//                            Text(rec)
//                        }
//                    }
//                    .onChange(of: selectedCourseName, perform: { value in
//                        gameRecord =  sqlGames().selectPlayersLastGame(playerID: MyDefaults().playerID, courseID: sqlCourses().courseID(CourseName: selectedCourseName))
//                        setupDummyEnvironment()
//                    })
//                    .onAppear(perform: {
//                        courseNames = sqlGames().selectAllCourseNameWithScores(playerID: MyDefaults().playerID)
//                        selectedCourseName = courseNames[0]
//                    })
//                    .frame(width: 160, height: 25, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
//                    .labelsHidden()
//                    .clipped()
                }
                Spacer(minLength: 50)
                HStack {
                ScoreCardView(gamerec: $gameRecord)
                  //  .environmentObject(parsclass)
                  //  .environmentObject(scoreclass)
                }
                Spacer(minLength: 90)
            }
            .frame(width: MyDefaults().appDefaultWidth, height: MyDefaults().appDefaultHeight, alignment: .center)
            .myBackgroundStyle()

    }
    func setupDummyEnvironment() {

        var current = [ScoreRec]()
        for i in 1...18 {
            var myrec = ScoreRec()
            myrec.PlayerID = gameRecord.PlayerID
            myrec.GameID = gameRecord.GameID
            myrec.Hole = i
            myrec.Score = 99
            myrec.Bunker = 99
            myrec.Penalty = 99
            current.append(myrec)
        }
        let allScores: [ScoreRec] = sqlScores().selectAllScores(playerID: gameRecord.PlayerID, courseID: gameRecord.CourseID)
        for i in 0..<allScores.count {
            if allScores[i].Score > 0 {
            if current[allScores[i].Hole - 1].Score > allScores[i].Score {
                current[allScores[i].Hole - 1].Score = allScores[i].Score
            }
            }
            if allScores[i].Putts > 0 {
            if current[allScores[i].Hole - 1].Putts > allScores[i].Putts {
                current[allScores[i].Hole - 1].Putts = allScores[i].Putts
            }
            }

            if "H" == allScores[i].Fairway {
                current[allScores[i].Hole - 1].Fairway = allScores[i].Fairway
            }


            if "H" == allScores[i].Green {
                current[allScores[i].Hole - 1].Green = allScores[i].Green
            }

            if allScores[i].Bunker > 0 {
            if current[allScores[i].Hole - 1].Bunker > allScores[i].Bunker {
                current[allScores[i].Hole - 1].Bunker = allScores[i].Bunker
            }
            }
            if allScores[i].Penalty > 0 {
            if current[allScores[i].Hole - 1].Penalty > allScores[i].Penalty {
                current[allScores[i].Hole - 1].Penalty = allScores[i].Penalty
            }
            }
        }
        for i in 0...17 {

            if current[i].Score == 99 {
                current[i].Score = 0
            }
            if current[i].Bunker == 99 {
                current[i].Bunker = 0
            }
            if current[i].Penalty == 99 {
                current[i].Penalty = 0
            }
        }
        scoreclass.create(newscore: current)

    }

}

struct BestGamePossible_Previews: PreviewProvider {
struct BindingTestHolder: View {
@State var games = sqlGames().selectAllGames(playerID: MyDefaults().playerID)
    @StateObject var scoreclass = ScoreClass()
    @StateObject var parsclass = ParsClass()
    var body: some View {
        BestGamePossible()
            .environmentObject(scoreclass)
            .environmentObject(parsclass)
    }
}
static var previews: some View {
    BindingTestHolder()
}
}
