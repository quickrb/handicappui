//
//  CourseMaintenance.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-31.
//

import SwiftUI

struct CourseMaintenance: View {
    @State var isEntering: Bool = true
    @State var selection: Int?

    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var parsclass: ParsClass
    @State var selectedCourseName: String = ""
    @State var selectedTeeName: String = ""
    @State private var isChangedCourseTee: String = ""
    @State var par: String = ""
    @State var rating: String = ""
    @State var slope: String = ""

    @State  var courserecs = [CourseRec]()
    @State  var teerecs = [TeeRec]()
    var body: some View {
        HStack {
            NavigationView {
                ZStack {
                    LinearGradient(gradient: Gradient(colors: [.white, .blue ]), startPoint: .top, endPoint: .bottom)
                    List {
                        ForEach(teerecs.indices, id: \.self) { ind in

                            NavigationLink(
                                destination: CourseMaintenanceDetail(courserec: courserecs[ind], teerec: teerecs[ind]).environmentObject(parsclass),
                                tag: ind,
                                selection: self.$selection) {

                                Text("\(courserecs[ind].CourseName)\n \(teerecs[ind].Tee)")
                                    .lineLimit(2)
                            }
                            .frame(width: 130, alignment: .center)
                            .myBackgroundStyle()
                        }


                    }
                    .onAppear {
                        teerecs = sqlTees().SelectAll()
                        for i in 0..<teerecs.count {
                            courserecs.append(sqlCourses().select(courseID: teerecs[i].CourseID))
                        }
                        self.selection = 0
                    }
                }
            }
        }
        .frame(width: MyDefaults().appDefaultWidth, height: MyDefaults().appDefaultHeight, alignment: .center)
        .myBackgroundStyle()
    }
}

struct CourseMaintenance_Previews: PreviewProvider {
    static var previews: some View {
        CourseMaintenance()
    }
}
