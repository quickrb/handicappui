//
//  GolfCoursePicture.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-17.
//

import SwiftUI

struct GolfCoursePicture: View {
    var coursename:String
    var body: some View {
        Image(coursename)
            .resizable()
            .frame(width: 83, height: 60)
            .clipShape(Capsule())
            .overlay(Capsule().stroke(Color.primary, lineWidth: 1))
    }
}
private extension Image {
    init(_ str: String) {
        self.init(nsImage:
             NSImage(named: str) ?? NSImage(named: "unKnown")!
        )
    }
}
struct GolfCoursePicture_Previews: PreviewProvider {
    static var previews: some View {
        GolfCoursePicture(coursename: "Tarandowah")
    }
}
