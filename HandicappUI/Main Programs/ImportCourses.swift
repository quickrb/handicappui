//
//  ImportCourses.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-06-13.
//

import SwiftUI
import SQLite3

struct ImportCourses: View {
    @State var selection: Int?
    let courserecs: [CourseRec] = Bundle.main.decode("Course.json")
    let teerecs: [TeeRec] = Bundle.main.decode("Tee.json")
    let importparsrecs: [ParsRec] = Bundle.main.decode("Pars.json")
    @State private var showAlert: Bool = false
    @State private var alertMessage = ""
   // @State var parsrecs: [ParsRec] = [ParsRec]()
    @EnvironmentObject var parsclass: ParsClass
    var body: some View {
        HStack {
            NavigationView {
                ZStack {
                    LinearGradient(gradient: Gradient(colors: [.white, .blue ]), startPoint: .top, endPoint: .bottom)
                    List {
                        ForEach(teerecs.indices, id: \.self) { ind in

                            NavigationLink(
                                destination: ImportCoursesDetail(
                                    courserec: myCourseRec(ID: teerecs[ind].CourseID, courserecs: courserecs),
                                    teerec: teerecs[ind],
                                    parsrecs: myParsrecs(courseID: teerecs[ind].CourseID, teeID: teerecs[ind].TeeID, parsrecs: importparsrecs, parsclass: parsclass)

                                    ).environmentObject(parsclass),
                                tag: ind,
                                selection: self.$selection) {

                                Text("\(teerecs[ind].CourseID) - \(myCourseName(ID: teerecs[ind].CourseID, courserecs: courserecs)) \n\(teerecs[ind].TeeID) - \(teerecs[ind].Tee)")
                                    .lineLimit(2)
                                    .background(checkHasChanges(myteerec: teerecs[ind]) ? LinearGradient(gradient: Gradient(colors: [.white, .red ]), startPoint: .top, endPoint: .bottom) : LinearGradient(gradient: Gradient(colors: [.white, .blue ]), startPoint: .top, endPoint: .bottom))
                            }
                        }
                    }
                    .onAppear {
                        self.selection = 0
                    }
                }
            }
        }
        .frame(width: MyDefaults().appDefaultWidth, height: MyDefaults().appDefaultHeight, alignment: .center)
        .myBackgroundStyle()
        .alert(isPresented: $showAlert) {
            Alert(title: Text("Updates Applied for"), message: Text(alertMessage), dismissButton: .default(Text("OK")) )
        }

        if courserecs.count > 1 {
        myButton(action: {
                    applyChanges()
                }) {
                    Text("Apply Changes")
                }
        } else {
            Text("No Updates available for this version")
        }
    }
    func applyChanges() {
        if sqlCourses().rebuildCourseTables() != SQLITE_OK {
            alertMessage = "The update failed...\ndo not add any courses until this is fixed.\nSorry for the inconvenience."
            showAlert.toggle()
            return
        }
        changeCourses()
        changeTees()
        changePars()
        alertMessage = "Course, Tee, Par, Yardage and Handicapp"
        showAlert.toggle()
    }
    func changeCourses() {
        for i in 0..<courserecs.count {
            let courserec = sqlCourses().select(courseID: courserecs[i].CourseID)
            if courserec.CourseName == "unKnown" {
                _ = sqlCourses().insert(courserec: courserecs[i])
            } else {
                _ = sqlCourses().update(record: courserecs[i], orgCourseName: courserec.CourseName)
            }
        }
    }
    func changeTees() {
        for i in 0..<teerecs.count {
            let teerec = sqlTees().select(courseID: teerecs[i].CourseID, teeID: teerecs[i].TeeID)
            if teerec.Tee == "unKnown" {
                _ = sqlTees().insert(record: teerecs[i])
            } else {
                _ = sqlTees().update(record: teerecs[i], orgTee: teerec.Tee)
            }
        }
    }
    func changePars() {
        for i in 0..<importparsrecs.count {
            let parsrec = sqlPars().select(courseID: importparsrecs[i].CourseID, teeID: importparsrecs[i].TeeID, hole: importparsrecs[i].Hole)
            if parsrec.CourseID == importparsrecs[i].CourseID &&
                parsrec.TeeID == importparsrecs[i].TeeID &&
                parsrec.Hole == importparsrecs[i].Hole {
                _ = sqlPars().updateOnePar(par: importparsrecs[i])
            } else {
                _ = sqlPars().insertOnePar(par: importparsrecs[i])
            }
        }
    }
    func checkHasChanges(myteerec: TeeRec) ->  Bool {
        let mycourserec = myCourseRec(ID: myteerec.CourseID, courserecs: courserecs)
        let courserec = sqlCourses().select(courseID: myteerec.CourseID)

        if mycourserec != courserec {
            return true
        }

        let teerec = sqlTees().select(courseID: myteerec.CourseID, teeID: myteerec.TeeID)

        if myteerec != teerec {
            return true
        }
        let myparsrecs = myParsrecs(courseID: myteerec.CourseID, teeID: myteerec.TeeID, parsrecs: importparsrecs, parsclass: parsclass)
        let parsrecs = sqlPars().selectCourseTee(courseID: myteerec.CourseID, teeID: myteerec.TeeID)

        for i in 0...17 {
            if myparsrecs[i] != parsrecs[i] {
                return true
            }
        }
        return false



    }
}

func myCourseName(ID: Int, courserecs: [CourseRec]) -> String {
    let recs: [CourseRec] = courserecs.filter { $0.CourseID == ID }
    return recs.count == 0 ? "unknown" : recs[0].CourseName
}
func myCourseRec(ID: Int, courserecs: [CourseRec]) -> CourseRec {
    let recs: [CourseRec] = courserecs.filter { $0.CourseID == ID }
    return recs.count == 0 ? courserecs[0] : recs[0]
}
func myParsrecs(courseID: Int, teeID: Int, parsrecs: [ParsRec], parsclass: ParsClass) -> [ParsRec]{
    let recs: [ParsRec] = parsrecs.filter { $0.CourseID == courseID && $0.TeeID == teeID }
    return recs
}

struct ImportCourses_Previews: PreviewProvider {
struct BindingTestHolder: View {
    @StateObject var parsclass = ParsClass()
    var body: some View {
        ImportCourses()
            .environmentObject(parsclass)
    }
}
static var previews: some View {
    BindingTestHolder()
}
}
