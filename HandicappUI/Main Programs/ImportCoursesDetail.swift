//
//  ImportCoursesDetail.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-06-13.
//

import SwiftUI

struct ImportCoursesDetail: View {
    @State  var courserec: CourseRec
    @State var teerec: TeeRec
    @State var parsrecs: [ParsRec]
    @EnvironmentObject var parsclass: ParsClass
    @Environment(\.presentationMode) var presentationMode
    var body: some View {
        ZStack() {
            VStack {
                HStack {
                    myButton(action: {
                        MyDefaults().enteringData = false
                        presentationMode.wrappedValue.dismiss()

                    }) {
                        Text("Done")
                    }
                    GolfCoursePicture(coursename: courserec.CourseName)
                    Spacer()
                }
                Spacer()
                if courserec.CourseName == "Nothing" {
                    Text("No Updates available for this update")
                } else {
                HStack {
                    Text("CourseID")
                    Text("\(courserec.CourseID)")
                        .frame(width: 30, height: 50, alignment: .center)
                    Text("CourseName")
                    Text("\(courserec.CourseName)")
                        .frame( height: 50, alignment: .center)
                }
                HStack {
                    Text("Par")
                    Text("\(teerec.Par)")
                        .frame(width: 30, height: 50, alignment: .center)
                    Text("Rating")
                    Text("\(String(format: "%.1f",teerec.Rating))")
                        .frame(width: 40, height: 50, alignment: .center)
                    Text("Slope")
                    Text("\(String(format: "%.1f",teerec.Slope))")
                        .frame(width: 45, height: 50, alignment: .center)
                }.frame(height: 40)
                .onAppear(perform: {
                    parsclass.create(newpars: parsrecs)
                })
                    HeaderRowView(foregroundColor: MyDefaults().printingScoreCard ? .black : .white)
                    YardageRowView(enteringRow: false)
                    HandicapRowView(enteringRow: false)
                    ParRowView(enteringRow: false)
                Spacer()
                }
                HStack {
//                    myButton(action: { updateACourse(courserec: courserec, teerec: teerec) }) {
//                        Text("Update")
//                    }
//                    myButton(action: { deleteACourse(courserec: courserec, teerec: teerec) }) {
//                        Text("Delete")
//                    }
//                    .alert(isPresented: $showingCannotDeleteCourse) {
//                        Alert(title: Text("Course cannot be deleted"), message: Text(alertMessage), dismissButton: .default(Text("OK")))
//                    }
                    Spacer()
                    ToggleSidebar()
                }
            }

        }
    }
}

//struct ImportCoursesDetail_Previews: PreviewProvider {
//struct BindingTestHolder: View {
//    @StateObject var parsclass = ParsClass()
//    var body: some View {
//        ImportCoursesDetail(courserec: CourseRec(), teerec: TeeRec(), parsCount: 18).environmentObject(parsclass)
//
//    }
//}
//static var previews: some View {
//    BindingTestHolder()
//}
//}
