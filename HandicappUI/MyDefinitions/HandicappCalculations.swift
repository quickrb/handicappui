//
//  HandicappCalculations.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-17.
//

import SwiftUI

func calculateDifferential(GameRec: GameRec) -> Double {
//        A Score Differential is calculated using the following formula:
//        (113 / Slope Rating) x (Adjusted Gross Score - Course Rating - PCC adjustment)
//  (113 / 122) * (88 - 68.1) = 18.431967213 = 18.4
//  (113 / 122) * (85 - 68.1) = 15.653278689 = 15.7
//  (113 / 122) * (83 - 68.1) = 13.800819672 = 13.8
    let mydifferentialRec = sqlGames().differentialScore(GameRec: GameRec)
    return mydifferentialRec.DifferentialCalced
}
func calculateHandicap(GameRec: GameRec) -> (Int, Double, [DifferentialRec]) {
    var myArray: [DifferentialRec] = sqlGames().differential20Scores(GameRec: GameRec)
    if myArray.count == 0 { return ( 0, 0.0, myArray ) }
    myArray.sort {
        $1.Differential > $0.Differential
    }
    var gamesUsed = 0
    var adjUsed = 0.0
    switch myArray.count {
    case 1:
        gamesUsed = 1
        adjUsed = -2.0
    case 2:
        gamesUsed = 1
        adjUsed = -2.0
    case 3:
        gamesUsed = 1
        adjUsed = -2.0
    case 4:
        gamesUsed = 1
        adjUsed = -1.0
    case 5:
        gamesUsed = 1
        adjUsed = 0.0
    case 6:
        gamesUsed = 2
        adjUsed = -1.0
    case 7...8:
        gamesUsed = 2
    case 9...11:
        gamesUsed = 3
    case 12...14:
        gamesUsed = 4
    case 15...16:
        gamesUsed = 5
    case 17...18:
        gamesUsed = 6
    case 19:
        gamesUsed = 7
    default:
        gamesUsed = 8
    }
    var sum =  0.0
    print("RemoveSubrange \(gamesUsed)..<\(myArray.count - 1)")
    myArray.removeSubrange(gamesUsed..<myArray.count)
    for i in 0..<myArray.count {
        sum += myArray[i].Differential
        print("Handi \(myArray[i].GDate) \(myArray[i].Differential) ")
    }

    let (Par, Rating, Slope) = sqlTees().TeeParRatingSlope(teeID: GameRec.TeeID, courseID: GameRec.CourseID)
    let handiIndex: Double = round((Double(sum / Double(gamesUsed)) + adjUsed) * 10) / 10
    let handicapp = Int(handiIndex * (Slope / 113) + (Rating - Par))
    print("(\(handiIndex) * (\(Slope) / 113) + (\(Rating) - \(Par))")
    print("Handi \(handicapp) ... \(handiIndex)")
    return (handicapp, handiIndex, myArray)
}
