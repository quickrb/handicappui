//
//  popDocNames.swift
//  Handisqlite
//
//  Created by Brian Quick on 2020-12-28.
//

import Foundation

struct popDocs {
    let popFairwayEntries: String = "popFairwayEntries"
    let popGreensEntries: String = "popGreensEntries"
    let popNumberEntries: String = "popNumberEntries"
    let popUnknownEntries: String = "popUnknownEntries"
}
