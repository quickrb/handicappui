//
//  ButtonModifiers.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-10.
//

import SwiftUI

struct myButton<textContent: View>: View {

    let action: () -> Void
    let content: textContent
    var width: CGFloat
    var height: CGFloat

    init(action: @escaping () -> Void, @ViewBuilder content: () -> textContent, width: CGFloat = 50, height: CGFloat = 20) {
        self.action = action
        self.content = content()
        self.width = width
        self.height = height
    }
    var body: some View {
        Button(action: action) {
            content
                .myButtonTextStyle()
                .cornerRadius(8)
                .frame(width: width, height: height)
        }
        .myButtonStyle()
    }
}
extension View {
    func hiddenConditionally(isHidden: Bool) -> some View {
        isHidden ? AnyView(self.hidden()) : AnyView(self)
    }
}
struct myButtonTextModifier: ViewModifier {
    func body(content: Content) -> some View {

    GeometryReader { geo in
        content
            .frame(width: geo.size.width, height: geo.size.height, alignment: .center)
    }
            .background(LinearGradient(gradient: Gradient(colors: [.white, .blue]), startPoint: .top, endPoint: .bottom))
            .cornerRadius(8)
    }
}
extension View {
    func myButtonTextStyle() -> some View {
        self.modifier(myButtonTextModifier())
    }
}
struct myButtonModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .buttonStyle(PlainButtonStyle())
            .foregroundColor(.init(Color.RGBColorSpace.sRGB, red: 0.0, green: 0.0, blue: 1.0))
            .padding(6)
            .background(LinearGradient(gradient: Gradient(colors: [Color.blue, Color.white]), startPoint: .top, endPoint: .bottom))
            .cornerRadius(8)
    }
}
extension View {
    func myButtonStyle() -> some View {
        self.modifier(myButtonModifier())
    }
}

struct myBackgroundModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .background(LinearGradient(gradient: Gradient(colors: [.white, .blue ]), startPoint: .top, endPoint: .bottom))
    }
}
extension View {
    func myBackgroundStyle() -> some View {
        self.modifier(myBackgroundModifier())
    }
}
struct myBackgroundModifierRed: ViewModifier {
    func body(content: Content) -> some View {
        content
            .background(LinearGradient(gradient: Gradient(colors: [.white, .red ]), startPoint: .top, endPoint: .bottom))
    }
}
extension View {
    func myBackgroundStyleRed() -> some View {
        self.modifier(myBackgroundModifierRed())
    }
}
struct myRectangle: View {

    var highLight: Bool = false
    var body: some View {
        Rectangle()
            .fill(highLight ? LinearGradient(gradient: Gradient(colors: [.yellow, .blue ]), startPoint: .top, endPoint: .bottom) : LinearGradient(gradient: Gradient(colors: [.white, .blue ]), startPoint: .top, endPoint: .bottom))

        }
    }


