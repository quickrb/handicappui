//
//  PopoverLink.swift
//  NavigationMacos
//
//  Created by Brian Quick on 2021-02-21.
//

import SwiftUI

struct PopoverLink<Destination, Label> : View where Label : View, Destination : View {
// not available on Macos   @Environment(\.horizontalSizeClass) var horizontalSizeClass
    #if !os(macOS)
    @Environment(\.horizontalSizeClass) var horizontalSizeClass
    #endif
private let destination: Destination
private let label: Label
private var isActive: Binding<Bool>?
    private var action: () -> Void = {}
@State private var internalIsActive = false

    public init(destination: Destination, action: @escaping () -> Void, @ViewBuilder label: () -> Label ) {
self.destination = destination
self.label = label()
        self.action = action
    }

/// Creates an instance that presents `destination`.
    public init(destination: Destination, @ViewBuilder label: () -> Label) {
self.destination = destination
self.label = label()
    }
/// Creates an instance that presents `destination` when active.
    public init(destination: Destination, isActive: Binding<Bool>, @ViewBuilder label: () -> Label) {
self.destination = destination
self.label = label()
self.isActive = isActive
    }
private func popoverButton() -> some View {
    myButton(action: {
        (isActive ?? _internalIsActive.projectedValue).wrappedValue = true
        action()
    }, content: {
        label
    }, width: 150, height: 20)
//        Button {
//            (isActive ?? _internalIsActive.projectedValue).wrappedValue = true
//            action()
//        } label: {
//            label
//        }
    }
/// The content and behavior of the view.
    public var body: some View {
        #if os(macOS)
        //  this works as I expect for the sheet
                popoverButton().sheet(isPresented: (isActive ?? _internalIsActive.projectedValue)) {
                    destination
                }
        // FIXME: this should be able to be implemented for a popover or a sheet
//        popoverButton().popover(isPresented: (isActive ?? _internalIsActive.projectedValue)) {
//            destination
//        }
        #endif



        #if !os(macOS)
        if horizontalSizeClass == .compact {
            popoverButton().sheet(isPresented: (isActive ?? _internalIsActive.projectedValue)) {
                destination
            }
        } else {
            popoverButton().popover(isPresented: (isActive ?? _internalIsActive.projectedValue)) {
                destination
            }
        }
        #endif
    }
}
