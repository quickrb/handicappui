//
//  MyDefaults.swift
//  Handisqlite
//
//  Created by Brian Quick on 2020-12-23.
//

import Foundation
    /**
     MyDefaults gets/sets the UserDefaults

     - parameter <#parameterName#>: <#Description#>.
     - returns: appropriate type as stored inthe db
     - warning:

     # Notes: #
     1. If you have to set a value, you must create an occurance
            like
            var myDEF = MyDefaults
     */
class MyDefaults {

    let defaults = UserDefaults.standard

    var appDefaultWidth: CGFloat {
        return 920
    }
    var appDefaultHeight: CGFloat {
        return 520
    }
    var sqliteLocation: String {
        get { return defaults.string(forKey: "sqliteLocation") ?? "unKnown" }
        set { defaults.setValue(newValue, forKey: "sqliteLocation")}
    }
    var appLogging: Bool {
        get { return defaults.bool(forKey: "appLogging") }
        set { defaults.setValue(newValue, forKey: "appLogging")}
    }
    var sqlLogging: Bool {
        get { return defaults.bool(forKey: "sqlLogging") }
        set { defaults.setValue(newValue, forKey: "sqlLogging")}
    }
    var enteringData: Bool {
        get { return defaults.bool(forKey: "enteringData") }
        set { defaults.setValue(newValue, forKey: "enteringData")}
    }
    var printingScoreCard: Bool {
        get { return defaults.bool(forKey: "printingScoreCard") }
        set { defaults.setValue(newValue, forKey: "printingScoreCard")}
    }
    var popScreenName: String {
        get { return defaults.string(forKey: "popScreenName") ?? "" }
        set { defaults.setValue(newValue, forKey: "popScreenName") }
    }
    var playerID: Int {
        get {
            let lastUserID = defaults.integer(forKey: "playerID")
            if lastUserID == 0 {
                return 1
            }
            return lastUserID
            //            return 1
        }
        set { defaults.setValue(newValue, forKey: "playerID") }
    }
    var playerName: String {
        get {
            let lastPlayerName = defaults.string(forKey: "playerName") ?? ""
            if lastPlayerName.isEmpty {
                return "UnKnown"
            }
            return lastPlayerName
        }
        set { defaults.setValue(newValue, forKey: "playerName") }
    }
    var gameID: Int {
        get { return defaults.integer(forKey: "gameID") }
        set { defaults.setValue(newValue, forKey: "gameID") }
    }
    var courseID: Int {
        get { return defaults.integer(forKey: "courseID") }
        set { defaults.setValue(newValue, forKey: "courseID") }
    }
    var teeID: Int {
        get { return defaults.integer(forKey: "teeID") }
        set { defaults.setValue(newValue, forKey: "teeID") }
    }

}
