//
//  ToggleSidebar.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-05-11.
//

import SwiftUI

struct ToggleSidebar: View {
    var body: some View {
        Button(action: toggleSidebar, label: {
            Image(systemName: "sidebar.left")
        })
        .myButtonStyle()
    }
}
func toggleSidebar() {
        NSApp.keyWindow?.firstResponder?.tryToPerform(#selector(NSSplitViewController.toggleSidebar(_:)), with: nil)
}
struct ToggleSidebar_Previews: PreviewProvider {
    static var previews: some View {
        ToggleSidebar()
    }
}
