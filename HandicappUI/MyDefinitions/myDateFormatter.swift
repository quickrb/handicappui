//
//  myDateFormatter.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-24.
//

import SwiftUI

func myDateToString(from myDate: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    return  formatter.string(from: myDate)
}
func myDateTimeToString(from myDate: Date) -> String {
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    return  formatter.string(from: myDate)
}
