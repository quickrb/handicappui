//
//  parColors.swift
//  Handisqlite
//
//  Created by Brian Quick on 2021-01-13.
//

import Foundation
import Cocoa
import SwiftUI

struct ParColorsNS {
    var Score: Int = 0
    var Par: Int = 0
    var Color: NSColor {
        switch Score - Par {
        case -2:
            return NSColor.systemOrange
        case -1:
            return NSColor.systemYellow
        case 0:
            return NSColor.clear
        case 1:
            return NSColor.systemGreen
        case 2:
            return NSColor.systemGray
        case 3:
            return NSColor.systemPurple
        default:
            return NSColor.systemRed
        }
    }
}
    struct ParColorsCG {
        var Score: Int = 0
        var Par: Int = 0
    var CGColor: Color {
        switch Score - Par {
        case -2:
            return Color.orange
        case -1:
            return Color.yellow
        case 0:
            return Color.clear
        case 1:
            return Color.green
        case 2:
            return Color.gray
        case 3:
            return Color.purple
        default:
            return Color.clear
        }
    }
    }
//        struct ParColors {
//            var Score: Int = 0
//            var Par: Int = 0
//        var Color: Color {
//            switch Score - Par {
//            case -2:
//                return .orange
//            case -1:
//                return .yellow
//            case 0:
//                return .clear
//            case 1:
//                return .green
//            case 2:
//                return .gray
//            case 3:
//                return .purple
//            default:
//                return .clear
//            }
//        }
//        }

