//
//  myPopoverLink.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-03-05.
//

import SwiftUI

struct myPopoverLink<Content: View>: View {
    var destinationView: Content
    var text: String
    var action: () -> Void

    init(destinationView: Content, text: String, action: @escaping () -> Void) {
        self.destinationView = destinationView
        self.text = text
        self.action = action
    }
    var body: some View {
        PopoverLink(destination: destinationView, action: { action() }) {
            Text("\(text)")
                .myButtonTextStyle()
        }
        .myButtonStyle()
    }
}
