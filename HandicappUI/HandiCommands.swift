//
//  HandiCommands.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-05-10.
//

import SwiftUI

struct HandiCommands: Commands {
    var body: some Commands {
        SidebarCommands()
        ToolbarCommands()
    }
}
