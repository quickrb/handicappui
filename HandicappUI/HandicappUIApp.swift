//
//  HandicappUIApp.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-02-28.
//

import SwiftUI

@main
struct HandicappUIApp: App {
    @StateObject var scoreclass = ScoreClass()
    @StateObject var parsclass = ParsClass()

    @StateObject var currentPlayer = CurrentPlayer(playerID: MyDefaults().playerID, playerName: MyDefaults().playerName)
    var body: some Scene {
        WindowGroup {
            SwiftUIMenu()
                .environmentObject(scoreclass)
                .environmentObject(parsclass)
                .environmentObject(currentPlayer)
        }

    }
}
