//
//  sqlScores.swift
//  Handisqlite
//
//  Created by Brian Quick on 2020-12-22.
//

import Foundation
import SQLite3

class sqlScores: SqliteDbStore {

    func updateAGamesScores(scores: [ScoreRec]) {
        for score in scores {
            if checkOneScore(score: score) {
                if updateOneScore(score: score) != SQLITE_OK {
                        // FIXME: What to do on an error???
                }
            } else {
                if insertOneScore(score: score) != SQLITE_OK {
                        // FIXME: What to do on an error???
                }
            }
        }
    }
    func insertOneScore(score: ScoreRec) -> Int32 {

        let aSqlStmt = """
                INSERT INTO score (PlayerID, GameID, Hole, Score, Fairway,  Green, Putts, Bunker, Penalty)
                Values (?, ?, ?, ?, ?, ?, ?, ?, ?)
                """

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db) }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(score.PlayerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.insertOneScore - Score)")
            return sqlite3_errcode(db)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(score.GameID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.insertOneScore - GameID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 3, Int32(score.Hole)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.insertOneScore - Hole)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 4, Int32(score.Score)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.insertOneScore - Score)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 5, (score.Fairway as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.insertOneScore - Fairway)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 6, (score.Green as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.insertOneScore - Green)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 7, Int32(score.Putts)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.insertOneScore - Putts)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 8, Int32(score.Bunker))  != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.insertOneScore - Bunker)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 9, Int32(score.Penalty)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.insertOneScore - Penalty)")
            return sqlite3_errcode(db)
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(sqlScore.insertOneScore) \(r)")
            return sqlite3_errcode(db)
        }

        return SQLITE_OK
    }
    func updateOneScore(score: ScoreRec) -> Int32 {

        let aSqlStmt = """
                update score set PlayerID = ?, GameID = ?, Hole = ?, Score = ?, Fairway = ?,  Green = ?, Putts = ?, Bunker = ?, Penalty = ?
                WHERE PlayerID = ? AND GameID = ?  AND Hole = ?
                """

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db) }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(score.PlayerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.updateOneScore - Score)")
            return sqlite3_errcode(db)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(score.GameID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.updateOneScore - GameID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 3, Int32(score.Hole)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.updateOneScore - Hole)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 4, Int32(score.Score)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.updateOneScore - Score)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 5, (score.Fairway as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.updateOneScore - Fairway)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 6, (score.Green as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.updateOneScore - Green)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 7, Int32(score.Putts)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.updateOneScore - Putts)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 8, Int32(score.Bunker))  != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.updateOneScore - Bunker)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 9, Int32(score.Penalty)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.updateOneScore - Penalty)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 10, Int32(score.PlayerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.updateOneScore - PlayerID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 11, Int32(score.GameID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.updateOneScore - GameID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 12, Int32(score.Hole)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlScore.updateOneScore - Hole)")
            return sqlite3_errcode(db)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(sqlScore.updateOneScore) \(r)")
            return sqlite3_errcode(db)
        }

        return SQLITE_OK
    }
    func checkOneScore(score: ScoreRec) -> Bool {

        let aSqlStmt = "SELECT count(*) FROM score WHERE PlayerID = ? AND GameID = ? AND Hole = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else
        {  logDbErr("sqlScores.selectOneScore")
            return true
        }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(score.PlayerID)) != SQLITE_OK {
            logDbErr("sqlScores.checkOneScore bind playerID error")
            return true
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(score.GameID)) != SQLITE_OK {
            logDbErr("sqlScores.checkOneScore bind GameID error")
            return true
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 3, Int32(score.Hole)) != SQLITE_OK {
            logDbErr("sqlScores.checkOneScore bind Hole error")
            return true
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        var myCount : Int64 = 1
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {
             myCount = Int64(sqlite3_column_int64(sqlitePrepareStmt, 0))
        }
        return myCount > 0
    }
    // this is the same as selectAGame with the courseID rather than the gameID
    func selectAllScores(playerID: Int, courseID: Int) -> [ScoreRec] {
        let parAllSqlStmt = """
        SELECT score.PlayerID, score.GameID, Hole, score.Score, Fairway, Green, Putts, Bunker, Penalty, courseID
        FROM Score
        inner join game on game.GameID = score.GameID AND Game.PlayerID = score.PlayerID
        WHERE score.PlayerID = ? AND CourseID = ?
        """
        var scoreRecArray = [ScoreRec]()
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: parAllSqlStmt) == SQLITE_OK
        else {
            return scoreRecArray
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(gamesCountACourseEntryStmt)")
            return scoreRecArray
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(gamesCountACourseEntryStmt)")
            return scoreRecArray
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {


            let PlayerID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            let GameID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
            let Hole = Int(sqlite3_column_int(sqlitePrepareStmt, 2))
            let Score = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
            var Fairway = ""
            if sqlite3_column_type(sqlitePrepareStmt, 4) != SQLITE_NULL {
                Fairway = String(cString: sqlite3_column_text(sqlitePrepareStmt, 4))
            }
            var Green = ""
            if sqlite3_column_type(sqlitePrepareStmt,5) != SQLITE_NULL {
                Green = String(cString: sqlite3_column_text(sqlitePrepareStmt, 5))
            }
            let Putts = Int(sqlite3_column_int(sqlitePrepareStmt, 6))
            let Bunker = Int(sqlite3_column_int(sqlitePrepareStmt, 7))

            //let Bunker = String(cString: sqlite3_column_text(sqlitePrepareStmt, 7))
            let Penalty = Int(sqlite3_column_int(sqlitePrepareStmt, 8))

            scoreRecArray.append( ScoreRec(PlayerID: PlayerID, GameID: GameID, Hole: Hole, Score: Score, Fairway: Fairway, Green: Green, Putts: Putts, Bunker: Bunker, Penalty: Penalty))
        }
        return scoreRecArray
    }
    func selectAGame(playerID: Int, gameID: Int)  -> [ScoreRec]{

        let parAllSqlStmt = "SELECT PlayerID, GameID, Hole, Score, Fairway, Green, Putts, Bunker, Penalty FROM Score WHERE PlayerID = ? AND GameID = ? ORDER BY Hole ASC"
        var scoreRecArray = [ScoreRec]()
        for i in 1...18 {
            var myRec = ScoreRec()
            myRec.PlayerID = playerID
            myRec.GameID = gameID
            myRec.Hole = i
            scoreRecArray.append(myRec)
        }
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: parAllSqlStmt) == SQLITE_OK
        else {
            return scoreRecArray
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(gamesCountACourseEntryStmt)")
            return scoreRecArray
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(gameID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(gamesCountACourseEntryStmt)")
            return scoreRecArray
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {


            let PlayerID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            let GameID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
            let Hole = Int(sqlite3_column_int(sqlitePrepareStmt, 2))
            let Score = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
            var Fairway = ""
            if sqlite3_column_type(sqlitePrepareStmt, 4) != SQLITE_NULL {
                Fairway = String(cString: sqlite3_column_text(sqlitePrepareStmt, 4))
            }
            var Green = ""
            if sqlite3_column_type(sqlitePrepareStmt,5) != SQLITE_NULL {
                Green = String(cString: sqlite3_column_text(sqlitePrepareStmt, 5))
            }
            let Putts = Int(sqlite3_column_int(sqlitePrepareStmt, 6))
            let Bunker = Int(sqlite3_column_int(sqlitePrepareStmt, 7))

            //let Bunker = String(cString: sqlite3_column_text(sqlitePrepareStmt, 7))
            let Penalty = Int(sqlite3_column_int(sqlitePrepareStmt, 8))
            if Hole < 19 {
                scoreRecArray[Hole - 1] =  ScoreRec(PlayerID: PlayerID, GameID: GameID, Hole: Hole, Score: Score, Fairway: Fairway, Green: Green, Putts: Putts, Bunker: Bunker, Penalty: Penalty)
            }
        }

        return scoreRecArray
    }

    func sumGamePuts(gameID: Int) -> Int {

        let aSqlStmt = "select sum(putts) from Score where GameID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else
        {  logDbErr("sqlScores.selectOneScore")
            return 0
        }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(gameID)) != SQLITE_OK {
            logDbErr("sqlScores.sumGamePuts bind gameID error")
            return 0
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        var myCount : Int = 0
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {
             myCount = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
        }
        return myCount
    }

    func deleteAGame(gameID: Int) -> Int32 {
        let aSqlStmt = "DELETE FROM Score WHERE GameID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db)}

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(gameID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(deleteEntryStmt)")
            return sqlite3_errcode(db)
        }
        //executing the query to delete row
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(deleteEntryStmt) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func scoreCardStatistics(scores: [ScoreRec]) -> ScoreCardStatsRec {
        let myStats = ScoreCardStatsRec(frontScore: 0, backScore: 0, totalScore: 0, frontPutts: 0, backPutts: 0, totalPutts: 0, frontFairway: 0, backFairway: 0, totalFairway: 0, frontGreen: 0, backGreen: 0, totalGreen: 0, frontBunker: 0, backBunker: 0, totalBunker: 0, frontPenalty: 0, backPenalty: 0, totalPenalty: 0)
        if scores.count == 0 { return myStats }
        for rec in 0..<scores.count {
            if scores[rec].Hole < 10 {
                myStats.frontScore += scores[rec].Score
            } else {
                myStats.backScore += scores[rec].Score
            }
        }
        myStats.frontScore = scores.filter({ $0.Hole < 10 }).map({ $0.Score }).reduce(0, +)
        myStats.totalScore = scores.reduce(0, {$0 + $1.Score})
        myStats.totalPutts = scores.reduce(0, {$0 + $1.Putts})
        return myStats
    }
}

