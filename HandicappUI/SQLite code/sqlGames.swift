//
//  sqlGames.swift
//  Handisqlite
//
//  Created by Brian Quick on 2020-10-10.
//

import Foundation
import SQLite3

class  sqlGames: SqliteDbStore {
    func selectAllCourseNameWithScores(playerID: Int) -> [String] {
        let aSqlStmt = """
        SELECT  Course.CourseName, Tee.tee, sum(score.score) as myScore FROM Game
        INNER JOIN Course ON Game.CourseID = Course.CourseID
        INNER JOIN Tee on Game.CourseID = Tee.CourseID AND Game.TeeID = Tee.TeeID
        INNER JOIN score on Game.GameID = Score.GameID
        WHERE game.PlayerID = ?
        GROUP BY   CourseName
        order by myScore DESC
        """


        var myArray = [String]()

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return myArray }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.selectAllGamesWithScores ... playerID)")
            return myArray
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            let CourseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 0))
            let TeeName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 1))
            let sumScore = sqlite3_column_double(sqlitePrepareStmt, 2)
            if sumScore > 0 {
            myArray.append(CourseName)
            }
        }

        return myArray
    }
    func selectAllGamesWithScores(playerID: Int, courseID: Int) -> [GameRec] {
        let aSqlStmt = """
        SELECT Game.PlayerID, Game.GameID, Gdate, Game.CourseID, Game.TeeID, Game.Score, ScoreAdj, Handicapp, HandiIndex, Differential, Course.CourseName, Tee.tee, sum(score.score) as myScore FROM Game
        INNER JOIN Course ON Game.CourseID = Course.CourseID
        INNER JOIN Tee on Game.CourseID = Tee.CourseID AND Game.TeeID = Tee.TeeID
        INNER JOIN score on Game.GameID = Score.GameID
        WHERE game.PlayerID = ? AND game.CourseID = ?
        GROUP BY Game.GameID
        order by Game.GDate DESC, Game.GameID DESC
        """


        var myArray = [GameRec]()

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return myArray }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.selectAllGamesWithScores ... playerID)")
            return myArray
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.selectAllGamesWithScores ... courseID)")
            return myArray
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            let PlayerID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            let GameID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
            let GDate = String(cString: sqlite3_column_text(sqlitePrepareStmt, 2))
            let CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
            let TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 4))
            let Score = Int(sqlite3_column_int(sqlitePrepareStmt, 5))
            let ScoreAdj = Int(sqlite3_column_int(sqlitePrepareStmt, 6))
            let Handicapp = Int(sqlite3_column_int(sqlitePrepareStmt, 7))
            let HandiIndex = sqlite3_column_double(sqlitePrepareStmt, 8)
            let Differential = sqlite3_column_double(sqlitePrepareStmt, 9)
            let CourseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 10))
            let TeeName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 11))
            let sumScore = sqlite3_column_double(sqlitePrepareStmt, 12)
            if sumScore > 0 {
            myArray.append(GameRec(PlayerID: PlayerID, GameID: GameID, GDate: GDate, CourseID: CourseID, TeeID: TeeID, Score: Score, ScoreAdj: ScoreAdj, Handicapp: Handicapp, HandiIndex: HandiIndex, Differential: Differential, CourseName: CourseName, TeeName: TeeName))
            }
        }

        return myArray
    }
    func selectAllGamesWithScores(playerID: Int) -> [GameRec] {
        let aSqlStmt = """
        SELECT Game.PlayerID, Game.GameID, Gdate, Game.CourseID, Game.TeeID, Game.Score, ScoreAdj, Handicapp, HandiIndex, Differential, Course.CourseName, Tee.tee, sum(score.score) as myScore FROM Game
        INNER JOIN Course ON Game.CourseID = Course.CourseID
        INNER JOIN Tee on Game.CourseID = Tee.CourseID AND Game.TeeID = Tee.TeeID
        INNER JOIN score on Game.GameID = Score.GameID
        WHERE game.PlayerID = ?
        GROUP BY Game.GameID
        order by Game.GDate DESC, Game.GameID DESC
        """


        var myArray = [GameRec]()

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return myArray }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... GameID)")
            return myArray
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            let PlayerID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            let GameID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
            let GDate = String(cString: sqlite3_column_text(sqlitePrepareStmt, 2))
            let CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
            let TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 4))
            let Score = Int(sqlite3_column_int(sqlitePrepareStmt, 5))
            let ScoreAdj = Int(sqlite3_column_int(sqlitePrepareStmt, 6))
            let Handicapp = Int(sqlite3_column_int(sqlitePrepareStmt, 7))
            let HandiIndex = sqlite3_column_double(sqlitePrepareStmt, 8)
            let Differential = sqlite3_column_double(sqlitePrepareStmt, 9)
            let CourseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 10))
            let TeeName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 11))
            let sumScore = sqlite3_column_double(sqlitePrepareStmt, 12)
            if sumScore > 0 {
            myArray.append(GameRec(PlayerID: PlayerID, GameID: GameID, GDate: GDate, CourseID: CourseID, TeeID: TeeID, Score: Score, ScoreAdj: ScoreAdj, Handicapp: Handicapp, HandiIndex: HandiIndex, Differential: Differential, CourseName: CourseName, TeeName: TeeName))
            }
        }

        return myArray
    }




    func selectAllGames(playerID: Int) -> [GameRec] {
        let aSqlStmt = """
        SELECT PlayerID, GameID, Gdate, Game.CourseID, Game.TeeID, Score, ScoreAdj, Handicapp, HandiIndex, Differential, Course.CourseName, Tee.tee FROM Game
        INNER JOIN Course ON Game.CourseID = Course.CourseID
        INNER JOIN Tee on Game.CourseID = Tee.CourseID AND Game.TeeID = Tee.TeeID
        WHERE PlayerID = ? order by Gdate DESC, gameID DESC
        """


        var myArray = [GameRec]()

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return myArray }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... GameID)")
            return myArray
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            let PlayerID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            let GameID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
            let GDate = String(cString: sqlite3_column_text(sqlitePrepareStmt, 2))
            let CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
            let TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 4))
            let Score = Int(sqlite3_column_int(sqlitePrepareStmt, 5))
            let ScoreAdj = Int(sqlite3_column_int(sqlitePrepareStmt, 6))
            let Handicapp = Int(sqlite3_column_int(sqlitePrepareStmt, 7))
            let HandiIndex = sqlite3_column_double(sqlitePrepareStmt, 8)
            let Differential = sqlite3_column_double(sqlitePrepareStmt, 9)
            let CourseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 10))
            let TeeName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 11))
            myArray.append(GameRec(PlayerID: PlayerID, GameID: GameID, GDate: GDate, CourseID: CourseID, TeeID: TeeID, Score: Score, ScoreAdj: ScoreAdj, Handicapp: Handicapp, HandiIndex: HandiIndex, Differential: Differential, CourseName: CourseName, TeeName: TeeName))
        }

        return myArray
    }
    func selectPlayersLastGame(playerID: Int, courseID: Int, teeID: Int) -> GameRec {
        let aSqlStmt = """
        SELECT PlayerID, GameID, Gdate, Game.CourseID, Game.TeeID, Score, ScoreAdj, Handicapp, HandiIndex, Differential, Course.CourseName, Tee.tee FROM Game
        INNER JOIN Course ON Game.CourseID = Course.CourseID
        INNER JOIN Tee on Game.CourseID = Tee.CourseID AND Game.TeeID = Tee.TeeID
        WHERE PlayerID = ? AND Game.CourseID = ? AND Game.TeeID = ? order by Game.GDate DESC,  GameID DESC  limit 1
        """

        var myGameRec = GameRec(PlayerID: playerID, GameID: 0, GDate: "2021-01-01", CourseID: courseID, TeeID: teeID, Score: 0, ScoreAdj: 0, Handicapp: 0, HandiIndex: 0, Differential: 0.0, CourseName: "", TeeName: "")

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return myGameRec }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... GameID)")
            return myGameRec
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... GameID)")
            return myGameRec
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 3, Int32(teeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... GameID)")
            return myGameRec
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        if sqlite3_step(sqlitePrepareStmt) != SQLITE_ROW {
            // just return an empty record is there is nothing there
            // logDbErr("sqlite3_step sqlGames.selectGameRec:")
            return myGameRec
        }

        myGameRec.PlayerID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
        myGameRec.GameID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
        myGameRec.GDate = String(cString: sqlite3_column_text(sqlitePrepareStmt, 2))
        myGameRec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
        myGameRec.TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 4))
        myGameRec.Score = Int(sqlite3_column_int(sqlitePrepareStmt, 5))
        myGameRec.ScoreAdj = Int(sqlite3_column_int(sqlitePrepareStmt, 6))
        myGameRec.Handicapp = Int(sqlite3_column_int(sqlitePrepareStmt, 7))
        myGameRec.HandiIndex = sqlite3_column_double(sqlitePrepareStmt, 8)
        myGameRec.Differential = sqlite3_column_double(sqlitePrepareStmt, 9)
        myGameRec.CourseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 10))
        myGameRec.TeeName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 11))
        return myGameRec
    }
    func selectPlayersLastGame(playerID: Int) -> GameRec {
        let aSqlStmt = """
        SELECT PlayerID, GameID, Gdate, Game.CourseID, Game.TeeID, Score, ScoreAdj, Handicapp, HandiIndex, Differential, Course.CourseName, Tee.tee FROM Game
        INNER JOIN Course ON Game.CourseID = Course.CourseID
        INNER JOIN Tee on Game.CourseID = Tee.CourseID AND Game.TeeID = Tee.TeeID
        WHERE PlayerID = ? order by Game.GDate DESC,  GameID DESC  limit 1
        """

        var myGameRec = GameRec(PlayerID: 0, GameID: 0, GDate: "2021-01-01", CourseID: 0, TeeID: 0, Score: 0, ScoreAdj: 0, Handicapp: 0, HandiIndex: 0, Differential: 0.0, CourseName: "", TeeName: "")

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return myGameRec }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... GameID)")
            return myGameRec
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        if sqlite3_step(sqlitePrepareStmt) != SQLITE_ROW {
            logDbErr("sqlite3_step sqlGames.selectGameRec:")
            return myGameRec
        }

        myGameRec.PlayerID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
        myGameRec.GameID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
        myGameRec.GDate = String(cString: sqlite3_column_text(sqlitePrepareStmt, 2))
        myGameRec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
        myGameRec.TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 4))
        myGameRec.Score = Int(sqlite3_column_int(sqlitePrepareStmt, 5))
        myGameRec.ScoreAdj = Int(sqlite3_column_int(sqlitePrepareStmt, 6))
        myGameRec.Handicapp = Int(sqlite3_column_int(sqlitePrepareStmt, 7))
        myGameRec.HandiIndex = sqlite3_column_double(sqlitePrepareStmt, 8)
        myGameRec.Differential = sqlite3_column_double(sqlitePrepareStmt, 9)
        myGameRec.CourseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 10))
        myGameRec.TeeName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 11))
        return myGameRec
    }
    func selectGameRec(gameID: Int) -> GameRec {
        let aSqlStmt = """
        SELECT PlayerID, GameID, Gdate, Game.CourseID, Game.TeeID, Score, ScoreAdj, Handicapp, HandiIndex, Differential, Course.CourseName, Tee.tee FROM Game
        INNER JOIN Course ON Game.CourseID = Course.CourseID
        INNER JOIN Tee on Game.CourseID = Tee.CourseID AND Game.TeeID = Tee.TeeID
                WHERE GameID = ?
        """

        var myGameRec = GameRec(PlayerID: 1, GameID: 0, GDate: "2021-01-04", CourseID: 1, TeeID: 1, Score: 0, ScoreAdj: 0, Handicapp: 0, HandiIndex: 0, Differential: 0.0, CourseName: "", TeeName: "")

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return myGameRec }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(gameID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... GameID)")
            return myGameRec
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        if sqlite3_step(sqlitePrepareStmt) != SQLITE_ROW {
            // no record found and that is ok
            return myGameRec
        }

        myGameRec.PlayerID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
        myGameRec.GameID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
        myGameRec.GDate = String(cString: sqlite3_column_text(sqlitePrepareStmt, 2))
        myGameRec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
        myGameRec.TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 4))
        myGameRec.Score = Int(sqlite3_column_int(sqlitePrepareStmt, 5))
        myGameRec.ScoreAdj = Int(sqlite3_column_int(sqlitePrepareStmt, 6))
        myGameRec.Handicapp = Int(sqlite3_column_int(sqlitePrepareStmt, 7))
        myGameRec.HandiIndex = sqlite3_column_double(sqlitePrepareStmt, 8)
        myGameRec.Differential = sqlite3_column_double(sqlitePrepareStmt, 9)
        myGameRec.CourseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 10))
        myGameRec.TeeName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 11))
        return myGameRec
    }
    func deleteAGame(gameID: Int) -> Int32 {
        let aSqlStmt = "DELETE FROM Game WHERE GameID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db)}

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(gameID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(deleteEntryStmt)")
            return sqlite3_errcode(db)
        }
        //executing the query to delete row
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(deleteEntryStmt) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func insert(game: GameRec) -> Int32 {

        let aSqlStmt = """
                INSERT INTO Game (PlayerID, Gdate, CourseID, TeeID, Score, ScoreAdj, Handicapp, HandiIndex, Differential)
                Values (?, ?, ?, ?, ?, ?, ?, ?, ?)
                """

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db) }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(game.PlayerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... PlayerID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 2, (game.GDate as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlite3_bind_text(sqlGames.insert ... GDate )")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 3, Int32(game.CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... CourseID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 4, Int32(game.TeeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... TeeID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 5, Int32(game.Score)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... Score)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 6, Int32(game.ScoreAdj)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... ScoreAdj)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 7, Int32(game.Handicapp)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... ScoreAdj)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_double(self.sqlitePrepareStmt, 8, game.HandiIndex) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_double(self.sqlitePrepareStmt, 9, game.Differential) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(sqlScore.insert) \(r)")
            return sqlite3_errcode(db)
        }

        return SQLITE_OK
    }
    func update(game: GameRec) -> Int32 {

        let aSqlStmt = """
                Update Game SET PlayerID = ?, Gdate = ?, CourseID = ?, TeeID = ?, Score = ?, ScoreAdj = ?, Handicapp = ?, HandiIndex = ?, Differential = ?
                    WHERE GameID = ?
                """

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db) }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(game.PlayerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... PlayerID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 2, (game.GDate as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlite3_bind_text(sqlGames.insert ... GDate )")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 3, Int32(game.CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... CourseID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 4, Int32(game.TeeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... TeeID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 5, Int32(game.Score)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... Score)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 6, Int32(game.ScoreAdj)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... ScoreAdj)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 7, Int32(game.Handicapp)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... ScoreAdj)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_double(self.sqlitePrepareStmt, 8, game.HandiIndex) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_double(self.sqlitePrepareStmt, 9, game.Differential) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 10, Int32(game.GameID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... PlayerID)")
            return sqlite3_errcode(db)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(sqlScore.insert) \(r)")
            return sqlite3_errcode(db)
        }

        return SQLITE_OK
    }
    func GetLastGameID()  -> Int32 {

        let aSqlStmt = "SELECT GameID from Game ORDER BY GameID DESC LIMIT 1"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db) }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        var gameid : Int32 = 1
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {
             gameid = sqlite3_column_int(sqlitePrepareStmt, 0)
        }
        return gameid
    }
    /**
     Count the number of games that have been played on a course.  Used before allowing a delete

     - parameter CourseID: Int
     - returns: Int64 of the count of games
     - warning:

     # Notes: #
     1. Why an Int64??
     */
    func CountACourse(CourseID: Int) throws -> Int64 {

        let aSqlStmt = "SELECT count(*) FROM Game WHERE CourseID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else
        { throw SqliteError(message: "Error in gamesCountACourseEntryStmt") }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        // MARK: Select statement

        //          Inserting CourseID in readEntryStmt prepared statement
       // if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (CourseID as NSString).utf8String, -1, nil) != SQLITE_OK {
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(CourseID)) != SQLITE_OK {

            logDbErr("sqlite3_bind_text(gamesCountACourseEntryStmt)")
            throw SqliteError(message: "Error in  value in CountACourseEntryStmt")
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        var myCount : Int64 = 1
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {
             myCount = Int64(sqlite3_column_int64(sqlitePrepareStmt, 0))
        }
        return myCount
    }
    func CountATee(CourseID: Int, TeeID: Int) throws -> Int64 {

        let aSqlStmt = "SELECT count(*) FROM Game WHERE CourseID = ? AND TeeID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else
        { throw SqliteError(message: "Error in sqlGames.CountATee") }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_in(sqlGames.CountATee)")
            throw SqliteError(message: "Error in value in sqlGames.CountATee")
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(TeeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.CountATee)")
            throw SqliteError(message: "Error in value in sqlGames.CountATee")
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        var myCount : Int64 = 1
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {
             myCount = Int64(sqlite3_column_int64(sqlitePrepareStmt, 0))
        }
        return myCount
    }
    func CountPlayerGames(PlayerID: Int) throws -> Int64 {

        let aSqlStmt = "SELECT count(*) FROM Game WHERE PlayerID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else
        { throw SqliteError(message: "Error in sqlGames.CountPlayerGames") }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(PlayerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(gamesCountACourseEntryStmt)")
            throw SqliteError(message: "Error in  value in sqlGames.CountPlayerGames")
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        var myCount : Int64 = 1
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {
             myCount = Int64(sqlite3_column_int64(sqlitePrepareStmt, 0))
        }
        return myCount
    }
    func differentialScore(GameRec: GameRec) -> DifferentialRec {
        let aSqlStmt = """
            SELECT Par, Rating, Slope from tee
                where CourseID = ? AND TeeID = ?
        """

        let mydifferentialRec = DifferentialRec()

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return mydifferentialRec }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(GameRec.CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... GameID)")
            return mydifferentialRec
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(GameRec.TeeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... GameID)")
            return mydifferentialRec
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        if sqlite3_step(sqlitePrepareStmt) != SQLITE_ROW {
            logDbErr("sqlite3_step sqlGames.selectGameRec:")
            return mydifferentialRec
        }

        mydifferentialRec.PlayerID = GameRec.PlayerID
        mydifferentialRec.GameID = GameRec.GameID
        mydifferentialRec.GDate = GameRec.GDate
        mydifferentialRec.CourseID = GameRec.CourseID
        mydifferentialRec.TeeID = GameRec.TeeID
        mydifferentialRec.Par = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
        mydifferentialRec.Rating = sqlite3_column_double(sqlitePrepareStmt, 1)
        mydifferentialRec.Slope = sqlite3_column_double(sqlitePrepareStmt, 2)
        mydifferentialRec.ScoreAdj = GameRec.ScoreAdj

        return mydifferentialRec
    }
    func differential20Scores(GameRec: GameRec) -> [DifferentialRec] {
        let aSqlStmt = """
            SELECT  Game.PlayerID,
                    Game.GameID,
                    Game.Gdate,
                    Game.CourseID,
                    Game.TeeID,
                    tee.Par,
                    tee.Rating,
                    tee.Slope,
                    Game.ScoreAdj,
                    game.Differential
            from Game
            INNER join tee on game.CourseID = tee.CourseID AND game.TeeID = tee.TeeID
            where  game.PlayerID = ? AND game.GDate <= ? ORDER by Game.GDate DESC LIMIT 20
        """

        var myArray = [DifferentialRec]()
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return myArray }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(GameRec.PlayerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlGames.insert ... GameID)")
            return myArray
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 2, (GameRec.GDate as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlite3_bind_text(sqlGames.insert ... GDate )")
            return myArray
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {
            let mydifferentialRec = DifferentialRec()

            mydifferentialRec.PlayerID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            mydifferentialRec.GameID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
            mydifferentialRec.GDate = String(cString: sqlite3_column_text(sqlitePrepareStmt, 2))
            mydifferentialRec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
            mydifferentialRec.TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 4))
            mydifferentialRec.Par = Int(sqlite3_column_int(sqlitePrepareStmt, 5))
            mydifferentialRec.Rating = sqlite3_column_double(sqlitePrepareStmt, 6)
            mydifferentialRec.Slope = sqlite3_column_double(sqlitePrepareStmt, 7)
            mydifferentialRec.ScoreAdj = Int(sqlite3_column_int(sqlitePrepareStmt, 8))
            mydifferentialRec.Differential = sqlite3_column_double(sqlitePrepareStmt, 9)
            myArray.append(mydifferentialRec)
        }
        return myArray
    }
    func playedLastCourseTeeName(playerID: Int) -> (courseName: String, teeName: String) {

            var courseName: String = ""
            var teeName: String = ""


            let aSqlStmt = """
            select Course.CourseName, Tee.Tee, Game.GameID from game
                    INNER JOIN Course on course.CourseID = game.CourseID
                    INNER JOIN Tee on game.TeeID = tee.TeeID AND Game.CourseID = tee.CourseID
                    WHERE game.PlayerID = ?
                    ORDER by Game.GameID DESC
                    LIMIT 1
            """

            guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
            else {
                return (courseName, teeName)
            }
            defer {
                resetFinalize(stmt: self.sqlitePrepareStmt)
            }
            if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
                logDbErr("sqlite3_bind_text(playedLastCourseTeeName)")
                return (courseName, teeName)
            }

            _ = logDbSQL(stmt: &sqlitePrepareStmt)
            if sqlite3_step(sqlitePrepareStmt) != SQLITE_ROW {
                logDbErr("nothing returned playedLastCourseTeeName:")
                return (courseName, teeName)
            }

            courseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 0))
            teeName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 1))

        return (courseName: courseName, teeName: teeName)
        }
    func playedCourseTeeName(playerID: Int, gameID: Int) -> (courseName: String, teeName: String) {

        var courseName: String = ""
        var teeName: String = ""


        let aSqlStmt = """
        select Course.CourseName, Tee.Tee from game
        INNER JOIN Course on course.CourseID = game.CourseID
        INNER JOIN Tee on game.TeeID = tee.TeeID AND Game.CourseID = tee.CourseID
        WHERE game.PlayerID = ? AND game.GameID = ?
        """

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return (courseName, teeName)
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(playedCourseTeeName)")
            return (courseName, teeName)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(gameID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(playedCourseTeeName)")
            return (courseName, teeName)
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        if sqlite3_step(sqlitePrepareStmt) != SQLITE_ROW {
            logDbErr("nothing returned playedCourseTeeName:")
            return (courseName, teeName)
        }

        courseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 0))
        teeName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 1))

    return (courseName: courseName, teeName: teeName)
    }
}
