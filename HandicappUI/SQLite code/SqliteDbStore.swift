//
//  SqliteDbStore.swift
//  SqliteIntegration
//
//  Created by Ayush Gupta on 1/18/19.
//  Copyright © 2019 Ayush Gupta. All rights reserved.
// ///Users/rbq/Library/Containers/com.codewithbrian.macstoryswiftUI
//
//  the actual path is
//      /Users/rbq/Library/Containers/macstoryswiftUI

import Foundation
import os.log
import SQLite3
import Cocoa

class SqliteDbStore {
    // Get the URL to db store file
    let dbURL: URL
    // The database pointer.
    var db: OpaquePointer?
    var lastErrMsg : String = ""
    // Prepared statement https://www.sqlite.org/c3ref/stmt.html to insert an event into Table.
    // we use prepared statements for efficiency and safe guard against sql injection.
    // there is only one PrepareStmt since it is destroyed after each use
    var sqlitePrepareStmt: OpaquePointer?

    let oslog = OSLog(subsystem: "codewithbrian", category: "handisqlite")

    private var dbName: String = "Handicap.sqlite"
    deinit {
        sqlite3_close(db)
        if MyDefaults().sqlLogging {
            _ = myLog("CLOSED")
        }
    }
    init() {

        do {
            do {


/* this block worked for relative to this application because the app is sandboxed*/
                dbURL = try FileManager.default
                    .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    .appendingPathComponent("HandicappUI")
                    .appendingPathComponent("HandiCap.sqlite")

                //os_log("URL: %s", dbURL.absoluteString)
               // print(dbURL.absoluteString)
            } catch {
                //TODO: Just logging the error and returning empty path URL here. Handle the error gracefully after logging
                os_log("Some error occurred. Returning empty path.")
                dbURL = URL(fileURLWithPath: "")
                return
            }
            MyDefaults().sqliteLocation = dbURL.absoluteString
            try openDB()

            } catch {
                //TODO: Handle the error gracefully after logging
                os_log("Some error occurred. Returning.")
                return
            }


    }
    func getDocumentsDirectory() -> URL {
        // find all possible documents directories for this user
        let paths = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask  )
        _ = myLog(String(paths.count))
        for path in paths {
            print(path.absoluteURL)
        }
        // just send back the first one, which ought to be the only one
        return paths[0]
    }
    /// Command: sqlite3_open(dbURL.path, &db)
    /// Open the DB at the given path. If file does not exists, it will create one for you
    func openDB() throws {

        if sqlite3_open_v2(dbURL.path, &db, SQLITE_OPEN_READWRITE, nil) != SQLITE_OK { // error mostly because of corrupt database
            createFromBundle()
            if db == nil {
            let errmsg = String(cString: sqlite3_errmsg(db))
            os_log("error opening database at %s - %s", log: oslog, type: .error, dbURL.absoluteString, errmsg)
//            deleteDB(dbURL: dbURL)
            throw SqliteError(message: "error opening database \(dbURL.absoluteString)")
            }
        }
        if MyDefaults().sqlLogging {
            _ = myLog("OPEN")
        }
    }
    func createFromBundle() {
        var fileURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("HandicappUI")


        if !FileManager.default.fileExists(atPath: fileURL.absoluteString) {
            try! FileManager.default.createDirectory(at: fileURL, withIntermediateDirectories: true, attributes: nil)
        }

                 fileURL = FileManager.default.homeDirectoryForCurrentUser
                    .appendingPathComponent("Documents")
                    .appendingPathComponent("HandicappUI")
                    .appendingPathComponent(dbName)


        MyDefaults().sqliteLocation = fileURL.absoluteString


        // see if db is in app support directory already
        if sqlite3_open_v2(fileURL.path, &db, SQLITE_OPEN_READWRITE, nil) == SQLITE_OK {
                    print(fileURL.path)
                    return
                }
        // clean up before proceeding
        sqlite3_close(db)
        db = nil
                // if not, get URL from bundle
        guard let bundleURL =  Bundle.main.url(forResource: "Handicap", withExtension: "sqlite") else {
            print("db not found in bundle")
            return
        }

        // copy from bundle to app support directory
        do {
            try FileManager.default.copyItem(at: bundleURL, to: fileURL)
        } catch {
            print("unable to copy db", error.localizedDescription)
            return
        }

        // now open database again
        guard sqlite3_open_v2(fileURL.path, &db, SQLITE_OPEN_READWRITE, nil) == SQLITE_OK else {
            logDbErr("Error opening database", alert: true)
            sqlite3_close(db)
            db = nil
            return
        }

        // report success
        print("db copied and opened ok")
        return
    }
    
//    // Code to delete a db file. Useful to invoke in case of a corrupt DB and re-create another
//    func deleteDB(dbURL: URL) {
//        os_log("removing db", log: oslog)
//        do {
//            try FileManager.default.removeItem(at: dbURL)
//        } catch {
//            os_log("exception while removing db %s", log: oslog, error.localizedDescription)
//        }
//    }
    
    func createTables() throws {
        // create the tables if they dont exist.
        
        // create the table to store the entries.
        // ID | Name | Employee Id | Designation
        //let ret =  sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS Records (id INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, Name TEXT NOT NULL, EmployeeID TEXT UNIQUE NOT NULL, Designation TEXT NOT NULL)", nil, nil, nil)
        let ret = sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS Courses (CourseName TEXT NOT NULL, CourseID INTEGER)", nil, nil, nil)
        if (ret != SQLITE_OK) { // corrupt database.
            logDbErr("Error creating db table - Records")
            throw SqliteError(message: "unable to create table Records")
        }
        
    }

    func prepareEntryStmt( stmt: inout OpaquePointer?, sql: String)  -> Int32 {
        //rbq guard stmt == nil else {return SQLITE_OK}
        //the above statement is in when there was a unique pointer for every statement
        //  not there are all finalized after use
        let r = sqlite3_prepare_v2(db, sql, -1, &stmt, nil)
        if r != SQLITE_OK {
            logDbErr("sqlite3_prepare readEntryStmt")
            let x = String(cString: sqlite3_errmsg(db))
            logDbErr(x)
        }
        return r
    }

    func resetFinalize(stmt: OpaquePointer?) {
        sqlite3_reset(self.sqlitePrepareStmt)
        sqlite3_finalize(self.sqlitePrepareStmt)
    }

    func logDbErr(_ msg: String, alert: Bool? = true) {
        lastErrMsg = String(cString: sqlite3_errmsg(db)!)
        os_log("ERROR %s : %s", log: oslog, type: .error, msg, lastErrMsg)
        if alert == true {
            alertDbErr(msg)
        }
    }
    func alertDbErr(_ msg: String){
        let ac = NSAlert()
        ac.messageText = msg + "\n\n" + lastErrMsg
        ac.alertStyle = .warning
        ac.runModal()
    }
    func logDbSQL(stmt: inout  OpaquePointer?) -> Int32 {
        if !MyDefaults().sqlLogging { return SQLITE_OK }
        guard let expandedSQL = sqlite3_expanded_sql(stmt) else { return SQLITE_FAIL }
        let msg = String(cString: expandedSQL)
        os_log("SQL -- %s ", log: oslog,  msg)
        return SQLITE_OK
    }
}

// Indicates an exception during a SQLite Operation.
class SqliteError : Error {
    var message = ""
    var error = SQLITE_ERROR
    init(message: String = "") {
        self.message = message
    }
    init(error: Int32) {
        self.error = error
    }
}
