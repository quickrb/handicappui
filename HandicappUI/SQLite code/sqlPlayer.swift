//
//  sqlPlayer.swift
//  Handisqlite
//
//  Created by Brian Quick on 2020-12-21.
//

import Foundation
import SQLite3

class sqlPlayer: SqliteDbStore {

    func CountID(ID: Int ) -> Int64 {

        let aSqlStmt = "SELECT count(*) FROM Player WHERE PlayerID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else
        { return Int64(sqlite3_errcode(db)) }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(ID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(playedCourseName - name)")
            return Int64(sqlite3_errcode(db))
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        var myCount : Int64 = 1
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {
             myCount = Int64(sqlite3_column_int64(sqlitePrepareStmt, 0))
        }
        return myCount
    }
    func delete(PlayerID: Int) -> Int32{

        let aSqlStmt = "DELETE FROM Player WHERE PlayerID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db)}

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(PlayerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(deleteEntryStmt)")
            return sqlite3_errcode(db)
        }

        //executing the query to delete row
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(sqlPlayer.delete) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func name(PlayerID: Int) -> String {
        let aSqlStmt = "SELECT Name FROM Player WHERE PlayerID = ?"

        var aName = MyErrors.unKnown
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return aName
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(PlayerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(playedCourseName - name)")
            return (aName)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            aName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 0))

        }
        _ = myLog(aName)
        return aName
    }
    func selectAPlayer(playerID: Int)  -> PlayerRec {

        var myplayerRec = PlayerRec(Name: "unKnown", PlayerID: 0, Level: 0, Hdate: "20210101", Handicap: 0, Hindex: 0.0)

        let aSqlStmt = "SELECT name, PlayerID, Level, HDate, Handicap, Hindex from Player WHERE PlayerID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return myplayerRec
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(gamesCountACourseEntryStmt)")
            return myplayerRec
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        if sqlite3_step(sqlitePrepareStmt) != SQLITE_ROW {
            logDbErr("sqlite3_step COUNT* readEntryStmt:")
            return myplayerRec
        }

        myplayerRec.Name = String(cString: sqlite3_column_text(sqlitePrepareStmt, 0))
        myplayerRec.PlayerID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
        myplayerRec.Level = Int(sqlite3_column_int(sqlitePrepareStmt, 2))
        myplayerRec.Hdate = String(cString: sqlite3_column_text(sqlitePrepareStmt, 3))
        myplayerRec.Handicap = Int(sqlite3_column_int(sqlitePrepareStmt, 4))
        myplayerRec.Hindex = Double(sqlite3_column_double(sqlitePrepareStmt, 5))

        return myplayerRec
    }
    func selectPlayers() -> [PlayerRec] {
          var players = [PlayerRec]()
          let aSqlStmt = "SELECT Name, PlayerID, Level, Hdate, Handicap, Hindex FROM Player ORDER BY PlayerID ASC"

          guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
          else {

              return players
          }
          defer {
              resetFinalize(stmt: self.sqlitePrepareStmt)
          }
          while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {
              let myplayer: PlayerRec = PlayerRec(
                  Name: String(cString: sqlite3_column_text(sqlitePrepareStmt, 0)),
                  PlayerID: Int(sqlite3_column_int(sqlitePrepareStmt, 1)),
                  Level: Int(sqlite3_column_int(sqlitePrepareStmt, 2)),
                  Hdate: String(cString: sqlite3_column_text(sqlitePrepareStmt, 3)),
                  Handicap: Int(sqlite3_column_int(sqlitePrepareStmt, 4)),
                  Hindex: Double(sqlite3_column_double(sqlitePrepareStmt, 5))
              )

              players.append(myplayer)
          }
          return players
      }
    func insert(player: PlayerRec ) -> Int32 {
        let aSqlStmt = "INSERT INTO Player (Name, Level, Hdate, Handicap, Hindex) VALUES (?,?,?,?,?)"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return sqlite3_errcode(db)
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (player.Name as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlplayer.update - Name)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2,  Int32(player.Level)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlplayer.update - Level")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 3, (player.Hdate as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlplayer.update - Hdate)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 4,  Int32(player.Handicap)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlplayer.update - Handicap")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_double(self.sqlitePrepareStmt, 5, player.Hindex) != SQLITE_OK {
            logDbErr("sqlite3_bind_double(sqlplayer.update - Hindex")
            return sqlite3_errcode(db)
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(sqlplayer.update) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
}
    func update(player: PlayerRec ) -> Int32 {
        let aSqlStmt = "UPDATE Player SET Name = ?, Level = ?, Hdate = ?, Handicap = ?, Hindex = ? WHERE PlayerID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return sqlite3_errcode(db)
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (player.Name as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlplayer.update - Name)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2,  Int32(player.Level)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlplayer.update - Level")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 3, (player.Hdate as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlplayer.update - Hdate)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 4,  Int32(player.Handicap)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlplayer.update - Handicap")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_double(self.sqlitePrepareStmt, 5, player.Hindex) != SQLITE_OK {
            logDbErr("sqlite3_bind_double(sqlplayer.update - Hindex")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 6, Int32(player.PlayerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlplayer.update - PlayerID")
            return sqlite3_errcode(db)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(courseUpdateEntryStmt) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
}
    func updateHandicap(PlayerID: Int, Hdate: String, Handicap: Int, Hindex: Double ) -> Int32 {
        let aSqlStmt = "UPDATE Player SET Hdate = ?, Handicap = ?, Hindex = ? WHERE PlayerID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return sqlite3_errcode(db)
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (Hdate as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(updateHandicap - Hdate)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2,  Int32(Handicap)) != SQLITE_OK {
            logDbErr("sqlite3_bind_double...updateHandicap.Handicap")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_double(self.sqlitePrepareStmt, 3, Hindex) != SQLITE_OK {
            logDbErr("sqlite3_bind_double...updateHandicap.Hindex")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 4, Int32(PlayerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int...updateHandicap.PlayerID")
            return sqlite3_errcode(db)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(courseUpdateEntryStmt) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
}
}
