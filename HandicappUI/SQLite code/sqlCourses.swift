//
//  SqliteDbStore+CrudOperations.swift
//  SqliteIntegration
//
//  Created by Ayush Gupta on 1/20/19.
//  Copyright © 2019 Ayush Gupta. All rights reserved.
//

import Foundation
import SQLite3
import Cocoa

class sqlCourses: SqliteDbStore {

    func rebuildCourseTables() -> Int32 {
        var returnCode = rebuildCourseTables1()
        if returnCode != SQLITE_OK {
            return returnCode
        }
        returnCode = rebuildCourseTables2()
        if returnCode != SQLITE_OK {
            return returnCode
        }
        returnCode = rebuildCourseTables3()
        if returnCode != SQLITE_OK {
            return returnCode
        }
        returnCode = rebuildCourseTables4()
        if returnCode != SQLITE_OK {
            return returnCode
        }
        returnCode = rebuildCourseTables5()
        if returnCode != SQLITE_OK {
            return returnCode
        }
        returnCode = rebuildCourseTables6()
        if returnCode != SQLITE_OK {
            return returnCode
        }
        returnCode = rebuildCourseTables7()
        if returnCode != SQLITE_OK {
            return returnCode
        }
        returnCode = rebuildCourseTables8()
        if returnCode != SQLITE_OK {
            return returnCode
        }
        return SQLITE_OK
    }
    func rebuildCourseTables1() -> Int32 {
        let aSqlStmt = """
            CREATE TABLE "CourseNew" (
                "CourseID"    INTEGER UNIQUE,
                "CourseName"    TEXT UNIQUE,
                PRIMARY KEY("CourseID")
            )
            """
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return sqlite3_errcode(db)
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(rebuildCourseTables1) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func rebuildCourseTables2() -> Int32 {
        let aSqlStmt = """
            insert INTO CourseNew SELECT * from Course
            """
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return sqlite3_errcode(db)
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(rebuildCourseTables2) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func rebuildCourseTables3() -> Int32 {
        let aSqlStmt = """
            DROP TABLE Course
            """
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return sqlite3_errcode(db)
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(rebuildCourseTables3) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func rebuildCourseTables4() -> Int32 {
        let aSqlStmt = """
            ALTER TABLE CourseNew RENAME TO Course
            """
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return sqlite3_errcode(db)
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(rebuildCourseTables4) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func rebuildCourseTables5() -> Int32 {
        let aSqlStmt = """
            CREATE TABLE "TeeNew" (
                "CourseID"    INTEGER,
                "Tee"    TEXT,
                "TeeID"    INTEGER,
                "Par"    INTEGER,
                "Rating"    INTEGER,
                "Slope"    INTEGER,
                PRIMARY KEY("CourseID","TeeID")
            )
            """
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return sqlite3_errcode(db)
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(rebuildCourseTables5) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func rebuildCourseTables6() -> Int32 {
        let aSqlStmt = """
            insert INTO TeeNew SELECT * from Tee
            """
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return sqlite3_errcode(db)
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(rebuildCourseTables6) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func rebuildCourseTables7() -> Int32 {
        let aSqlStmt = """
            DROP TABLE Tee
            """
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return sqlite3_errcode(db)
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(rebuildCourseTables7) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func rebuildCourseTables8() -> Int32 {
        let aSqlStmt = """
            ALTER TABLE TeeNew RENAME TO Tee
            """
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return sqlite3_errcode(db)
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(rebuildCourseTables8) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }

    func setupComboBoxNames(cboBox: inout NSComboBox) {
        let courseNamesArray = self.SelectNames()

        cboBox.removeAllItems()
        cboBox.addItems(withObjectValues: courseNamesArray)

    }
    func setupComboBoxLastPlayed(cboBox: inout NSComboBox, playerID: Int, gameID: Int) {
        let courseName = self.playedCourseName(playerID: playerID, gameID: gameID)
        cboBox.stringValue = courseName
    }
    func CourseName(courseID: Int) -> String {

        let courseName: String = MyErrors.unKnown

            let aSqlStmt = """
            select Course.CourseName from Course
            WHERE CourseID = ?
            """

            guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
            else {
                return (courseName)
            }
            defer {
                resetFinalize(stmt: self.sqlitePrepareStmt)
            }
            if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(courseID)) != SQLITE_OK {
                logDbErr("sqlite3_bind_text(playedCourseName - playerID)")
                return (courseName)
            }

            _ = logDbSQL(stmt: &sqlitePrepareStmt)
            if sqlite3_step(sqlitePrepareStmt) != SQLITE_ROW {
                logDbErr("nothing returned playedCourseName:", alert: false)
                return (courseName)
            }

            return(String(cString: sqlite3_column_text(sqlitePrepareStmt, 0)))

    }
    func playedCourseName(playerID: Int, gameID: Int) -> String {

        let courseName: String = MyErrors.unKnown

            let aSqlStmt = """
            select Course.CourseName from game
            INNER JOIN Course on course.CourseID = game.CourseID
            WHERE game.PlayerID = ? AND game.GameID = ?
            """

            guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
            else {
                return (courseName)
            }
            defer {
                resetFinalize(stmt: self.sqlitePrepareStmt)
            }
            if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
                logDbErr("sqlite3_bind_text(playedCourseName - playerID)")
                return (courseName)
            }
            if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(gameID)) != SQLITE_OK {
                logDbErr("sqlite3_bind_text(playedCourseName - gameID)")
                return (courseName)
            }
            _ = logDbSQL(stmt: &sqlitePrepareStmt)
            if sqlite3_step(sqlitePrepareStmt) != SQLITE_ROW {
                logDbErr("nothing returned playedCourseName:", alert: false)
                return (courseName)
            }

            return(String(cString: sqlite3_column_text(sqlitePrepareStmt, 0)))

    }
    /// Select * from Course
    func selectAll()  -> Array<CourseRec>{

        let courseAllSqlStmt = "SELECT CourseName, CourseID FROM Course ORDER BY CourseName ASC"

        var courseRecs: [CourseRec] = []
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: courseAllSqlStmt) == SQLITE_OK
        else {
            return courseRecs
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            let courserec = CourseRec()
            courserec.CourseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 0))
            courserec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))

            courseRecs.append(courserec)
        }
        return courseRecs
    }
    ///  INSERT INTO Course (CourseName, CourseID) VALUES (?,?)"
    func insert(courserec: CourseRec) -> Int32 {

        let courseInsertSqlStmt = "INSERT INTO Course (CourseID, CourseName) VALUES (?, ?)"

        // ensure statements are created on first usage if nil
        //guard self.prepareInsertEntryStmt() == SQLITE_OK else { return }
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: courseInsertSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db) }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        
        //  At some places (esp sqlite3_bind_xxx functions), we typecast String to NSString and then convert to char*,
        // ex: (eventLog as NSString).utf8String. This is a weird bug in swift's sqlite3 bridging. this conversion resolves it.
        
        //Inserting fields in insertEntryStmt prepared statement
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(courserec.CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 2, (courserec.CourseName as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return  sqlite3_errcode(db)
        }


        //executing the query to insert values
        
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(insertEntryStmt) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func select(courseName: String)  -> CourseRec {

        let courseReadSqlStmt = "SELECT CourseName, CourseID FROM Course WHERE CourseName = ? LIMIT 1"

        let courserec = CourseRec()
        courserec.CourseID = 99
        courserec.CourseName = "unKnown"
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: courseReadSqlStmt) == SQLITE_OK
        else
        { return courserec }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (courseName as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlCourses.select ... CourseName)")
            return courserec
        }

        //          executing the query to read value
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        if sqlite3_step(sqlitePrepareStmt) != SQLITE_ROW {
            logDbErr("sqlite3_step COUNT* readEntryStmt:")
            return courserec
        }

        courserec.CourseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 0))
        courserec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
        return courserec

    }
    func select(courseID: Int)  -> CourseRec {

        let courseReadSqlStmt = "SELECT CourseName, CourseID FROM Course WHERE CourseID = ? LIMIT 1"

        let courserec = CourseRec()
        courserec.CourseID = 99
        courserec.CourseName = "unKnown"
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: courseReadSqlStmt) == SQLITE_OK
        else
        { return courserec }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        
        //          Inserting CourseID in readEntryStmt prepared statement
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(readEntryStmt)")
            return courserec
        }
        
        //          executing the query to read value
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        if sqlite3_step(sqlitePrepareStmt) != SQLITE_ROW {
            logDbErr("sqlite3_step COUNT* readEntryStmt:")
            return courserec
        }

        //          take data out of the column into the returning record
        courserec.CourseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 0))
        courserec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
        return courserec

    }
    func courseID(CourseName: String) -> Int {
        let aSqlStmt = "SELECT CourseID FROM Course WHERE CourseName = ?"

        var myCourseID = 99
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return myCourseID
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (CourseName as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlCourses.courseID ... CourseName)")
            return myCourseID
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

             myCourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))

        }
        return myCourseID
    }
    func courseREC(CourseName: String) -> CourseRec {
        let aSqlStmt = "SELECT CourseID, CourseName FROM Course WHERE CourseName = ?"

        let myRec = CourseRec()
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return myRec
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (CourseName as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlCourses.courseID ... CourseName)")
            return myRec
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            myRec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            myRec.CourseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 1))

        }
        return myRec
    }
    //SelectNames
    func name(courseID: Int) -> String {
        let aSqlStmt = "SELECT CourseName FROM Course WHERE CourseID = ?"

        var courseName = MyErrors.unKnown
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return courseName
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(playedCourseName - playerID)")
            return (courseName)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            courseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 0))

        }
        return courseName
    }
    func SelectNames()  -> Array<String> {

        let aSqlStmt = "SELECT CourseName FROM Course ORDER BY CourseName ASC"

        var anArray = [String]()
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return anArray
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            let courseName = String(cString: sqlite3_column_text(sqlitePrepareStmt, 0))

            anArray.append(courseName)
        }
        return anArray
    }
    /// UPDATE Course SET CourseName = ?, CourseID = ?  WHERE CourseName = ?
    func update(record: CourseRec, orgCourseName: String) -> Int32{

        let courseUpdateSqlStmt = "UPDATE Course SET CourseName = ?, CourseID = ?  WHERE CourseName = ?"

        // ensure statements are created on first usage if nil
        // guard self.prepareUpdateEntryStmt() == SQLITE_OK else { return }
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: courseUpdateSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db) }
        
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        //  At some places (esp sqlite3_bind_xxx functions), we typecast String to NSString and then convert to char*,
        // ex: (eventLog as NSString).utf8String. This is a weird bug in swift's sqlite3 bridging. this conversion resolves it.
        
        //   update the courseName where CourseID
        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (record.CourseName as NSString).utf8String, -1, nil) != SQLITE_OK {
                    logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
                    return sqlite3_errcode(db)
                }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (record.CourseName as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }
        
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(record.CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 3, (orgCourseName as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(courseUpdateEntryStmt) \(r)")
            return sqlite3_errcode(db)
        }
        
        return SQLITE_OK
    }
    /// DELETE FROM Course WHERE CourseID = ?"
    func delete(CourseID: Int) -> Int32{

        let courseDeleteSqlStmt = "DELETE FROM Course WHERE CourseID = ?"

        // ensure statements are created on first usage if nil
        // guard self.prepareDeleteEntryStmt() == SQLITE_OK else { return }
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: courseDeleteSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db)}
        
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        
        //  At some places (esp sqlite3_bind_xxx functions), we typecast String to NSString and then convert to char*,
        // ex: (eventLog as NSString).utf8String. This is a weird bug in swift's sqlite3 bridging. this conversion resolves it.
        
        //Inserting name in deleteEntryStmt prepared statement
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(deleteEntryStmt)")
            return sqlite3_errcode(db)
        }
        
        //executing the query to delete row
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(deleteEntryStmt) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func exists(CourseName: String) -> Bool {
        let aSqlStmt = "SELECT CourseID FROM Course WHERE CourseName = ?"

        var exists = false
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return exists
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (CourseName as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlCourses.courseID ... CourseName)")
            return exists
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            exists = true

        }
        return exists
    }

    
}
