//
//  sqlTees.swift
//  Handisqlite
//
//  Created by Brian Quick on 2020-12-23.
//

import Foundation
import SQLite3
import Cocoa

class sqlTees: SqliteDbStore  {

    func insert(record: TeeRec) -> Int32 {

        let aSqlStmt = "INSERT INTO Tee (CourseID, TeeID, Tee, Par, Rating, Slope) VALUES (?,?,?,?,?,?)"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db) }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(record.CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlTees.insert - CourseID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(record.TeeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlTees.insert - CourseID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 3, (record.Tee as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlTees.insert - Tee)")
            return  sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 4, Int32(record.Par)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlTees.insert - Par)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_double(self.sqlitePrepareStmt, 5, record.Rating) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlTees.insert - Rating)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_double(self.sqlitePrepareStmt, 6, record.Slope) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlTees.insert - Slope)")
            return sqlite3_errcode(db)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(sqlTees.create) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func update(record: TeeRec, orgTee: String) -> Int32{

        let aSqlStmt = "UPDATE Tee SET Tee = ?, TeeID = ?, Par = ?, Rating = ?, Slope = ?  WHERE CourseID = ? AND Tee = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db) }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (record.Tee as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(record.TeeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 3, Int32(record.Par)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_double(self.sqlitePrepareStmt, 4, record.Rating) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_double(self.sqlitePrepareStmt, 5, record.Slope) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 6, Int32(record.CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 7, (orgTee as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(courseUpdateEntryStmt)")
            return sqlite3_errcode(db)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(courseUpdateEntryStmt) \(r)")
            return sqlite3_errcode(db)
        }

        return SQLITE_OK
    }
    func delete(CourseID: Int, TeeID: Int) -> Int32{

        let aSqlStmt = "DELETE FROM Tee WHERE CourseID = ? AND TeeID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db)}

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlTees.delete)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(TeeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlTees.delete)")
            return sqlite3_errcode(db)
        }

        //executing the query to delete row
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(sqlTees.delete) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func exists(courseName: String, tee: String) -> Bool {
        let aSqlStmt = """
        SELECT tee.CourseID, Tee, TeeID, Par, Rating, Slope FROM tee
        INNER JOIN Course on tee.CourseID = Course.CourseID
        WHERE Tee = ? AND CourseName = ? Limit 1
        """

        var exists = false

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return exists
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (tee as NSString).utf8String, -1, nil) != SQLITE_OK {
                           logDbErr("sqlite3_bind_text(sqlCourses.tee ... CourseName)")
                           return exists
                       }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 2, (courseName as NSString).utf8String, -1, nil) != SQLITE_OK {
                    logDbErr("sqlite3_bind_text(sqlCourses.courseName ... CourseName)")
                    return exists
                }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            exists = true

        }
        return exists
    }
    func setupComboBoxNames(cboBox: inout NSComboBox, CourseID: Int) {
        var NamesArray = self.SelectNames(courseID: CourseID)

        cboBox.removeAllItems()
        if NamesArray.count == 0 {
            NamesArray.append(MyErrors.unKnown)
        }
        cboBox.addItems(withObjectValues: NamesArray)


    }
    func setupComboBoxLastPlayed(cboBox: inout NSComboBox, playerID: Int, gameID: Int) {
        let teeName = self.playedTeeName(playerID: playerID, gameID: gameID)
        cboBox.stringValue = teeName
    }
    func playedTeeName(playerID: Int, gameID: Int) -> String {

        let teeName: String = MyErrors.unKnown

        let aSqlStmt = """
        select Tee.Tee from game
        INNER JOIN Course on course.CourseID = game.CourseID
        INNER JOIN Tee on game.TeeID = tee.TeeID AND Game.CourseID = tee.CourseID
        WHERE game.PlayerID = ? AND game.GameID = ?
        """

            guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
            else {
                return (teeName)
            }
            defer {
                resetFinalize(stmt: self.sqlitePrepareStmt)
            }
            if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
                logDbErr("sqlite3_bind_text(playedTeeName - playerID)")
                return (teeName)
            }
            if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(gameID)) != SQLITE_OK {
                logDbErr("sqlite3_bind_text(playedTeeName - gameID)")
                return (teeName)
            }

            _ = logDbSQL(stmt: &sqlitePrepareStmt)
            if sqlite3_step(sqlitePrepareStmt) != SQLITE_ROW {
                logDbErr("nothing returned playedTeeName:", alert: false)
                return (teeName)
            }

            return(String(cString: sqlite3_column_text(sqlitePrepareStmt, 0)))

    }
    func select(courseID: Int, teeID: Int) -> TeeRec {
        let aSqlStmt = "SELECT CourseID, Tee, TeeID, Par, Rating, Slope FROM tee WHERE courseID = ? AND TeeID = ? Limit 1"

        let teerec = TeeRec()

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return teerec
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(playedTeeName - gameID)")
            return teerec
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(teeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(playedTeeName - gameID)")
            return teerec
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {


            teerec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            teerec.Tee = String(cString: sqlite3_column_text(sqlitePrepareStmt, 1))
            teerec.TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 2))
            teerec.Par = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
            teerec.Rating = Double(sqlite3_column_double(sqlitePrepareStmt, 4))
            teerec.Slope = Double(sqlite3_column_double(sqlitePrepareStmt, 5))

        }
        return teerec
    }
    func select(courseName: String, tee: String) -> TeeRec {
        let aSqlStmt = """
        SELECT tee.CourseID, Tee, TeeID, Par, Rating, Slope FROM tee
        INNER JOIN Course on tee.CourseID = Course.CourseID
        WHERE Tee = ? AND CourseName = ? Limit 1
        """

        let teerec = TeeRec()

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return teerec
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (tee as NSString).utf8String, -1, nil) != SQLITE_OK {
                           logDbErr("sqlite3_bind_text(sqlCourses.tee ... CourseName)")
                           return teerec
                       }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 2, (courseName as NSString).utf8String, -1, nil) != SQLITE_OK {
                    logDbErr("sqlite3_bind_text(sqlCourses.courseName ... CourseName)")
                    return teerec
                }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            teerec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            teerec.Tee = String(cString: sqlite3_column_text(sqlitePrepareStmt, 1))
            teerec.TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 2))
            teerec.Par = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
            teerec.Rating = Double(sqlite3_column_double(sqlitePrepareStmt, 4))
            teerec.Slope = Double(sqlite3_column_double(sqlitePrepareStmt, 5))

        }
        return teerec
    }
    func SelectPlayedNames(playerID: Int, gameID: Int)  -> Array<String>{

        let SqlStmt = """
        select Tee.Tee
        from game
        INNER JOIN Course on course.CourseID = game.CourseID
        INNER JOIN Tee on  Game.CourseID = tee.CourseID
        WHERE game.PlayerID = ? AND game.GameID = ?
        """

        var namesArray = [String]()
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: SqlStmt) == SQLITE_OK
        else {
            return namesArray
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(playerID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(SelectPlayedNames in sqlTees)")
            return namesArray
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(gameID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(SelectPlayedNames in sqlTees)")
            return namesArray
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            let Tee = String(cString: sqlite3_column_text(sqlitePrepareStmt, 0))

            namesArray.append(Tee)
        }
        return namesArray
    }
    func SelectAll()  -> Array<TeeRec>{

        let SqlStmt = "SELECT CourseID, Tee, TeeID, Par, Rating, Slope FROM Tee  ORDER BY CourseID ASC, Tee ASC"

        var anArray: [TeeRec] = []
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: SqlStmt) == SQLITE_OK
        else {
            return anArray
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            let teerec = TeeRec()
            teerec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            teerec.Tee = String(cString: sqlite3_column_text(sqlitePrepareStmt, 1))
            teerec.TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 2))
            teerec.Par = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
            teerec.Rating  = Double(sqlite3_column_double(sqlitePrepareStmt, 4))
            teerec.Slope = Double(sqlite3_column_double(sqlitePrepareStmt, 5))

            anArray.append(teerec)
        }
        return anArray
    }
    func SelectAllForCourse(courseID: Int)  -> Array<TeeRec>{

        let SqlStmt = "SELECT CourseID, Tee, TeeID, Par, Rating, Slope FROM Tee WHERE CourseID = ? ORDER BY Tee ASC"

        var anArray: [TeeRec] = []
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: SqlStmt) == SQLITE_OK
        else {
            return anArray
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(SelectNames in sqlTees)")
            return anArray
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            let teerec = TeeRec()
            teerec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            teerec.Tee = String(cString: sqlite3_column_text(sqlitePrepareStmt, 1))
            teerec.TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 2))
            teerec.Par = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
            teerec.Rating  = Double(sqlite3_column_double(sqlitePrepareStmt, 4))
            teerec.Slope = Double(sqlite3_column_double(sqlitePrepareStmt, 5))

            anArray.append(teerec)
        }
        return anArray
    }

    func CountForCourse(courseID: Int)  throws -> Int64{

        let SqlStmt = "SELECT COUNT(*) FROM Tee WHERE CourseID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: SqlStmt) == SQLITE_OK
        else {
            logDbErr("sqlite3_bind_text(sqlTees.CountForCourse)")
            throw SqliteError(message: "Error in  value in sqlTees.CountForCourse")
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlTees.CountForCourse)")
            throw SqliteError(message: "Error in  value in sqlTees.CountForCourse")
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        var myCount : Int64 = 0
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {
             myCount = Int64(sqlite3_column_int64(sqlitePrepareStmt, 0))
        }
        return myCount
    }
    func SelectAllForCourseName(CourseName: String)  -> Array<TeeRec>{

        let SqlStmt = "SELECT tee.CourseID, Tee, TeeID, Par, Rating, Slope FROM Tee inner JOIN Course on tee.CourseID = Course.CourseID AND Course.CourseName = ? ORDER BY Tee ASC"

        var anArray: [TeeRec] = []
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: SqlStmt) == SQLITE_OK
        else {
            return anArray
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (CourseName as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(SelectNames in sqlTees)")
            return anArray
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            let teerec = TeeRec()
            teerec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            teerec.Tee = String(cString: sqlite3_column_text(sqlitePrepareStmt, 1))
            teerec.TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 2))
            teerec.Par = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
            teerec.Rating  = Double(sqlite3_column_double(sqlitePrepareStmt, 4))
            teerec.Slope = Double(sqlite3_column_double(sqlitePrepareStmt, 5))

            anArray.append(teerec)
        }
        return anArray
    }
    func SelectNames(courseID: Int)  -> Array<String>{

        let SqlStmt = "SELECT Tee FROM Tee WHERE CourseID = ? ORDER BY TeeID ASC"

        var namesArray = [String]()
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: SqlStmt) == SQLITE_OK
        else {
            return namesArray
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(SelectNames in sqlTees)")
            return namesArray
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            let Tee = String(cString: sqlite3_column_text(sqlitePrepareStmt, 0))

            namesArray.append(Tee)
        }
        return namesArray
    }
    func Tee(teeID: Int, courseID: Int) -> String {
        let aSqlStmt = "SELECT Tee FROM tee WHERE TeeID = ? AND courseID = ?"

        var myTee = MyErrors.unKnown

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return MyErrors.unKnown
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(teeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(playedTeeName - gameID)")
            return (MyErrors.unKnown)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(playedTeeName - gameID)")
            return (MyErrors.unKnown)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

             myTee = String(cString: sqlite3_column_text(sqlitePrepareStmt, 0))

        }
        return myTee
    }
    func TeeID(teeName: String, courseID: Int) -> Int {
        let aSqlStmt = "SELECT TeeID FROM tee WHERE Tee = ? AND courseID = ?"

        var myTeeID = 99
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return myTeeID
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_text(self.sqlitePrepareStmt, 1, (teeName as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlCourses.courseID ... CourseName)")
            return myTeeID
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(playedTeeName - gameID)")
            return (myTeeID)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

             myTeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))

        }
        return myTeeID
    }
    func TeeParRatingSlope(teeID: Int, courseID: Int ) -> (Double, Double, Double) {
        let aSqlStmt = "SELECT Par, Rating, Slope FROM tee WHERE TeeID = ? AND courseID = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else {
            return (99.9, 99.9, 99.9)
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(teeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(playedTeeName - gameID)")
            return (99.9, 99.9, 99.9)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(playedTeeName - gameID)")
            return (99.9, 99.9, 99.9)
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        if sqlite3_step(sqlitePrepareStmt) != SQLITE_ROW {
            logDbErr("sqlite3_bind_text(playedTeeName - gameID)")
            return (99.9, 99.9, 99.9)
        }
        let Par  = Double(sqlite3_column_double(sqlitePrepareStmt, 0))
        let Rating  = Double(sqlite3_column_double(sqlitePrepareStmt, 1))
        let Slope = Double(sqlite3_column_double(sqlitePrepareStmt, 2))

        return (Par, Rating, Slope)
    }

}
