//
//  sqlPars.swift
//  Handisqlite
//
//  Created by Brian Quick on 2020-12-21.
//

import Foundation
import SQLite3

class sqlPars: SqliteDbStore {

    func select(courseID: Int, teeID: Int, hole: Int)  -> ParsRec{

        //let parAllSqlStmt = "SELECT * FROM Pars WHERE  TeeID = ? ORDER BY Hole ASC"
        let parAllSqlStmt = """
            SELECT CourseID, TeeID, Hole, Par, Handicap, Yardage
                FROM Pars
                WHERE CourseID = ? AND TeeID = ? AND Hole = ?
            """
        var parsrec = ParsRec()
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: parAllSqlStmt) == SQLITE_OK
        else {
            return parsrec
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlpars.select.courseID)")
            return parsrec
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(teeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlpars.select.teeID)")
            return parsrec
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 3, Int32(hole)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlpars.select.hole)")
            return parsrec
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            parsrec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            parsrec.TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
            parsrec.Hole = Int(sqlite3_column_int(sqlitePrepareStmt, 2))
            parsrec.Par = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
            parsrec.Handicap = Int(sqlite3_column_int(sqlitePrepareStmt, 4))
            parsrec.Yardage = Int(sqlite3_column_int(sqlitePrepareStmt, 5))

        }

        return parsrec
    }
    func selectCourseTee(courseID: Int, teeID: Int)  -> Array<ParsRec>{

        //let parAllSqlStmt = "SELECT * FROM Pars WHERE  TeeID = ? ORDER BY Hole ASC"
        let parAllSqlStmt = "SELECT CourseID, TeeID, Hole, Par, Handicap, Yardage FROM Pars WHERE CourseID = ? AND TeeID = ? ORDER BY Hole ASC"
        var parsRecArray: [ParsRec] = []
        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: parAllSqlStmt) == SQLITE_OK
        else {
            return parsRecArray
        }
        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(courseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(gamesCountACourseEntryStmt)")
            return parsRecArray
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(teeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(gamesCountACourseEntryStmt)")
            return parsRecArray
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {

            let parsrec = ParsRec()
            parsrec.CourseID = Int(sqlite3_column_int(sqlitePrepareStmt, 0))
            parsrec.TeeID = Int(sqlite3_column_int(sqlitePrepareStmt, 1))
            parsrec.Hole = Int(sqlite3_column_int(sqlitePrepareStmt, 2))
            parsrec.Par = Int(sqlite3_column_int(sqlitePrepareStmt, 3))
            parsrec.Handicap = Int(sqlite3_column_int(sqlitePrepareStmt, 4))
            parsrec.Yardage = Int(sqlite3_column_int(sqlitePrepareStmt, 5))

            parsRecArray.append(parsrec)
        }
        _ = myLog("sqlPars.selectCourseTee nr. records \(parsRecArray.count)")
        if parsRecArray.count == 0 {
            for i in 1...18 {
                let parsrec = ParsRec()
                parsrec.CourseID = courseID
                parsrec.TeeID = teeID
                parsrec.Hole = i
                parsRecArray.append(parsrec)
            }
        }
        return parsRecArray
    }
    func updateATeePars(par: [ParsRec]) {
        for par in par {
            if checkOnePar(par: par) {
                if updateOnePar(par: par) != SQLITE_OK {
                        // FIXME: What to do on an error???
                }
            } else {
                if insertOnePar(par: par) != SQLITE_OK {
                        // FIXME: What to do on an error???
                }
            }
        }
    }
    func insertOnePar(par: ParsRec) -> Int32 {

        let aSqlStmt = """
                INSERT INTO Pars (CourseID, TeeID,  Hole, Par, Handicap, Yardage)
                VALUES (?, ?, ?, ?, ?, ?)
                """

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db) }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(par.CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlPars.insertOnePar ... CourseID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(par.TeeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlPars.insertOnePar ... teeID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 3, Int32(par.Hole)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlPars.insertOnePar ... Hole)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 4, Int32(par.Par)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlPars.insertOnePar ... Par)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 5, Int32(par.Handicap)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlPars.insertOnePar ... Handicap)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 6, Int32(par.Yardage)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlPars.insertOnePar ... Yardage)")
            return sqlite3_errcode(db)
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(sqlPars.insertOnePar ... ) \(r)")
            return sqlite3_errcode(db)
        }

        return SQLITE_OK
    }
    func updateOnePar(par: ParsRec) -> Int32 {

        let aSqlStmt = "UPDATE Pars SET Par = ?, Handicap = ?, Yardage = ? WHERE CourseID = ? AND TeeID = ? AND Hole = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db) }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(par.Par)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlpars.updateOnePar ... Par)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(par.Handicap)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlpars.updateOnePar ... Hole)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 3, Int32(par.Yardage)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlpars.updateOnePar ... Yardage)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 4, Int32(par.CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlpars.updateOnePar ... CourseID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 5, Int32(par.TeeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlpars.updateOnePar ... TeeID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 6, Int32(par.Hole)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int(sqlpars.updateOnePar ... Hole)")
            return sqlite3_errcode(db)
        }

        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(sqlPars.updateOnePar) \(r)")
            return sqlite3_errcode(db)
        }

        return SQLITE_OK
    }
    func delete(CourseID: Int, TeeID: Int, Hole: Int) -> Int32{

        let aSqlStmt = "DELETE FROM Pars WHERE CourseID = ? AND TeeID = ? AND Hole = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else { return sqlite3_errcode(db)}

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlPars.delete .. CourseID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(TeeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlPars.delete .. TeeID)")
            return sqlite3_errcode(db)
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 3, Int32(Hole)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(sqlPars.delete .. Hole)")
            return sqlite3_errcode(db)
        }

        //executing the query to delete row
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        let r = sqlite3_step(self.sqlitePrepareStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(sqlPars.delete) \(r)")
            return sqlite3_errcode(db)
        }
        return SQLITE_OK
    }
    func checkOnePar(par: ParsRec) -> Bool {

        let aSqlStmt = "SELECT count(*) FROM Pars WHERE CourseID = ? AND TeeID = ? AND Hole = ?"

        guard self.prepareEntryStmt(stmt: &sqlitePrepareStmt, sql: aSqlStmt) == SQLITE_OK
        else
        {  logDbErr("sqlScores.selectOnePar")
            return true
        }

        defer {
            resetFinalize(stmt: self.sqlitePrepareStmt)
        }

        if sqlite3_bind_int(self.sqlitePrepareStmt, 1, Int32(par.CourseID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int sqlpars.checkOnePar ... CourseID")
            return true
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 2, Int32(par.TeeID)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int sqlpars.checkOnePar ... TeeID")
            return true
        }
        if sqlite3_bind_int(self.sqlitePrepareStmt, 3, Int32(par.Hole)) != SQLITE_OK {
            logDbErr("sqlite3_bind_int sqlpars.checkOnePar ... Hole")
            return true
        }
        _ = logDbSQL(stmt: &sqlitePrepareStmt)
        var myCount : Int64 = 1
        while sqlite3_step(sqlitePrepareStmt) == SQLITE_ROW {
             myCount = Int64(sqlite3_column_int64(sqlitePrepareStmt, 0))
        }
        return myCount > 0
    }
}
