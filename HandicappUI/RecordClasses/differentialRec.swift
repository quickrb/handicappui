//
//  DifferentialRec.swift
//  Handisqlite
//
//  Created by Brian Quick on 2021-01-23.
//
// get an array of the last 20 games
//    SELECT  Game.gdate, Game.GameID, tee.Par, tee.Rating, tee.Slope, Game.ScoreAdj, Game.ScoreAdj - Game.ScoreAdj as Differential from Game
//    INNER join tee on game.CourseID = tee.CourseID AND game.TeeID = tee.TeeID
//    where game.PlayerID = 1 AND GDate <= "2021-01-08"  LIMIT 20
// get one record for a single game
//    SELECT  Game.gdate, Game.GameID, tee.Par, tee.Rating, tee.Slope, Game.ScoreAdj, Game.ScoreAdj - Game.ScoreAdj as Differential from Game
//      INNER join tee on game.CourseID = tee.CourseID AND game.TeeID = tee.TeeID
//      where   Game.GameID = 1 and game.PlayerID = 1

import Foundation

@objcMembers class DifferentialRec: NSObject {

    var PlayerID: Int = 1
    var GameID: Int = 1
    var GDate: String = "2020-01-01"
    var CourseID: Int = 1
    var TeeID: Int = 1
    var Par: Int = 1
    var Rating: Double = 1.0
    var Slope: Double = 1.0
    var ScoreAdj: Int = 1
    var Differential: Double = 1.0
    var DifferentialCalced: Double {
        let x = Double((113 / Slope) * ( Double(ScoreAdj) - Rating))
        return round(x * 10) / 10
    }
    //        A Score Differential is calculated using the following formula:
    //        (113 / Slope Rating) x (Adjusted Gross Score - Course Rating - PCC adjustment)
    //  (113 / 122) * (88 - 68.1) = 18.431967213 = 18.4
    //  (113 / 122) * (85 - 68.1) = 15.653278689 = 15.7
    //  (113 / 122) * (83 - 68.1) = 13.800819672 = 13.8


}
