//
//  Record.swift
//  SqliteIntegration
//
//  Created by Ayush Gupta on 1/20/19.
//  Copyright © 2019 Ayush Gupta. All rights reserved.
//

import Foundation


@objcMembers class CourseRec:  Codable, Identifiable, Equatable {
    static func == (lhs: CourseRec, rhs: CourseRec) -> Bool {
        return
            lhs.CourseID == rhs.CourseID &&
            lhs.CourseName == rhs.CourseName
    }

     var CourseName: String = "unKnown"
     var CourseID: Int = 99
}
