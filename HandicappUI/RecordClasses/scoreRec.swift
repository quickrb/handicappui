//
//  scoreRec.swift
//  ScoreCardView
//
//  Created by Brian Quick on 2020-12-09.
//

import SwiftUI
class ScoreParClass: ScoreClass {
     var pars = [ParsRec]()
    override init() {
        super.init()
        self.pars = []
        for i in 1...18 {
            var parsrec = ParsRec()
            parsrec.Hole = i
            self.pars.append(parsrec)
        }
    }
    var numberOfPars: Int {
        var count = 0
        for i in 0..<18 {
            if scores[i].Score == pars[i].Par { count += 1 }
        }
        return count
    }
    func scoreNameCode(index: Int) -> String {
        var code = ""
        switch scores[index].Score - pars[index].Par {
        case -3:
            code = "A"
        case -2:
            code = "E"
        case -1:
            code = "I"
        case 0:
            code = "P"
        case 1:
            code = "B"
        case 2:
            code = "D"
        default:
            code = "O"
        }
        return code
    }
}
class ScoreClass: ObservableObject {
    @Published var scores = [ScoreRec]()
    @Published var isChanged: Bool = false

    static let saveKey = "ScoreClass"

    init() {
        self.scores = []
        buildScores()
    }
    func buildScores() {
        for i in 1...18 {
            var scorerec = ScoreRec()
            scorerec.Hole = i
            self.scores.append(scorerec)
        }
        self.isChanged.toggle()
    }
    func reInit() {
        scores.removeAll()
        buildScores()
    }
    func create(newscore: [ScoreRec]) {
        scores.removeAll()        // I do not think I need to do this
        scores = newscore
    }
    var bunkerFront9:  Int { return scores.filter({ $0.Hole < 10 }).map({ $0.Bunker }).reduce(0, +) }
    var bunkerBack9:  Int { return scores.filter({ $0.Hole > 9 }).map({ $0.Bunker }).reduce(0, +) }
    var bunkerBack18:  Int { return scores.reduce(0, {$0 + $1.Bunker}) }

    var fairwayRecorded: Int { return scores.filter({ !$0.Fairway.isEmpty }).map({ _ in 1 }).reduce(0, +) }
    var fairwayFront9:  Int { return scores.filter({ $0.Fairway == "H" && $0.Hole < 10}).map({ _ in 1 }).reduce(0, +) }
    var fairwayBack9:  Int { return scores.filter({ $0.Fairway == "H" && $0.Hole > 9 }).map({ _ in 1 }).reduce(0, +) }
    var fairwayBack18:  Int { return scores.filter({ $0.Fairway == "H" }).map({ _ in 1 }).reduce(0, +) }
    var fairwayH:  Int { return scores.filter({ $0.Fairway == "H" }).map({ _ in 1 }).reduce(0, +) }
    var fairwayR:  Int { return scores.filter({ $0.Fairway == "R" }).map({ _ in 1 }).reduce(0, +) }
    var fairwayL:  Int { return scores.filter({ $0.Fairway == "L" }).map({ _ in 1 }).reduce(0, +) }
    var fairwayM:  Int { return scores.filter({ $0.Fairway == "M" }).map({ _ in 1 }).reduce(0, +) }
    
    var penaltyFront9:  Int { return scores.filter({ $0.Hole < 10 }).map({ $0.Penalty }).reduce(0, +) }
    var penaltyBack9:  Int { return scores.filter({ $0.Hole > 9 }).map({ $0.Penalty }).reduce(0, +) }
    var penaltyBack18:  Int {return scores.reduce(0, {$0 + $1.Penalty}) }

    var puttsFront9:  Int { return scores.filter({ $0.Hole < 10 }).map({ $0.Putts }).reduce(0, +) }
    var puttsBack9:  Int { return scores.filter({ $0.Hole > 9 }).map({ $0.Putts }).reduce(0, +) }
    var puttsBack18:  Int {return scores.reduce(0, {$0 + $1.Putts}) }

    var greenRecorded: Int { return scores.filter({ !$0.Green.isEmpty }).map({ _ in 1 }).reduce(0, +) }
    var greensFront9:  Int { return scores.filter({ $0.Green == "H" && $0.Hole < 10}).map({ _ in 1 }).reduce(0, +) }
    var greensBack9:  Int { return scores.filter({ $0.Green == "H" && $0.Hole > 9 }).map({ _ in 1 }).reduce(0, +) }
    var greensBack18:  Int { return scores.filter({ $0.Green == "H" }).map({ _ in 1 }).reduce(0, +) }
    var greensH:  Int { return scores.filter({ $0.Green == "H" }).map({ _ in 1 }).reduce(0, +) }
    var greensR:  Int { return scores.filter({ $0.Green == "R" }).map({ _ in 1 }).reduce(0, +) }
    var greensL:  Int { return scores.filter({ $0.Green == "L" }).map({ _ in 1 }).reduce(0, +) }
    var greensO:  Int { return scores.filter({ $0.Green == "O" }).map({ _ in 1 }).reduce(0, +) }
    var greensS:  Int { return scores.filter({ $0.Green == "S" }).map({ _ in 1 }).reduce(0, +) }

    var scoreFront9:  Int { return scores.filter({ $0.Hole < 10 }).map({ $0.Score }).reduce(0, +) }
    var scoreBack9:  Int { return scores.filter({ $0.Hole > 9 }).map({ $0.Score }).reduce(0, +) }
    var scoreBack18:  Int { return scores.reduce(0, {$0 + $1.Score}) }
}

struct ScoreRec: Identifiable, Codable {
     var id = UUID()
     var PlayerID = 0
     var GameID = 0
     var Hole = 1
     var Score = 0
     var Fairway = ""
     var Green = ""
     var Putts = 0
     var Bunker = 0
     var Penalty = 0
}


