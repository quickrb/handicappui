//
//  playerRec.swift
//  Handisqlite
//
//  Created by Brian Quick on 2020-12-21.
//

import Foundation

class PlayersClass: ObservableObject {
    @Published var players = [PlayerRec]()
    static let saveKey = "PlayersClass"

    init() {
        refresh()
//        if let data = UserDefaults.standard.data(forKey: Self.saveKey) {
//            if let decoded = try? JSONDecoder().decode([PlayerRec].self, from: data) {
//                self.players = decoded
//                return
//            }
//        }
//        self.players = []
//        self.players.append(PlayerRec())
    }
    private func save() {
        if let encoded = try? JSONEncoder().encode(players) {
            UserDefaults.standard.set(encoded, forKey: Self.saveKey)
        }
    }
    func reInit() {
        players.removeAll()
        let playerrec = PlayerRec()
        players.append(playerrec)
    }
    func recreate(newPlayers: [PlayerRec]) {
        players.removeAll()        // I do not think I need to do this
        players = newPlayers
        save()
    }
    func refresh() {
        players = sqlPlayer().selectPlayers()
    }
}

struct PlayerRec: Identifiable, Codable  {
    var id = UUID()
    var Name: String = ""
    var PlayerID: Int = 0
    var Level: Int = 0
    var Hdate: String = ""
    var Handicap: Int = 0
    var Hindex: Double = 0.0

}
