//
//  teesRec.swift
//  Handisqlite
//
//  Created by Brian Quick on 2020-12-23.
//

import Foundation

class TeeRec: Codable, Identifiable, Equatable {
    static func == (lhs: TeeRec, rhs: TeeRec) -> Bool {
        return
            lhs.CourseID == rhs.CourseID &&
            lhs.Par == rhs.Par &&
            lhs.Rating == rhs.Rating &&
            lhs.Slope == rhs.Slope &&
            lhs.Tee == rhs.Tee &&
            lhs.TeeID == rhs.TeeID
    }

    internal init(CourseID: Int = 0, Tee: String = "unKnown", TeeID: Int = 0, Par: Int = 0, Rating: Double = 0.0, Slope: Double = 0.0) {
        self.CourseID = CourseID
        self.Tee = Tee
        self.TeeID = TeeID
        self.Par = Par
        self.Rating = Rating
        self.Slope = Slope
    }
    

    var CourseID: Int = 0
    var Tee: String = "unKnown"
    var TeeID: Int = 0
    var Par: Int = 0
    var Rating: Double = 0.0
    var Slope: Double = 0.0

}

