//
//  scoreCardStatsRec.swift
//  macstoryswiftUI
//
//  Created by Brian Quick on 2021-02-15.
//

import Foundation

@objcMembers class ScoreCardStatsRec: NSObject {

    var frontScore: Int
    var backScore: Int
    var totalScore: Int
    var frontPutts: Int
    var backPutts: Int
    var totalPutts: Int
    var frontFairway: Int
    var backFairway: Int
    var totalFairway: Int
    var frontGreen: Int
    var backGreen: Int
    var totalGreen: Int
    var frontBunker: Int
    var backBunker: Int
    var totalBunker: Int
    var frontPenalty: Int
    var backPenalty: Int
    var totalPenalty: Int

    internal init(frontScore: Int, backScore: Int, totalScore: Int, frontPutts: Int, backPutts: Int, totalPutts: Int, frontFairway: Int, backFairway: Int, totalFairway: Int, frontGreen: Int, backGreen: Int, totalGreen: Int, frontBunker: Int, backBunker: Int, totalBunker: Int, frontPenalty: Int, backPenalty: Int, totalPenalty: Int) {
        self.frontScore = frontScore
        self.backScore = backScore
        self.totalScore = totalScore
        self.frontPutts = frontPutts
        self.backPutts = backPutts
        self.totalPutts = totalPutts
        self.frontFairway = frontFairway
        self.backFairway = backFairway
        self.totalFairway = totalFairway
        self.frontGreen = frontGreen
        self.backGreen = backGreen
        self.totalGreen = totalGreen
        self.frontBunker = frontBunker
        self.backBunker = backBunker
        self.totalBunker = totalBunker
        self.frontPenalty = frontPenalty
        self.backPenalty = backPenalty
        self.totalPenalty = totalPenalty
    }
}

