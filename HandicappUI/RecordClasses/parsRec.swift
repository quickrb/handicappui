//
//  parRec.swift
//  Handisqlite
//
//  Created by Brian Quick on 2020-12-21.
//

import SwiftUI

class ParsClass: ObservableObject {
    @Published var pars = [ParsRec]()
    @Published var isChanged: Bool = false
//    {
//        willSet {
//            if pars.count > 0 {
//            _ = myLog("ParsClass Published willSet yardage \(pars[0].Yardage)")
//            }
//            objectWillChange.send()
//        }
//    }


    static let saveKey = "ParsClass"

    init() {
//        if let data = UserDefaults.standard.data(forKey: Self.saveKey) {
//            if let decoded = try? JSONDecoder().decode([ParsRec].self, from: data) {
//                self.pars = decoded
//                return
//            }
//        }
        self.pars = [ParsRec]()
        create()

    }

//    private func save() {
//        if let encoded = try? JSONEncoder().encode(pars) {
//            UserDefaults.standard.set(encoded, forKey: Self.saveKey)
//        }
//    }
    private func create() {
        for i in 1...18 {
            let parsrec = ParsRec()
            parsrec.Hole = i
            self.pars.append(parsrec)
        }
    }
    func deleteAll() {
        pars.removeAll()
        create()
        isChanged.toggle()
    }
    func create(newpars: [ParsRec]) {
        _ = myLog("Parsclass.create \(newpars[0].CourseID).\(newpars[0].TeeID)")
        pars.removeAll()        // I do not think I need to do this
        pars = newpars
        isChanged.toggle()
    }

    var parFront9:  Int { return pars.filter({ $0.Hole < 10 }).map({ $0.Par }).reduce(0, +) }
    var parBack9:  Int { return pars.filter({ $0.Hole > 9 }).map({ $0.Par }).reduce(0, +) }
    var parBack18:  Int { return pars.reduce(0, {$0 + $1.Par}) }

    var yardageFront9:  Int { return pars.filter({ $0.Hole < 10 }).map({ $0.Yardage }).reduce(0, +) }
    var yardageBack9:  Int { return pars.filter({ $0.Hole > 9 }).map({ $0.Yardage }).reduce(0, +) }
    var yardageBack18:  Int { return pars.reduce(0, {$0 + $1.Yardage}) }
}

class ParsRec: Identifiable, Codable, Equatable {
    static func == (lhs: ParsRec, rhs: ParsRec) -> Bool {
        return
            lhs.CourseID == rhs.CourseID &&
            lhs.TeeID == rhs.TeeID &&
            lhs.Hole == rhs.Hole &&
            lhs.Par == rhs.Par &&
            lhs.Handicap == rhs.Handicap &&
            lhs.Yardage == rhs.Yardage
    }

    var CourseID = 0
    var TeeID = 0
    var Hole = 1
    var Par = 4
    var Handicap = 0
    var Yardage = 0
}

