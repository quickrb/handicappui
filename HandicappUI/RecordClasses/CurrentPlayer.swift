//
//  CurrentPlayer.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-05-11.
//

import SwiftUI

class CurrentPlayer: ObservableObject {
    internal init(playerID: Int, playerName: String) {
        self.playerID = playerID
        self.playerName = playerName
    }

    @Published var playerID: Int {
        didSet {
            MyDefaults().playerID = playerID
        }
    }
    @Published var playerName: String {
        didSet {
            MyDefaults().playerName = playerName
        }
    }

}
