//
//  gameRec.swift
//  Handisqlite
//
//  Created by Brian Quick on 2020-12-20.
//

import Foundation

class GameClass: NSObject {
    var games = [GameRec]()
}

struct GameRec: Identifiable, Hashable {
    var id = UUID()
    var PlayerID: Int = 1
    var GameID: Int = 1
    var GDate: String = "2021-01-01"
    var CourseID: Int = 1
    var TeeID: Int = 1
    var Score: Int = 0
    var ScoreAdj: Int = 0
    var Handicapp: Int = 0
    var HandiIndex: Double = 0.0
    var Differential: Double = 0.0
    var CourseName: String = ""
    var TeeName: String = ""
}


