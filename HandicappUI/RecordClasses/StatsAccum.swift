//
//  StatsAccum.swift
//  HandicappUI
//
//  Created by Brian Quick on 2021-05-02.
//

import SwiftUI

class Statistic: ObservableObject {
    var typeCode: String = ""
    @Published var numberOfGames: Int = 0
    @Published var numberOfType: Int = 0
     var averageOfType: String {
        let myNum: Double = numberOfGames == 0 ? 1.0 : Double(numberOfGames)
        let myPercent: Double = Double(Double(numberOfType) / myNum)
        return String(format: "%.1f", myPercent)
    }
    var percentOfType: String {
        let myNum: Double = numberOfGames == 0 ? 1.0 : Double(numberOfGames)
        let myPercent: Double = Double(Double(numberOfType) / myNum) * 100
        return String(format: "%.1f", myPercent)
    }

}
struct StatsAccum {

     var accumChars: String
     var statistics = [Statistic]()

    internal init(accumChars: String, statistics: [Statistic] = [Statistic]()) {
        self.accumChars = accumChars
        self.statistics = statistics
        if accumChars == "Scores" {
            for i in 1...18 {
                let one = Statistic()
                one.typeCode = String(i)
                self.statistics.append(one)
            }
        } else {
                let myChars = Array(accumChars)
                for i in 0..<myChars.count {
                    let one = Statistic()
                    one.typeCode = String(myChars[i])
                    self.statistics.append(one)
                }
        }
    }
    // this counts every hole played for all the stats and accums value for the specific code to get the averages
    func accumForAverages(typeCode: String, value: Int) {
        if typeCode.isEmpty { return }
        let codeindex = statistics.firstIndex(where: {$0.typeCode == typeCode})
        if codeindex != nil {
            for i in 0..<statistics.count {
                statistics[i].numberOfGames += 1
            }
            statistics[codeindex ?? 0].numberOfType += value
        }
    }
    func countGameType(typeCode: String, value: Int) {
        let codeindex = statistics.firstIndex(where: {$0.typeCode == typeCode})
        if codeindex != nil {
            statistics[codeindex ?? 0].numberOfGames += 1
            statistics[codeindex ?? 0].numberOfType += value
        }
    }

}
struct StatisticsForGames {
    var allAccums: [StatsAccum] = [
        StatsAccum(accumChars: "Scores"),
        StatsAccum(accumChars: "Scores"),
        StatsAccum(accumChars: HoleValueFilters().scoringValuesStored),
        StatsAccum(accumChars: HoleValueFilters().fairwayValuesStored),
        StatsAccum(accumChars: HoleValueFilters().greenValuesStored),
        StatsAccum(accumChars: "G")
    ]
    let scoreIdx = 0
    let puttsIdx = 1
    let scoringIdx = 2
    let fairwayIdx = 3
    let greenIdx = 4
    let girIdx = 5

    @State private var scoreParClass = ScoreParClass()
    func accumAGame(gameRec: GameRec) {
        scoreParClass.create(newscore: sqlScores().selectAGame(playerID: gameRec.PlayerID, gameID: gameRec.GameID))
        if scoreParClass.pars[0].CourseID != gameRec.CourseID ||
            scoreParClass.pars[0].TeeID != gameRec.TeeID {
            scoreParClass.pars = sqlPars().selectCourseTee(courseID: gameRec.CourseID, teeID: gameRec.TeeID)
        }
        let doScores = scoreParClass.scoreBack18 != 0
        let doPutts = scoreParClass.puttsBack18 != 0
        let doFairway = scoreParClass.fairwayRecorded != 0
        let doGreen = scoreParClass.greenRecorded != 0
        for index in 0..<scoreParClass.scores.count {
            if doScores {
                allAccums[scoreIdx].countGameType(typeCode: String(index + 1), value: scoreParClass.scores[index].Score)
            }
            if doPutts {
                allAccums[puttsIdx].countGameType(typeCode: String(index + 1), value: scoreParClass.scores[index].Putts)
            }
            if doScores {
                allAccums[scoringIdx].accumForAverages(typeCode: scoreParClass.scoreNameCode(index: index), value: 1)
            }
            if doScores && doPutts {

//                var gir = 0
//                if (scoreParClass.scores[index].Putts == 0 && scoreParClass.scores[index].Score == scoreParClass.pars[index].Par - 2)
//                ||
//                    (scoreParClass.scores[index].Putts == 1 && scoreParClass.scores[index].Score == scoreParClass.pars[index].Par - 1)
//                ||
//                    (scoreParClass.scores[index].Putts == 2 && scoreParClass.scores[index].Score == scoreParClass.pars[index].Par) {
//                    gir = 1
//                }
                let gir = greenInRegulation(score: scoreParClass.scores[index].Score, putts: scoreParClass.scores[index].Putts, par: scoreParClass.pars[index].Par)
                allAccums[girIdx].accumForAverages(typeCode: "G", value: gir)
            }
            if doFairway {
                allAccums[fairwayIdx].accumForAverages(typeCode: scoreParClass.scores[index].Fairway, value: 1)
            }
            if doGreen {
                allAccums[greenIdx].accumForAverages(typeCode: scoreParClass.scores[index].Green, value: 1)
            }
        }
    }



    mutating func removeZeroes() {

        allAccums[scoringIdx].statistics.removeAll(where: { $0.numberOfType == 0 })
        for i in 0..<allAccums[scoringIdx].statistics.count {
            allAccums[scoringIdx].statistics[i].typeCode = HoleValueFilters().scoringDictionary[allAccums[scoringIdx].statistics[i].typeCode] ??  "unKnown"
        }
        allAccums[fairwayIdx].statistics.removeAll(where: { $0.numberOfType == 0 })
        for i in 0..<allAccums[fairwayIdx].statistics.count {
            allAccums[fairwayIdx].statistics[i].typeCode = HoleValueFilters().fairwayDictionary[allAccums[fairwayIdx].statistics[i].typeCode] ??  "unKnown"
        }
        allAccums[greenIdx].statistics.removeAll(where: { $0.numberOfType == 0 })
        for i in 0..<allAccums[greenIdx].statistics.count {
                    allAccums[greenIdx].statistics[i].typeCode = HoleValueFilters().greenDictionary[allAccums[greenIdx].statistics[i].typeCode] ??  "unKnown"
                }
    }
}


